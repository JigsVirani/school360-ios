//
//  ProjectUtility.swift
//  Happy Roads
//
//  Created by fiplmac1 on 01/10/16.
//  Copyright © 2016 fusion. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class ProjectUtility: NSObject
{
    //MARK:- Change DateForamatter
    class func stringFromDate (date: Date, strFormatter strDateFormatter: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = strDateFormatter
        
        let convertedDate = dateFormatter.string(from: date)
        
        return convertedDate
    }
    
    class func dateFromString (strDate: String, strFormatter strDateFormatter: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = strDateFormatter
        
        let convertedDate = dateFormatter.date(from: strDate)
        
        return convertedDate!
    }
    
    class func changeDateFormate (strDate: String, strFormatter1 strDateFormatter1: String, strFormatter2 strDateFormatter2: String) -> NSString
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = strDateFormatter1
        
        if let date = dateFormatter.date(from: strDate)
        {
            dateFormatter.dateFormat = strDateFormatter2
            
            if let strConvertedDate:NSString = dateFormatter.string(from: date) as NSString?
            {
                return strConvertedDate
            }
        }
        return ""
    }
    
    class func animatePopupView (viewPopup: UIView)
    {
        viewPopup.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001);
        
        UIView.animate(withDuration: 0.3/1.5, animations: {
            
            viewPopup.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
            
        }) { (finished) in
            
            UIView.animate(withDuration: 0.3/2, animations: {
                viewPopup.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9);
                }, completion: { (finished) in
                    
                    UIView.animate(withDuration: 0.3/2, animations: {
                        viewPopup.transform = CGAffineTransform.identity;
                    })
            })
        }
    }
    
    class func setCommonButton(button: UIButton){
        
        button.layer.cornerRadius = 20.0
    }
    
    class func loadingShow()
    {
        let loader = Loader()
        loader.show(UIColor.black)
    }
    
    class func loadingHide()
    {
        let loader = Loader()
        loader.hide()
    }
    
    class func displayTost(erroemessage: String)
    {
        let appDelegate = (UIApplication.shared.delegate as? AppDelegate)!
        
        let style = CSToastStyle.init(defaultStyle: ())
        style?.messageFont = UIFont.boldSystemFont(ofSize: 15.0)
        style?.messageColor = UIColor.white
        style?.messageAlignment = .center
        style?.titleAlignment = .center
        style?.backgroundColor = UIColor.init(red: 36.0 / 255.0, green: 159.0 / 255.0, blue: 231.0 / 255.0, alpha: 1.0)
        appDelegate.window?.makeToast(erroemessage, duration: 2.5, position: CSToastPositionBottom, style: style)
    }
    
    class func showAlert(vc: UIViewController, strMessage: String) {
        let alert = UIAlertController(title: "", message: strMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    class func videoSnapshot(filePathLocal: String) -> UIImage? {
        
        let asset = AVURLAsset.init(url: NSURL.init(string: filePathLocal)! as URL)
        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true
        
        let timestamp = CMTime(seconds: 1, preferredTimescale: 60)
        
        do {
            let imageRef = try generator.copyCGImage(at: timestamp, actualTime: nil)
            return UIImage.init(cgImage: imageRef)
        }
        catch let error as NSError
        {
            print("Image generation failed with error \(error)")
            return nil
        }
    }
    
    class func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) || (components.year! >= 1){
            
            return ProjectUtility.stringFromDate(date: date, strFormatter: "dd MMMM yyyy hh:mm a")
            
        }else if (components.month! >= 2) || (components.month! >= 1) || (components.weekOfYear! >= 2) || (components.weekOfYear! >= 1) || (components.day! >= 2) {
            
            return ProjectUtility.stringFromDate(date: date, strFormatter: "dd MMMM hh:mm a")
        }else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday ago"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) mins"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 min"
            } else {
                return "A min"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds"
        } else {
            return "Just now"
        }
    }
    
    class func footerTabActions(intTag: NSInteger, viewController: UIViewController, navigationController: UINavigationController) {
        
        if intTag == 1 {
            for controller: Any in (navigationController.viewControllers) {
                if (controller is AdvanceSearchVC) {
                    navigationController.popToViewController(controller as! AdvanceSearchVC, animated: false)
                    return
                }
            }
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = (storyboard.instantiateViewController(withIdentifier: "AdvanceSearchVC") as! AdvanceSearchVC)
            SlideNavigationController.sharedInstance().pushViewController(controller, animated: false)
        }else if intTag == 2 {
            
            for controller: Any in (navigationController.viewControllers) {
                if (controller is ForumVC) {
                    navigationController.popToViewController(controller as! ForumVC, animated: false)
                    return
                }
            }
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = (storyboard.instantiateViewController(withIdentifier: "ForumVC") as! ForumVC)
            SlideNavigationController.sharedInstance().pushViewController(controller, animated: false)
        }else if intTag == 3 {
            
        }else if intTag == 4 {
            for controller: Any in (navigationController.viewControllers) {
                if (controller is FeedbackVC) {
                    navigationController.popToViewController(controller as! FeedbackVC, animated: false)
                    return
                }
            }
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = (storyboard.instantiateViewController(withIdentifier: "FeedbackVC") as! FeedbackVC)
            SlideNavigationController.sharedInstance().pushViewController(controller, animated: false)
        }
    }
}
