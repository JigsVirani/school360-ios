//
//  ProjectSharedObj.swift
//  Schools360
//
//  Created by Sandeep Gangajaliya on 19/05/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit

class ProjectSharedObj: NSObject {
    
    static let sharedInstance = ProjectSharedObj()
    
    var baseUrl = "http://schools3sixty.com"
}
