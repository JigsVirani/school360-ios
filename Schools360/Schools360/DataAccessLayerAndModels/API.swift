//
//  API.swift
//  SignupWithApiIntegrationDemo
//
//  Created by Sandeep Gangajaliya on 2/7/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import Foundation
import Alamofire
@objc protocol APIDelegate {
    @objc optional func setProgress(progress:CGFloat)
}
class API{
    
    var delegate:APIDelegate?
    
    //MARK: - initialization method -
    class var sharedInstance: API {
        struct Singleton {
            static let instance = API()
        }
        return Singleton.instance
    }
    
    //MARK: - API- normal parameter
    
//    this function is calling when you know about the httpmethod or In case of compulsory to give          of http method. you must go to with particular httpmethod.
    class func getResponseFromApi(strUrl: String, param: Dictionary<String, String>? = nil ,withHTTPMethod HTTPMethod:Alamofire.HTTPMethod, completionHandler: @escaping (_ result: NSDictionary) -> Void) -> Void {
        
        print("\(strUrl) --- \(String(describing: param))")
        
        //make new url from baseurl and argumented url
        Alamofire.request(strUrl, method: HTTPMethod, parameters: param, encoding: URLEncoding.default, headers: nil).validate().responseJSON { (response) in
            completionHandler(API.returnResponse(response: response))
        }
    }
    
    class func getResponseFromApi(strUrl:String,param:Dictionary<String, String>? = nil ,completionHandler: @escaping (_ result: NSDictionary) -> Void) -> Void {
        //make new url from baseurl and argumented url
        Alamofire.request(strUrl, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).validate().responseJSON { (response) in
            completionHandler(API.returnResponse(response: response))
        }
      
    }
    class func returnResponse (response: DataResponse<Any>)->NSDictionary {
        print(response)
        if (response.result.isSuccess){
            if let value = response.result.value{
                return value as! NSDictionary
            }
        }else{
            print("\(String(describing: response.error?.localizedDescription))")
            var statusCode = 0
            if let error = response.result.error as? AFError {
                statusCode = error._code // statusCode private
                switch error {
                case .invalidURL(let url):
                    print("Invalid URL: \(url) - \(error.localizedDescription)")
                case .parameterEncodingFailed(let reason):
                    print("Parameter encoding failed: \(error.localizedDescription)")
                    print("Failure Reason: \(reason)")
                case .multipartEncodingFailed(let reason):
                    print("Multipart encoding failed: \(error.localizedDescription)")
                    print("Failure Reason: \(reason)")
                case .responseValidationFailed(let reason):
                    print("Response validation failed: \(error.localizedDescription)")
                    print("Failure Reason: \(reason)")
                    
                    switch reason {
                    case .dataFileNil, .dataFileReadFailed:
                        print("Downloaded file could not be read")
                    case .missingContentType(let acceptableContentTypes):
                        print("Content Type Missing: \(acceptableContentTypes)")
                    case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                        print("Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)")
                    case .unacceptableStatusCode(let code):
                        print("Response status code was unacceptable: \(code)")
                        statusCode = code
                    }
                case .responseSerializationFailed(let reason):
                    print("Response serialization failed: \(error.localizedDescription)")
                    print("Failure Reason: \(reason)")
                    // statusCode = 3840 ???? maybe..
                }
                
                print("Underlying error: \(String(describing: error.underlyingError))")
            } else if let error = response.result.error as? URLError {
                print("URLError occurred: \(error)")
            }else if let error = response.result.error as NSError?{
              print("timeout")
               statusCode = error._code
            }else {
                print("Unknown error: \(String(describing: response.result.error))")
            }
            
            print(statusCode)
            //make a response with nil value and set error or other information in it and return it.
            let paramDic:NSMutableDictionary = NSMutableDictionary()
//            paramDic[KEY_MESSAGE] = response.error?.localizedDescription
//            paramDic[KEY_DATA] = nil
//            paramDic[KEY_STATUS_CODE] = "\(statusCode)"
//            paramDic[KEY_SUCCESS] = 0
            
            return paramDic as NSDictionary
        }
        return NSDictionary()
    }
    //MARK: - API- Image with normal parameter
    
    
    class func uploadImageWithData(strApiUrl: String, strName: String, strImageUrl: String, param:Dictionary<String, String>? = nil ,completionHandler: @escaping (_ result: NSDictionary) -> Void) -> Void{
        Alamofire.upload(multipartFormData: { multipartFormData in
            let urlImage:URL = URL.init(fileURLWithPath: strImageUrl)
            multipartFormData.append(urlImage, withName: strName)
            for (key, value) in param! {
                multipartFormData.append(value.data(using:String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
            }
        }, to: strApiUrl, encodingCompletion: {
            (encodingResult) in
            print("encoding result:\(encodingResult)")
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                    //send progress using delegate
                    API.sharedInstance.delegate?.setProgress!(progress: CGFloat(Progress.fractionCompleted))
                })
                upload.responseJSON{ (response) in
                    completionHandler(API.returnResponse(response: response))
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        })
        
    }
    
    //MARK: - Upload image
    //Function for single uploading image
    class func uploadImage(strStorageUrl:String, withImagePath strImageUrl:String)
    {
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            let urlImage:URL = URL.init(fileURLWithPath: strImageUrl)
            multipartFormData.append(urlImage, withName: "profile_pic")
        }, to: strStorageUrl)
        { (result) in
            //result
        }
    }
    
}

