//
//  NSRestApiHelper.swift
//  RedFootStep
//
//  Created by Ashesh on 07/06/16.
//  Copyright © 2016 Ashesh. All rights reserved.
//

import UIKit

typealias CompletionHandler = (_ obj: AnyObject?, _ success: Bool?) -> Void

class NSRestApiHelper: NSObject
{

    override init ()
    {
        super.init()
    }
    
    // service URL property
    var serviceURL: String = String()
        {
        didSet
        {
            self.serviceURL = ProjectSharedObj.sharedInstance.baseUrl + serviceURL
        }
    }
    
    
//    func requestforPOST(params: NSDictionary, withCompletionHandler handler: @escaping CompletionHandler)
//    {
//        let webserviceCall = WebserviceCall()
//        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
//        
//        print("\(serviceURL) --- \(params)")
//        
//        webserviceCall.post(NSURL(string: serviceURL) as URL!, parameters: params , withSuccessHandler: { (response) -> Void in
//            
//                handler(response?.webserviceResponse as AnyObject?, true)
//
//        }) { (error) -> Void in
//            
//            handler(nil, false)
//            
//        }
//    }
    
    func requestforGET(params: [NSObject: AnyObject?], withCompletionHandler handler: @escaping CompletionHandler)
    {
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        print("\(serviceURL) --- \(params)")
        
        webserviceCall.post(NSURL(string: serviceURL) as URL!, parameters: nil, withSuccessHandler: { (response) -> Void in
            
            handler(response?.webserviceResponse as AnyObject?, true)
            
        }) { (error) -> Void in
            handler(nil, false)
        }
    }
    
    //        ProjectUtility.loadingShow()
    //
    //        let restApi = NSRestApiHelper()
    //        restApi.serviceURL = "/login/loginuser"
    //
    //        let dict = ["email_address":txtEmail.text!, "password":txtPassword.text!]
    //
    //        restApi.requestforPOST(params: dict as NSDictionary) { (response, success) in
    //
    //            ProjectUtility.loadingHide()
    //
    //            print(response?.webserviceResponse)
    //
    //            if success == true {
    //
    //                if let dictData = response?.webserviceResponse as? NSDictionary{
    //
    //                    ProjectUtility.displayTost(erroemessage: dictData.value(forKey: "message") as! String)
    //                    if dictData.value(forKey: "success") as! Bool == true{
    //
    //                        Constants.appDelegate.dictLoginUserDetail = (dictData.value(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary
    //
    //                        let archivedUser = NSKeyedArchiver.archivedData(withRootObject: Constants.appDelegate.dictLoginUserDetail)
    //                        UserDefaults.standard.setValue(archivedUser, forKey: "UserDetail")
    //                        UserDefaults.standard.synchronize()
    //
    //                        if Constants.appDelegate.dictLoginUserDetail.value(forKey: "is_contributed") as! String == "0"{
    //
    //                            self.performSegue(withIdentifier: "contribute", sender: nil)
    //                        }else{
    //                            self.performSegue(withIdentifier: "login", sender: nil)
    //                        }
    //                    }
    //
    //                    return
    //                }
    //            }
    //            
    //            ProjectUtility.displayTost(erroemessage: "We are having some problem")
    //            
    //        }
}
