//
//  KEZCornerCell.m
//  KEZCollectionViewTableLayout
//
//  Created by James Richard on 12/27/13.
//  Copyright (c) 2013 James Richard. All rights reserved.
//

#import "KEZCornerCell.h"

@implementation KEZCornerCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        self.lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        self.lblTitle.font = [UIFont boldSystemFontOfSize:13.0f];
        self.lblTitle.textAlignment = NSTextAlignmentCenter;
        [self.lblTitle setText:@"School Name"];
        [self addSubview:self.lblTitle];
        
        self.imgLineBottom = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.frame.size.height-1, self.frame.size.width, 1)];
        [self.imgLineBottom setBackgroundColor:[UIColor colorWithRed:36.0/255.0 green:159.0/255.0 blue:231.0/255.0 alpha:1.0]];
        [self addSubview:self.imgLineBottom];
        
        self.btnDropDown = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [self.btnDropDown setImage:[UIImage imageNamed:@"drop_down_blue_icon"] forState:UIControlStateNormal];
        self.btnDropDown.accessibilityIdentifier = @"School Name";
        [self.btnDropDown addTarget:self action:@selector(btnDropDown:) forControlEvents:UIControlEventTouchUpInside];
        
        CGSize textSize = [[self.lblTitle text] sizeWithAttributes:@{NSFontAttributeName:[self.lblTitle font]}];
        CGFloat strikeWidth = textSize.width;
        self.btnDropDown.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        self.btnDropDown.imageEdgeInsets = UIEdgeInsetsMake(0, (self.frame.size.width/2)+(strikeWidth/2)+2, 0, 0);
        [self addSubview:self.btnDropDown];
        
    }
    return self;
}

-(IBAction)btnDropDown:(UIButton*)sender{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:sender forKey:@"Button"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"opneSchoolNameDropDown" object:dict];
}

@end
