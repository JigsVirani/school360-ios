//
//  KEZLabeledHeaderView.m
//  KEZCollectionViewTableLayout
//
//  Created by James Richard on 12/22/13.
//  Copyright (c) 2013 James Richard. All rights reserved.
//

#import "KEZLabeledHeaderView.h"

@interface KEZLabeledHeaderView ()
@property (nonatomic, strong, readwrite) UILabel *label;
@end

@implementation KEZLabeledHeaderView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        self.label.backgroundColor = [UIColor clearColor];
        [self addSubview:self.label];
        
        self.btnDropDown = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [self.btnDropDown setImage:[UIImage imageNamed:@"drop_down_blue_icon"] forState:UIControlStateNormal];
        [self addSubview:self.btnDropDown];
        
    }
    return self;
}

- (void) prepareForReuse {
    [super prepareForReuse];
    self.label.text = nil;
}

- (void) updateConstraints {
    NSDictionary *views = @{@"label": self.label};
    [self addConstraints:[NSLayoutConstraint
                          constraintsWithVisualFormat:@"|[label]|"
                          options:0
                          metrics:nil
                          views:views]];
    
    [self addConstraints:[NSLayoutConstraint
                          constraintsWithVisualFormat:@"V:|[label]|"
                          options:0
                          metrics:nil
                          views:views]];
    [super updateConstraints];
}

- (UILabel *) label {
    if (!_label) {
        _label = [[UILabel alloc] init];
        _label.font = [UIFont boldSystemFontOfSize:13.0f];
        _label.translatesAutoresizingMaskIntoConstraints = NO;
        _label.textAlignment = NSTextAlignmentCenter;
    }
    
    return _label;
}

@end
