//
//  KEZTableDataSource.h
//  KEZCollectionViewTableLayout
//
//  Created by James Richard on 12/13/13.
//  Copyright (c) 2013 James Richard. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol SortingPopupDelegate <NSObject>

- (void)strSchoolName:(NSString*)strSchoolTitle;
- (void)opneDropDown:(UIButton*)btnDropDown;

@end


@interface KEZTableLabelDataSource : NSObject <UICollectionViewDataSource>

@property (nonatomic, assign) id <SortingPopupDelegate> sortingPopupDelegate;

+ (instancetype) sampleDataSourceWithRows:(NSMutableArray*)arrSchoolName columns:(NSMutableArray*)arrColumnName rowHeaders:(BOOL)rowHeaders columnHeaders:(BOOL)columnHeaders data:(NSMutableArray*)arrData dict:(NSMutableDictionary*)dictData;
- (NSString *) labelAtIndexPath:(NSIndexPath *)indexPath;
- (NSString *) columnHeaderTitleAtIndexPath:(NSIndexPath *)indexPath;
- (NSString *) rowHeaderTitleAtIndexPath:(NSIndexPath *)indexPath;

@property (nonatomic, strong) NSMutableArray *arrResources;
@property (nonatomic, strong) NSMutableArray *arrQuestions;

@end
