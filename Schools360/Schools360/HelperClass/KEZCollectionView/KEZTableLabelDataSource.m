//
//  KEZTableDataSource.m
//  KEZCollectionViewTableLayout
//
//  Created by James Richard on 12/13/13.
//  Copyright (c) 2013 James Richard. All rights reserved.
//

#import "KEZTableLabelDataSource.h"
#import "KEZLabelCell.h"
#import "KEZLabeledHeaderView.h"
#import "KEZCollectionViewTableLayout.h"

@interface KEZTableLabelDataSource ()
@property (nonatomic, strong) NSArray *rowTitles;
@property (nonatomic, strong) NSArray *columnTitles;
@property (nonatomic, strong) NSArray *labelStrings;
@end

@implementation KEZTableLabelDataSource
- (instancetype) init {
    return [self initWithRowTitles:nil columnTitles:nil labelStrings:@[]];
}

- (instancetype) initWithRowTitles:(NSArray *)rowTitles columnTitles:(NSArray *)columnTitles labelStrings:(NSArray *)labelStrings {
    if (self = [super init]) {
        _rowTitles = rowTitles;
        _columnTitles = columnTitles;
        _labelStrings = labelStrings;
    }
    
    return self;
}

+ (instancetype) sampleDataSourceWithRows:(NSMutableArray*)arrSchoolName columns:(NSMutableArray*)arrColumnName rowHeaders:(BOOL)rowHeaders columnHeaders:(BOOL)columnHeaders data:(NSMutableArray*)arrData dict:(NSMutableDictionary*)dictData {
    static NSNumberFormatter *formatter;
    if (!formatter) {
        formatter = [[NSNumberFormatter alloc] init];
        formatter.numberStyle = NSNumberFormatterSpellOutStyle;
    }
    
    NSMutableArray *rowTitles = [NSMutableArray array];
    NSMutableArray *rowHeaderTitles = rowHeaders ? [NSMutableArray array] : nil;
    NSMutableArray *columnHeaderTitles = columnHeaders ? [NSMutableArray array] : nil;
    
    for (int i = 0; i != arrSchoolName.count; i++) {
        NSMutableArray *row = [NSMutableArray array];
        
        [rowHeaderTitles addObject:[NSString stringWithFormat:@"Row %i", i]];
        
        for (int j = 0; j != arrColumnName.count; j++) {
            
            [columnHeaderTitles addObject:[arrColumnName objectAtIndex:j]];
            
            NSString *strKey = [dictData valueForKey:[arrColumnName objectAtIndex:j]];
            
            if (strKey != nil) {
                
                if (![strKey integerValue]) {
                    
                    if ([[arrData objectAtIndex:i] valueForKey:strKey]) {
                        [row addObject:[[arrData objectAtIndex:i] valueForKey:strKey]];
                    }else{
                        NSArray *arrResources = [[[arrData objectAtIndex:i] valueForKey:@"resources"] componentsSeparatedByString:@", "];
                        
                        NSUInteger index = [arrResources indexOfObject:strKey];
                        
//                        if ([strKey isEqualToString:@"Gymnasium"]) {
//                            
//                            NSLog(@"%@ %lu",strKey, (unsigned long)index);
//                        }
                        
                        if (index != NSNotFound) {
                            [row addObject:@"Available"];
                        }else{
                            [row addObject:@"Not Available"];
                        }
                    }
                }else{
                    
                    float floatValue = [[[[arrData objectAtIndex:i] valueForKey:@"questions"] valueForKey:strKey] floatValue];
                    [row addObject:[NSString stringWithFormat:@"%.1f",floatValue]];
                }
            }else{
                [row addObject:[NSString stringWithFormat:@"Column %i", j]];
            }
        }
        [rowTitles addObject:row];
    }
    
    return [[self alloc] initWithRowTitles:[arrSchoolName copy] columnTitles:[columnHeaderTitles copy] labelStrings:[rowTitles copy]];
}

- (NSString *) labelAtIndexPath:(NSIndexPath *)indexPath {
    return self.labelStrings[indexPath.section][indexPath.row];
}

- (NSString *) columnHeaderTitleAtIndexPath:(NSIndexPath *)indexPath {
    return self.columnTitles[indexPath.row];
}

- (NSString *) rowHeaderTitleAtIndexPath:(NSIndexPath *)indexPath {
    return self.rowTitles[indexPath.section];
}

#pragma mark - UICollectionViewDataSource
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"LabelCell";
    KEZLabelCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if ([self.labelStrings[indexPath.section][indexPath.row] isEqualToString:@"Available"]) {
        [cell.label setHidden: true];
        [cell.imgCheckBox setHidden: false];
        
        cell.imgCheckBox.image = [UIImage imageNamed:@"checked_box_advance_search"];
        
    }else if ([self.labelStrings[indexPath.section][indexPath.row] isEqualToString:@"Not Available"]) {
        [cell.label setHidden: true];
        [cell.imgCheckBox setHidden: false];
        
        cell.imgCheckBox.image = [UIImage imageNamed:@"cross_advance_search"];
    }else{
        [cell.label setHidden: false];
        [cell.imgCheckBox setHidden: true];
        
        cell.label.text = self.labelStrings[indexPath.section][indexPath.row];
    }
    return cell;
}

- (UICollectionReusableView *) collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"LabelHeader";
    KEZLabeledHeaderView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if ([kind isEqualToString:KEZCollectionViewTableLayoutSupplementaryViewColumnHeader]) {
        
        view.label.text = self.columnTitles[indexPath.row];
        view.label.textColor = [UIColor blackColor];
        
        
        NSUInteger indexResources = [_arrResources indexOfObject:self.columnTitles[indexPath.row]];
        NSUInteger indexQuestions = [_arrQuestions indexOfObject:self.columnTitles[indexPath.row]];
        
        
        if (indexResources == NSNotFound && indexQuestions == NSNotFound) {
            view.btnDropDown.hidden = false;
            
            [view.btnDropDown setFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
            view.btnDropDown.tag = 10000 + indexPath.row;
            view.btnDropDown.accessibilityIdentifier = self.columnTitles[indexPath.row];
            [view.btnDropDown addTarget:self action:@selector(btnDropDown:) forControlEvents:UIControlEventTouchUpInside];
            
            CGSize textSize = [[view.label text] sizeWithAttributes:@{NSFontAttributeName:[view.label font]}];
            CGFloat strikeWidth = textSize.width;
            view.btnDropDown.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            view.btnDropDown.imageEdgeInsets = UIEdgeInsetsMake(0, (view.frame.size.width/2)+(strikeWidth/2)+2, 0, 0);
        }else{
            view.btnDropDown.hidden = true;
        }
    }
    else if ([kind isEqualToString:KEZCollectionViewTableLayoutSupplementaryViewRowHeader]) {
     
        view.label.text = self.rowTitles[indexPath.section];
        view.label.textColor = [UIColor colorWithRed:36.0/255.0 green:159.0/255.0 blue:231.0/255.0 alpha:1.0];
        
        UIButton *btnSchoolTitle = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
        btnSchoolTitle.accessibilityIdentifier = self.rowTitles[indexPath.section];
        [btnSchoolTitle addTarget:self action:@selector(btnSchoolTitle:) forControlEvents:UIControlEventTouchUpInside];
        
        [view addSubview:btnSchoolTitle];
        
        view.btnDropDown.hidden = true;
    }
    
    UIImageView *imgLineBottom = [[UIImageView alloc] initWithFrame:CGRectMake(0, view.frame.size.height-1, view.frame.size.width, 1)];
    [imgLineBottom setBackgroundColor:[UIColor colorWithRed:36.0/255.0 green:159.0/255.0 blue:231.0/255.0 alpha:1.0]];
    [view addSubview:imgLineBottom];
    
    return view;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.labelStrings.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return ((NSArray*)self.labelStrings[section]).count;
}

-(IBAction)btnSchoolTitle:(UIButton*)sender{
    
    [self.sortingPopupDelegate strSchoolName:sender.accessibilityIdentifier];
}

-(IBAction)btnDropDown:(UIButton*)sender{
    [self.sortingPopupDelegate opneDropDown:sender];
}

@end
