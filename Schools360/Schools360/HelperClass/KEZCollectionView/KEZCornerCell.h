//
//  KEZCornerCell.h
//  KEZCollectionViewTableLayout
//
//  Created by James Richard on 12/27/13.
//  Copyright (c) 2013 James Richard. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KEZCornerCell : UICollectionReusableView

@property (nonatomic, strong, readwrite) UILabel *lblTitle;
@property (nonatomic, strong, readwrite) UIImageView *imgLineBottom;
@property (nonatomic, strong, readwrite) UIButton *btnDropDown;

@end
