//
//  GeneralInformationVC.swift
//  Schools360
//
//  Created by Sandeep Gangajaliya on 16/04/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit

class GeneralInformationVC: UIViewController {

    //MARK:- Outlet Declartion
    @IBOutlet var btnContinueToMyPackage: UIButton!
    
    @IBOutlet var txtSchoolName: SearchTextField!
    @IBOutlet var txtCountry: SearchTextField!
    @IBOutlet var txtState: SearchTextField!
    @IBOutlet var txtCity: SearchTextField!
    @IBOutlet var txtCurriculumTaught: SearchTextField!
    @IBOutlet var txtSchoolSize: UITextField!
    
    @IBOutlet var imgSchoolName: UIImageView!
    @IBOutlet var imgCountry: UIImageView!
    @IBOutlet var imgState: UIImageView!
    @IBOutlet var imgCity: UIImageView!
    @IBOutlet var imgCurruculumTaught: UIImageView!
    
    @IBOutlet var btnSchoolSize: UIButton!
    @IBOutlet var btnCurriculum: UIButton!
    
    @IBOutlet var constButtonCointinueHeight: NSLayoutConstraint!
    
    //MARK:Other Varibles
    var arrCurriculumTaught: NSArray = ["PHP", "MYP", "IBDP", "British", "GCSE / IGCSE", "A-Level", "American", "AP", "Indian", "Canadian", "Australian", "Post secondary/college", "Higher secondary/university", "Other"]
    var arrSchoolSize: NSArray = ["Less than 500 students", "500 - 900 students","1000 - 1500 students","More than 1500"]
    var dictSelectedSchool = NSMutableDictionary()
    
    //MARK:TypeAndSearch
    var countryResults = [SearchTextFieldItem]()
    var stateResults = [SearchTextFieldItem]()
    var cityResults = [SearchTextFieldItem]()
    
    //MARK:DropDowns
    let schoolSizeDropDown = DropDown()
    let curriculumDropDown = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.schoolSizeDropDown, self.curriculumDropDown
        ]
    }()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialization()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK:- Initialization
    
    func initialization(){
        
        ProjectUtility.setCommonButton(button: btnContinueToMyPackage)
        
        setupDropDowns()
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        
        DropDown.setupDefaultAppearance()
        
        self.configureSchoolSearchTextField()
        self.configureCountrySearchTextField()
        self.configureStateSearchTextField()
        self.configureCitySearchTextField()
        
        let alert = UIAlertController(title: "", message: "We know that teacher's packages vary greatly regarding specifics.\n\nPlease choose the options that are closest to your particular situation during this registration process.\n\nOnce completed, you can then discuss the specifics in the forum area.\n\nMany Thanks!", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
        constButtonCointinueHeight.constant = Constants.ScreenSize.SCREEN_HEIGHT-85-60
    }
    
    func setupDropDowns() {
        setupSchoolSizeDropDown()
        setupCurriculumDropDown()
    }
    
    func setupSchoolSizeDropDown() {
        schoolSizeDropDown.anchorView = btnSchoolSize
        schoolSizeDropDown.bottomOffset = CGPoint(x: 0, y: 40)
        
        schoolSizeDropDown.dataSource = arrSchoolSize as! [String]
        
        schoolSizeDropDown.selectionAction = { [unowned self] (index, item) in
            self.txtSchoolSize.text = item
            self.txtSchoolSize.tag = index + 1
        }
    }
    
    func setupCurriculumDropDown() {
        curriculumDropDown.anchorView = btnCurriculum
        curriculumDropDown.bottomOffset = CGPoint(x: 0, y: 40)
        
        curriculumDropDown.dataSource = arrCurriculumTaught as! [String]
        
        curriculumDropDown.selectionAction = { [unowned self] (index, item) in
            self.txtCurriculumTaught.text = item
            self.txtCurriculumTaught.tag = index + 1
        }
    }
    
    //MARK:- Webservice Call
    fileprivate func searchSchoolNameJson(_ criteria: String, callback: @escaping ((_ results: [SearchTextFieldItem]) -> Void)) {
    
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        let dict = ["terms":txtSchoolName.text!]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/home/searchsuggestion"
        print("\(strUrl) --- \(dict)")
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            print(response?.webserviceResponse)
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        var results = [SearchTextFieldItem]()

                        for result in dictData.value(forKey: "data") as! NSArray {
                            results.append(SearchTextFieldItem(title: (result as! NSDictionary).value(forKey: "school_name") as! String, subtitle: "", image: nil, id: (result as! NSDictionary).value(forKey: "id") as? String))
                        }
                    
                        DispatchQueue.main.async {
                            callback(results)
                        }
                    }else {
                        DispatchQueue.main.async {
                            callback([])
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        callback([])
                    }
                }
            }else {
                DispatchQueue.main.async {
                    callback([])
                }
            }
            
        }) { (error) -> Void in
            
            print(error)
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    fileprivate func searchCountryJson(_ criteria: String, callback: @escaping ((_ results: [SearchTextFieldItem]) -> Void)) {
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/home/countrylist"
        print("\(strUrl)")
        
        webserviceCall.get(NSURL(string: strUrl) as URL!, parameters: nil as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            print(response?.webserviceResponse)
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        for result in dictData.value(forKey: "data") as! NSArray {
                            self.countryResults.append(SearchTextFieldItem(title: (result as! NSDictionary).value(forKey: "name") as! String, subtitle: "", image: nil, id: (result as! NSDictionary).value(forKey: "id") as? String))
                        }
                        
                        DispatchQueue.main.async {
                            callback(self.countryResults)
                        }
                    }else {
                        DispatchQueue.main.async {
                            callback([])
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        callback([])
                    }
                }
            }else {
                DispatchQueue.main.async {
                    callback([])
                }
            }
            
        }) { (error) -> Void in
            
            print(error)
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    fileprivate func searchStateJson(_ criteria: String, callback: @escaping ((_ results: [SearchTextFieldItem]) -> Void)) {
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        let dict = ["country_id":txtCountry.accessibilityIdentifier!]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/home/getstate"
        print("\(strUrl) --- \(dict)")
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            print(response?.webserviceResponse)
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        for result in dictData.value(forKey: "data") as! NSArray {
                            self.stateResults.append(SearchTextFieldItem(title: (result as! NSDictionary).value(forKey: "name") as! String, subtitle: "", image: nil, id: (result as! NSDictionary).value(forKey: "id") as? String))
                        }
                        
                        DispatchQueue.main.async {
                            callback(self.stateResults)
                        }
                    }else {
                        DispatchQueue.main.async {
                            callback([])
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        callback([])
                    }
                }
            }else {
                DispatchQueue.main.async {
                    callback([])
                }
            }
            
        }) { (error) -> Void in
            
            print(error)
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    fileprivate func searchCityJson(_ criteria: String, callback: @escaping ((_ results: [SearchTextFieldItem]) -> Void)) {
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        let dict = ["state_id":txtState.accessibilityIdentifier!]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/home/getcity"
        print("\(strUrl) --- \(dict)")
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            print(response?.webserviceResponse)
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        for result in dictData.value(forKey: "data") as! NSArray {
                            self.cityResults.append(SearchTextFieldItem(title: (result as! NSDictionary).value(forKey: "name") as! String, subtitle: "", image: nil, id: (result as! NSDictionary).value(forKey: "id") as? String))
                        }
                        
                        DispatchQueue.main.async {
                            callback(self.stateResults)
                        }
                    }else {
                        DispatchQueue.main.async {
                            callback([])
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        callback([])
                    }
                }
            }else {
                DispatchQueue.main.async {
                    callback([])
                }
            }
            
        }) { (error) -> Void in
            
            print(error)
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    func editSchoolInfoJson()
    {
        ProjectUtility.loadingShow()
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        let dict = ["school_name":txtSchoolName.accessibilityIdentifier!]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/home/editschoolinfo"
        print("\(strUrl) --- \(dict)")
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            ProjectUtility.loadingHide()
            
            print(response?.webserviceResponse)
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{

                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        self.dictSelectedSchool = (dictData.value(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        
                        self.dictSelectedSchool = ObjectiveCMethods.dictionaryByReplacingNulls(withBlanks: dictData.value(forKey: "data") as! NSDictionary? as? [AnyHashable: Any] ?? [:])
                        
                        self.txtCountry.text = self.dictSelectedSchool.value(forKey: "country_text_name") as? String
                        self.txtCountry.accessibilityIdentifier = self.dictSelectedSchool.value(forKey: "country_name") as? String
                        
                        self.txtState.text = self.dictSelectedSchool.value(forKey: "state_text_name") as? String
                        self.txtState.accessibilityIdentifier = self.dictSelectedSchool.value(forKey: "state_name") as? String
                        
                        self.txtCity.text = self.dictSelectedSchool.value(forKey: "city_text_name") as? String
                        self.txtCity.accessibilityIdentifier = self.dictSelectedSchool.value(forKey: "city_name") as? String
                        
                        for i in 0 ..< self.arrCurriculumTaught.count{
                            
                            if i+1 == Int(self.dictSelectedSchool.value(forKey: "tech_name") as! String) {
                                
                                self.txtCurriculumTaught.text = self.arrCurriculumTaught.object(at: i) as? String
                                self.txtCurriculumTaught.tag = i + 1
                            }
                        }
                        
                        for i in 0 ..< self.arrSchoolSize.count{
                            
                            if i+1 == Int(self.dictSelectedSchool.value(forKey: "school_size") as! String){
                                
                                self.txtSchoolSize.text = self.arrSchoolSize.object(at: i) as? String
                                self.txtSchoolSize.tag = i + 1
                            }
                        }
                    }
                    
                    return
                }
            }
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
            
        }) { (error) -> Void in
            
            print(error)
            ProjectUtility.loadingHide()
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }

    // MARK:- Button TouchUp
    
    @IBAction func btnSchoolSizeAction (_ sender: UIButton){
        self.view.endEditing(true)
        schoolSizeDropDown.show()
    }
    
    @IBAction func btnCurriculumTaughtAction (_ sender: UIButton){
        self.view.endEditing(true)
        curriculumDropDown.show()
    }
    
    @IBAction func btnContinueToMyPackageAction (_ sender: UIButton){
        self.view.endEditing(true)
        if txtSchoolName.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please enter school name")
        }else if txtCountry.text == ""{
            ProjectUtility.displayTost(erroemessage: "Please enter country name")
        }else if txtState.text == ""{
            ProjectUtility.displayTost(erroemessage: "Please enter state name")
        }else if txtCity.text == ""{
            ProjectUtility.displayTost(erroemessage: "Please enter city name")
        }else if txtCurriculumTaught.text == ""{
            ProjectUtility.displayTost(erroemessage: "Please select Curriculum taught")
        }else if txtSchoolSize.text == ""{
            ProjectUtility.displayTost(erroemessage: "Please select school size")
        }else{
            if txtSchoolName.accessibilityIdentifier == nil || txtSchoolName.accessibilityIdentifier == "" {
                self.performSegue(withIdentifier: "confirmSchool", sender: nil)
            }else{
                self.performSegue(withIdentifier: "myPackage", sender: nil)
            }
        }
    }
    
    //MARK:- Set Suggetion Box
    fileprivate func configureSchoolSearchTextField() {
        
        txtSchoolName.maxNumberOfResults = 100
        txtSchoolName.maxResultsListHeight = 200
        txtSchoolName.theme.font = UIFont.init(name: "Avenir Next", size: 12)!
        
        txtSchoolName.itemSelectionHandler = { item, itemPosition in
            
            self.view.endEditing(true)
            self.txtSchoolName.text = item.title
            self.txtSchoolName.accessibilityIdentifier = item.id
            
            self.editSchoolInfoJson()
        }
        
        txtSchoolName.userStoppedTypingHandler = {
            self.txtSchoolName.accessibilityIdentifier = ""
            if let criteria = self.txtSchoolName.text {
                if criteria.characters.count > 1 {
                    
                    self.txtSchoolName.showLoadingIndicator()
                    
                    self.searchSchoolNameJson(criteria) { results in
                    
                        self.txtSchoolName.filterItems(results)
                        self.txtSchoolName.stopLoadingIndicator()
                    }
                }
            }
        }
    }
    
    fileprivate func configureCountrySearchTextField() {
        
        txtCountry.maxNumberOfResults = 100
        txtCountry.maxResultsListHeight = 200
        txtCountry.theme.font = UIFont.init(name: "Avenir Next", size: 12)!
        
        txtCountry.itemSelectionHandler = { item, itemPosition in
            
            self.view.endEditing(true)
            self.txtCountry.text = item.title
            self.txtCountry.accessibilityIdentifier = item.id
            self.stateResults = [SearchTextFieldItem]()
            self.cityResults = [SearchTextFieldItem]()
        }
        
        txtCountry.userStoppedTypingHandler = {
            self.txtCountry.accessibilityIdentifier = ""
            self.txtState.text = ""
            self.txtState.accessibilityIdentifier = ""
            self.txtCity.text = ""
            self.txtCity.accessibilityIdentifier = ""
            
            if let criteria = self.txtCountry.text {
                if criteria.characters.count > 1 {
                    
                    if self.countryResults.count == 0 {
                        self.txtCountry.showLoadingIndicator()
                        
                        self.searchCountryJson(criteria) { results in
                            
                            self.txtCountry.filterItems(results)
                            self.txtCountry.stopLoadingIndicator()
                        }
                    }else{
                        self.txtCountry.showLoadingIndicator()
                        self.txtCountry.filterItems(self.countryResults)
                        self.txtCountry.stopLoadingIndicator()
                    }
                }
            }
        }
    }
    
    fileprivate func configureStateSearchTextField() {
        
        txtState.maxNumberOfResults = 100
        txtState.maxResultsListHeight = 200
        txtState.theme.font = UIFont.init(name: "Avenir Next", size: 12)!
        
        txtState.itemSelectionHandler = { item, itemPosition in
            
            self.view.endEditing(true)
            self.txtState.text = item.title
            self.txtState.accessibilityIdentifier = item.id
            
            self.cityResults = [SearchTextFieldItem]()
        }
        
        txtState.userStoppedTypingHandler = {
            self.txtState.accessibilityIdentifier = ""
            self.txtCity.text = ""
            self.txtCity.accessibilityIdentifier = ""
            if let criteria = self.txtState.text {
                if criteria.characters.count > 1 {
                    
                    if self.stateResults.count == 0 {
                        self.txtState.showLoadingIndicator()
                        
                        self.searchStateJson(criteria) { results in
                            
                            self.txtState.filterItems(results)
                            self.txtState.stopLoadingIndicator()
                        }
                    }else{
                        self.txtState.showLoadingIndicator()
                        self.txtState.filterItems(self.stateResults)
                        self.txtState.stopLoadingIndicator()
                    }
                }
            }
        }
    }
    
    fileprivate func configureCitySearchTextField() {
        
        txtCity.maxNumberOfResults = 100
        txtCity.maxResultsListHeight = 200
        txtCity.theme.font = UIFont.init(name: "Avenir Next", size: 12)!
        
        txtCity.itemSelectionHandler = { item, itemPosition in
            
            self.view.endEditing(true)
            self.txtCity.text = item.title
            self.txtCity.accessibilityIdentifier = item.id
        }
        
        txtCity.userStoppedTypingHandler = {
            self.txtCity.accessibilityIdentifier = ""
            if let criteria = self.txtCity.text {
                if criteria.characters.count > 1 {
                    
                    if self.cityResults.count == 0 {
                        self.txtCity.showLoadingIndicator()
                        
                        self.searchCityJson(criteria) { results in
                            
                            self.txtCity.filterItems(results)
                            self.txtCity.stopLoadingIndicator()
                        }
                    }else{
                        self.txtCity.showLoadingIndicator()
                        self.txtCity.filterItems(self.cityResults)
                        self.txtCity.stopLoadingIndicator()
                    }
                }
            }
        }
    }
    
    // MARK:- TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtSchoolName {
            self.changeTextFiledLineColor(textField, imgSchoolName)
        }else if textField == txtCountry {
            self.changeTextFiledLineColor(textField, imgCountry)
        }else if textField == txtState {
            self.changeTextFiledLineColor(textField, imgState)
        }else if textField == txtCity {
            self.changeTextFiledLineColor(textField, imgCity)
        }else if textField == txtCurriculumTaught {
            self.changeTextFiledLineColor(textField, imgCurruculumTaught)
        }
    }
    
    @IBAction func textFieldEditingChanged(_ textField: UITextField) {
        
        if textField == txtSchoolName {
            self.changeTextFiledLineColor(textField, imgSchoolName)
        }else if textField == txtCountry {
            self.changeTextFiledLineColor(textField, imgCountry)
        }else if textField == txtState {
            if txtCountry.text == "" {
                txtState.resignFirstResponder()
                txtState.text = ""
                ProjectUtility.displayTost(erroemessage: "Please enter country name")
                return
            }else if txtCountry.accessibilityIdentifier == ""{
                txtState.resignFirstResponder()
                txtState.text = ""
                ProjectUtility.displayTost(erroemessage: "Please select country from drop down box")
                return
            }
            self.changeTextFiledLineColor(textField, imgState)
        }else if textField == txtCity {
            if txtState.text == "" {
                self.view.endEditing(true)
                txtCity.text = ""
                ProjectUtility.displayTost(erroemessage: "Please enter state name")
                return
            }
            self.changeTextFiledLineColor(textField, imgCity)
        }else if textField == txtCurriculumTaught {
            self.changeTextFiledLineColor(textField, imgCurruculumTaught)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        imgSchoolName.backgroundColor = UIColor.lightGray
        imgCountry.backgroundColor = UIColor.lightGray
        imgState.backgroundColor = UIColor.lightGray
        imgCity.backgroundColor = UIColor.lightGray
        imgCurruculumTaught.backgroundColor = UIColor.lightGray
    }
    
    func changeTextFiledLineColor(_ textField: UITextField, _ imageView: UIImageView){
        
        if textField.text == "" {
            imageView.backgroundColor = UIColor.lightGray
        }else{
            imageView.backgroundColor = UIColor.init(red: 99.0 / 255.0, green: 160.0 / 255.0, blue: 248.0 / 255.0, alpha: 1.0)
        }
    }
    
    // MARK:- Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let dictDetail = NSMutableDictionary()
        
        dictDetail.setValue(txtSchoolName.text, forKey: "school_name")
        dictDetail.setValue(txtSchoolName.accessibilityIdentifier, forKey: "school_id")
        dictDetail.setValue(txtCountry.accessibilityIdentifier, forKey: "country_id")
        dictDetail.setValue(txtState.text, forKey: "state_name")
        dictDetail.setValue(txtState.accessibilityIdentifier, forKey: "state_id")
        dictDetail.setValue(txtCity.text, forKey: "city_name")
        dictDetail.setValue(txtCity.accessibilityIdentifier, forKey: "city_id")
        dictDetail.setValue(txtCurriculumTaught.tag, forKey: "tech_id")
        dictDetail.setValue(txtSchoolSize.tag, forKey: "school_size")
        
        if segue.identifier == "confirmSchool" {
            
            let vc: ConfirmSchoolVC = segue.destination as! ConfirmSchoolVC
            vc.dictDetail = dictDetail
            
        }else{
            
            let vc: MyPackageVC = segue.destination as! MyPackageVC
            vc.dictDetail = dictDetail
            vc.self.dictSelectedSchool = self.dictSelectedSchool
        }
    }

}
