//
//  ConfirmSchoolVC.swift
//  Schools360
//
//  Created by Sandeep Gangajaliya on 16/04/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit

class ConfirmSchoolVC: UIViewController {

    //MARK:- Outlet Declartion
    @IBOutlet var tblCofirmSchool: UITableView!
    
    @IBOutlet var btnConfirm: UIButton!
    
    @IBOutlet var txtSchoolName: UITextField!
    @IBOutlet var imgSchoolName: UIImageView!
    
    //MARK: Variable Declartion
    var dictDetail = NSMutableDictionary()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ProjectUtility.setCommonButton(button: btnConfirm)
        
        tblCofirmSchool.rowHeight = UITableViewAutomaticDimension
        tblCofirmSchool.estimatedRowHeight = 118.0
        
        txtSchoolName.text = dictDetail.value(forKey: "school_name") as? String
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK:- Button TouchUp
    
    @IBAction func btnBackAction (_ sender: UIButton){
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEditAction (_ sender: UIButton){
        
        txtSchoolName.isEnabled = true
        txtSchoolName.becomeFirstResponder()
    }
    
    @IBAction func btnConfirmAction (_ sender: UIButton){
        
        if txtSchoolName.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please enter school name")
            return
        }
        
        self.performSegue(withIdentifier: "myPackage", sender: nil)
    }
    
    //MARK:- Tableview Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        
        let cell:ThankYouTableCell = tableView.dequeueReusableCell(withIdentifier: "ThankYouTableCell", for: indexPath as IndexPath) as! ThankYouTableCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell.imgBorder.layer.cornerRadius = 5.0
        cell.imgBorder.clipsToBounds = true
        
        cell.imgBorder.layer.borderWidth = 1.0
        cell.imgBorder.layer.borderColor = UIColor.init(red: 86.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        
        return cell;
    }
    
    // MARK:- TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtSchoolName {
            self.changeTextFiledLineColor(textField, imgSchoolName)
        }
    }
    
    @IBAction func textFieldEditingChanged(_ textField: UITextField) {
        
        if textField == txtSchoolName {
            self.changeTextFiledLineColor(textField, imgSchoolName)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        imgSchoolName.backgroundColor = UIColor.lightGray
    }
    
    func changeTextFiledLineColor(_ textField: UITextField, _ imageView: UIImageView){
        
        if textField.text == "" {
            imageView.backgroundColor = UIColor.lightGray
        }else{
            imageView.backgroundColor = UIColor.init(red: 99.0 / 255.0, green: 160.0 / 255.0, blue: 248.0 / 255.0, alpha: 1.0)
        }
    }
    
    // MARK:- Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
        dictDetail.setValue(txtSchoolName.text, forKey: "school_name")
        
        let vc: MyPackageVC = segue.destination as! MyPackageVC
        vc.dictDetail = dictDetail
    }
}
