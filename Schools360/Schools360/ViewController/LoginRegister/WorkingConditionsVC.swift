//
//  WorkingConditionsVC.swift
//  Schools360
//
//  Created by Sandeep Gangajaliya on 17/04/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit


class WorkingConditionsVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {

    //MARK:- Outlet Declartion
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var colWorkingCondition: UICollectionView!
    var tblQues = UITableView()
    
    //MARK: Variables Declartion
    var arrQuestion = NSMutableArray()
    var arrTableQuestion = NSMutableArray()
    var dictDetail = NSMutableDictionary()
    var dictSelectedSchool = NSMutableDictionary()
    
    var lastContentOffsetCollectionView = 0
    var visibleCell = 0
    fileprivate (set) var scrollBar : SwiftyVerticalScrollBar!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.createQuestionsArray()
        self.createTableQuestionArray()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    // MARK:- Webservice Call
    
    func saveSchoolJson()
    {
        ProjectUtility.loadingShow()
        
        var strQuestionArray = "{"
        for i in 0 ..< arrQuestion.count {
            
            if i != 7 {
                
                if strQuestionArray != "{" {
                   strQuestionArray = strQuestionArray + ","
                }
                
                var strSelectedQue1 = ""
                
                if (arrQuestion.object(at: i) as! NSMutableDictionary).value(forKey: "SelectedQue1") as! String == "Agree" {
                    strSelectedQue1 = "1"
                    
                }else if (arrQuestion.object(at: i) as! NSMutableDictionary).value(forKey: "SelectedQue1") as! String == "Tend to agree" {
                    strSelectedQue1 = "2"
                    
                }else if (arrQuestion.object(at: i) as! NSMutableDictionary).value(forKey: "SelectedQue1") as! String == "Tend to disagree" {
                    strSelectedQue1 = "3"
                    
                }else if (arrQuestion.object(at: i) as! NSMutableDictionary).value(forKey: "SelectedQue1") as! String == "Disagree" {
                    strSelectedQue1 = "4"
                }
                
                if i < 7 {
                    
                    strQuestionArray = strQuestionArray + "\"\((i*2)+1)\":\"\(strSelectedQue1)\""
                }else{
                    strQuestionArray = strQuestionArray + "\"\((i*2)-1)\":\"\(strSelectedQue1)\""
                }
                
                if i != arrQuestion.count - 1 {
                    
                    strQuestionArray = strQuestionArray + ","
                    
                    var strSelectedQue2 = ""
                    if (arrQuestion.object(at: i) as! NSMutableDictionary).value(forKey: "SelectedQue2") as! String == "Agree" {
                        strSelectedQue2 = "1"
                        
                    }else if (arrQuestion.object(at: i) as! NSMutableDictionary).value(forKey: "SelectedQue2") as! String == "Tend to agree" {
                        strSelectedQue2 = "2"
                        
                    }else if (arrQuestion.object(at: i) as! NSMutableDictionary).value(forKey: "SelectedQue2") as! String == "Tend to disagree" {
                        strSelectedQue2 = "3"
                        
                    }else if (arrQuestion.object(at: i) as! NSMutableDictionary).value(forKey: "SelectedQue2") as! String == "Disagree" {
                        strSelectedQue2 = "4"
                    }
                    
                    if i < 7 {
                        strQuestionArray = strQuestionArray + "\"\((i*2)+2)\":\"\(strSelectedQue2)\""
                    }else{
                        strQuestionArray = strQuestionArray + "\"\(i*2)\":\"\(strSelectedQue2)\""
                    }
                }
            }
        }
        strQuestionArray = strQuestionArray + "}"
        
        var strResourcesId = ""
        for i in 0 ..< arrTableQuestion.count {
            
            if (arrTableQuestion.object(at: i) as! NSMutableDictionary).value(forKey: "isSelected") != nil && (arrTableQuestion.object(at: i) as! NSMutableDictionary).value(forKey: "isSelected") as! String == "true" {
                
                if strResourcesId != "" {
                    strResourcesId = strResourcesId + ","
                }
                
                strResourcesId = strResourcesId + "\(i+1)"
            }
        }
        
        var strScoolExist = "1"
        var strSchoolName = ""
        if dictDetail.value(forKey: "school_id") as! String == "" {
            strScoolExist = "0"
            strSchoolName = dictDetail.value(forKey: "school_name") as! String
        }else{
            strSchoolName = dictDetail.value(forKey: "school_id") as! String
        }
        
        var strStateExist = "1"
        var strStateId = dictDetail.value(forKey: "state_id") as! String
        if dictDetail.value(forKey: "state_id") as! String == "" {
            strStateExist = "0"
            strStateId = dictDetail.value(forKey: "state_name") as! String
        }
        
        var strCityExist = "1"
        var strCityId = dictDetail.value(forKey: "city_id") as! String
        if dictDetail.value(forKey: "city_id") as! String == "" {
            strCityExist = "0"
            strCityId = dictDetail.value(forKey: "city_name") as! String
        }
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        
        
        let dict = ["school_name":strSchoolName,"country_id":dictDetail.value(forKey: "country_id")!,"state_id":strStateId,"city_id":strCityId,"tech_id":dictDetail.value(forKey: "tech_id")!,"school_size":dictDetail.value(forKey: "school_size")!,"main_role_id":"0","salary_low":dictDetail.value(forKey: "salary_low")!,"salary_high":dictDetail.value(forKey: "salary_high")!,"responsiblity_point":dictDetail.value(forKey:"responsiblity_point")!,"housing_allowance_id":dictDetail.value(forKey:"housing_allowance_id")!,"school_fees":dictDetail.value(forKey:"school_fees")!,"health_ins_id":dictDetail.value(forKey: "health_ins_id")!,"flight_allowance":dictDetail.value(forKey: "flight_allowance")!,"gratuity_id":dictDetail.value(forKey: "gratuity_id")!,"shipping_allowance_id":dictDetail.value(forKey: "shipping_allowance_id")!,"annual_leave_id":dictDetail.value(forKey: "annual_leave_id")!,"resources_id":strResourcesId,"question_array":strQuestionArray,"is_school_exist":strScoolExist,"is_state_exist":strStateExist,"is_city_exist":strCityExist,"monthly_salary":"0", "user_id": Constant.appDelegate.dictLoginUserDetail.value(forKey: "user_id") as! String]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/home/saveschool"
        print("\(strUrl) --- \(dict)")
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            ProjectUtility.loadingHide()
            
            print(response?.webserviceResponse)
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        self.performSegue(withIdentifier: "contributionComplete", sender: nil)
                        return
                    }
                    ProjectUtility.displayTost(erroemessage: dictData.value(forKey: "message") as! String)
                    return
                }
            }
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
            
        }) { (error) -> Void in
            
            print(error)
            ProjectUtility.loadingHide()
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    // MARK:- Button TouchUp
    @IBAction func btnNextPageAction (_ sender: UIButton){
        
        for cell in colWorkingCondition.visibleCells {
            let currentIndex = colWorkingCondition.indexPath(for: cell)
            
            if currentIndex?.row != arrQuestion.count - 1 {
                let nextIndex = IndexPath.init(item: (currentIndex?.row)!+1, section: 0)
                self.colWorkingCondition.scrollToItem(at: nextIndex as IndexPath, at: UICollectionViewScrollPosition.right, animated: true)
                
                if nextIndex.row == 7 {
                    lblTitle.text = "Resources"
                }else if nextIndex.row < 7 {
                    lblTitle.text = "Working Conditions"
                }else{
                    lblTitle.text = "Living Conditions"
                }
            }
            
            break
        }
    }
    
    
    // MARK:Cell Event
    @IBAction func btnQuestion1Action (_ sender: UIButton){
        
        (arrQuestion.object(at: sender.tag) as! NSMutableDictionary).setValue(sender.accessibilityIdentifier, forKey: "SelectedQue1")
        
        (arrQuestion.object(at: sender.tag) as! NSMutableDictionary).setValue("true", forKey: "isSelected1")
        
        colWorkingCondition.reloadData()
    }
    
    @IBAction func btnQuestion2Action (_ sender: UIButton){
        
        (arrQuestion.object(at: sender.tag) as! NSMutableDictionary).setValue(sender.accessibilityIdentifier, forKey: "SelectedQue2")
        
        (arrQuestion.object(at: sender.tag) as! NSMutableDictionary).setValue("true", forKey: "isSelected2")
        
        colWorkingCondition.reloadData()
    }
    
    @IBAction func btnTableQueAction (_ sender: UIButton){
        
        if (arrTableQuestion.object(at: sender.tag) as! NSMutableDictionary).value(forKey: "isSelected") != nil && (arrTableQuestion.object(at: sender.tag) as! NSMutableDictionary).value(forKey: "isSelected") as! String == "true"{
            
            (arrTableQuestion.object(at: sender.tag) as! NSMutableDictionary).setValue("false", forKey: "isSelected")
        }else{
            
            (arrTableQuestion.object(at: sender.tag) as! NSMutableDictionary).setValue("true", forKey: "isSelected")
        }
        
        colWorkingCondition.reloadData()
    }
    
    @IBAction func btnSubmitAction (_ sender: UIButton){
        
        self.saveSchoolJson()
    }
    
    //MARK:- CollectionView Delegate
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrQuestion.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        
        if indexPath.row == 7 {
            
            let cell : Question8CollectionCell = colWorkingCondition.dequeueReusableCell(withReuseIdentifier: "Question8CollectionCell", for: indexPath) as! Question8CollectionCell
            
            cell.imgQuestionBg.layer.shadowRadius = 6
            cell.imgQuestionBg.layer.shadowOffset = CGSize.init(width: 0, height: 8)
            cell.imgQuestionBg.layer.shadowColor = UIColor.lightGray.cgColor
            cell.imgQuestionBg.layer.shadowOpacity = 0.75
            
            cell.tblQuestion8.delegate = self
            cell.tblQuestion8.dataSource = self
            cell.tblQuestion8.reloadData()
            cell.tblQuestion8.tag = 1000
            
            cell.imgArrow.image = UIImage.gifImageWithName("down_arrow")
            
            self.scrollBar = SwiftyVerticalScrollBar(frame: CGRect.zero, targetScrollView: cell.tblQuestion8)
            self.view.addSubview(self.scrollBar!)
            
//            cell.tblQuestion8.rowHeight = UITableViewAutomaticDimension
//            cell.tblQuestion8.estimatedRowHeight = 533.0
            
            return cell
        }else{
            
            let cell : WorkingConditionsCollectionCell = colWorkingCondition.dequeueReusableCell(withReuseIdentifier: "WorkingConditionsCollectionCell", for: indexPath) as! WorkingConditionsCollectionCell
            
            cell.lblQuestion1.text = (arrQuestion.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "Question1") as? String
            
            cell.lblQuestion2.text = (arrQuestion.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "Question2") as? String
            
            if indexPath.row == arrQuestion.count - 1 {
                cell.viewQuestion2.isHidden = true
            }else{
                cell.viewQuestion2.isHidden = false
            }
            
            cell.btnAgreeQue1.tag = indexPath.row
            cell.btnTendsToAgreeQue1.tag = indexPath.row
            cell.btnTendsToDisagreeQue1.tag = indexPath.row
            cell.btnDisagreeQue1.tag = indexPath.row
            
            cell.btnAgreeQue1.accessibilityIdentifier = "Agree"
            cell.btnTendsToAgreeQue1.accessibilityIdentifier = "Tend to agree"
            cell.btnTendsToDisagreeQue1.accessibilityIdentifier = "Tend to disagree"
            cell.btnDisagreeQue1.accessibilityIdentifier = "Disagree"
            
            cell.btnAgreeQue2.tag = indexPath.row
            cell.btnTendsToAgreeQue2.tag = indexPath.row
            cell.btnTendsToDisagreeQue2.tag = indexPath.row
            cell.btnDisagreeQue2.tag = indexPath.row
            
            cell.btnAgreeQue2.accessibilityIdentifier = "Agree"
            cell.btnTendsToAgreeQue2.accessibilityIdentifier = "Tend to agree"
            cell.btnTendsToDisagreeQue2.accessibilityIdentifier = "Tend to disagree"
            cell.btnDisagreeQue2.accessibilityIdentifier = "Disagree"
            
            cell.btnAgreeQue1.isSelected = false
            cell.btnTendsToAgreeQue1.isSelected = false
            cell.btnTendsToDisagreeQue1.isSelected = false
            cell.btnDisagreeQue1.isSelected = false
            
            cell.btnAgreeQue2.isSelected = false
            cell.btnTendsToAgreeQue2.isSelected = false
            cell.btnTendsToDisagreeQue2.isSelected = false
            cell.btnDisagreeQue2.isSelected = false
            
            cell.btnAgreeQue1.addTarget(self, action: #selector(WorkingConditionsVC.btnQuestion1Action(_ :)), for: UIControlEvents.touchUpInside)
            cell.btnTendsToAgreeQue1.addTarget(self, action: #selector(WorkingConditionsVC.btnQuestion1Action(_ :)), for: UIControlEvents.touchUpInside)
            cell.btnTendsToDisagreeQue1.addTarget(self, action: #selector(WorkingConditionsVC.btnQuestion1Action(_ :)), for: UIControlEvents.touchUpInside)
            cell.btnDisagreeQue1.addTarget(self, action: #selector(WorkingConditionsVC.btnQuestion1Action(_ :)), for: UIControlEvents.touchUpInside)
            
            cell.btnAgreeQue2.addTarget(self, action: #selector(WorkingConditionsVC.btnQuestion2Action(_ :)), for: UIControlEvents.touchUpInside)
            cell.btnTendsToAgreeQue2.addTarget(self, action: #selector(WorkingConditionsVC.btnQuestion2Action(_ :)), for: UIControlEvents.touchUpInside)
            cell.btnTendsToDisagreeQue2.addTarget(self, action: #selector(WorkingConditionsVC.btnQuestion2Action(_ :)), for: UIControlEvents.touchUpInside)
            cell.btnDisagreeQue2.addTarget(self, action: #selector(WorkingConditionsVC.btnQuestion2Action(_ :)), for: UIControlEvents.touchUpInside)
            
            
            if (arrQuestion.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "SelectedQue1") as! String == "Agree" {
                cell.btnAgreeQue1.isSelected = true
                
            }else if (arrQuestion.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "SelectedQue1") as! String == "Tend to agree" {
                cell.btnTendsToAgreeQue1.isSelected = true
                
            }else if (arrQuestion.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "SelectedQue1") as! String == "Tend to disagree" {
                cell.btnTendsToDisagreeQue1.isSelected = true
                
            }else if (arrQuestion.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "SelectedQue1") as! String == "Disagree" {
                cell.btnDisagreeQue1.isSelected = true
            }
            
            if (arrQuestion.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "SelectedQue2") as! String == "Agree" {
                cell.btnAgreeQue2.isSelected = true
                
            }else if (arrQuestion.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "SelectedQue2") as! String == "Tend to agree" {
                cell.btnTendsToAgreeQue2.isSelected = true
                
            }else if (arrQuestion.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "SelectedQue2") as! String == "Tend to disagree" {
                cell.btnTendsToDisagreeQue2.isSelected = true
                
            }else if (arrQuestion.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "SelectedQue2") as! String == "Disagree" {
                cell.btnDisagreeQue2.isSelected = true
            }
            
            if (arrQuestion.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "SelectedQue1") as! String != "" {
                
                cell.imgQuestion1.layer.shadowRadius = 6
                cell.imgQuestion1.layer.shadowOffset = CGSize.init(width: 0, height: 8)
                cell.imgQuestion1.layer.shadowColor = UIColor.init(red: 39.0/255.0, green: 46.0/255.0, blue: 54.0/255.0, alpha: 1.0).cgColor
                cell.imgQuestion1.layer.shadowOpacity = 0.75
                
            }else{
                
                cell.imgQuestion1.layer.shadowRadius = 3
                cell.imgQuestion1.layer.shadowOffset = CGSize.init(width: 0, height: 4)
                cell.imgQuestion1.layer.shadowColor = UIColor.init(red: 39.0/255.0, green: 46.0/255.0, blue: 54.0/255.0, alpha: 1.0).cgColor
                cell.imgQuestion1.layer.shadowOpacity = 0.75
            }
            
            if (arrQuestion.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "SelectedQue2") as! String != "" {
                
                cell.imgQuestion2.layer.shadowRadius = 6
                cell.imgQuestion2.layer.shadowOffset = CGSize.init(width: 0, height: 8)
                cell.imgQuestion2.layer.shadowColor = UIColor.init(red: 39.0/255.0, green: 46.0/255.0, blue: 54.0/255.0, alpha: 1.0).cgColor
                cell.imgQuestion2.layer.shadowOpacity = 0.75
                
            }else{
                
                cell.imgQuestion2.layer.shadowRadius = 3
                cell.imgQuestion2.layer.shadowOffset = CGSize.init(width: 0, height: 4)
                cell.imgQuestion2.layer.shadowColor = UIColor.init(red: 39.0/255.0, green: 46.0/255.0, blue: 54.0/255.0, alpha: 1.0).cgColor
                cell.imgQuestion2.layer.shadowOpacity = 0.75
            }
            
            if indexPath.row == colWorkingCondition.numberOfItems(inSection: 0) - 1 {
                
                cell.btnSubmit.isHidden = false
                ProjectUtility.setCommonButton(button: cell.btnSubmit)
                cell.btnSubmit.addTarget(self, action: #selector(WorkingConditionsVC.btnSubmitAction(_ :)), for: UIControlEvents.touchUpInside)
                
            }else{
                cell.btnSubmit.isHidden = true
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView : UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize{
        
        return CGSize.init(width: Constants.ScreenSize.SCREEN_WIDTH, height: Constants.ScreenSize.SCREEN_HEIGHT-75)
    }
    
    //MARK:- Tableview Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return arrTableQuestion.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell:Question8TableCell = tableView.dequeueReusableCell(withIdentifier: "Question8TableCell", for: indexPath as IndexPath) as! Question8TableCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell.lblQue.text = (arrTableQuestion.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "Question") as? String
        
        cell.btnQue.tag = indexPath.row
        cell.btnQue.addTarget(self, action: #selector(WorkingConditionsVC.btnTableQueAction(_ :)), for: UIControlEvents.touchUpInside)
        
        if (arrTableQuestion.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "isSelected") != nil && (arrTableQuestion.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "isSelected") as! String == "true"{
            
            cell.btnQue.isSelected = true
        }else{
            cell.btnQue.isSelected = false
        }
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return 39
    }
    
    //MARK:- Scrollview Delegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        
        if scrollView == colWorkingCondition {
            
            print(visibleCell)
            
            if (lastContentOffsetCollectionView > Int(scrollView.contentOffset.x))
            {
                print("Scrolling left")
                
                lastContentOffsetCollectionView = Int(scrollView.contentOffset.x)
            }
            else if (lastContentOffsetCollectionView < Int(scrollView.contentOffset.x))
            {
                print("Scrolling Right")
                
                if visibleCell == 7{
                    
                    var isResourcesSelected = false
                    for i in 0 ..< arrTableQuestion.count {
                        
                        if (arrTableQuestion.object(at: i) as! NSMutableDictionary).value(forKey: "isSelected") != nil && (arrTableQuestion.object(at: i) as! NSMutableDictionary).value(forKey: "isSelected") as! String == "true" {
                            
                            isResourcesSelected = true
                            break
                        }
                    }
                    
                    if isResourcesSelected == true {
                        
                        lastContentOffsetCollectionView = Int(scrollView.contentOffset.x)
                    }else{
                        scrollView.contentOffset.x = CGFloat(lastContentOffsetCollectionView)
                    }
                }
                else if (arrQuestion.object(at: visibleCell) as! NSMutableDictionary).value(forKey: "isSelected1") != nil && (arrQuestion.object(at: visibleCell) as! NSMutableDictionary).value(forKey: "isSelected1") as! String == "true" && (arrQuestion.object(at: visibleCell) as! NSMutableDictionary).value(forKey: "isSelected2") != nil && (arrQuestion.object(at: visibleCell) as! NSMutableDictionary).value(forKey: "isSelected2") as! String == "true" {
                    
                    lastContentOffsetCollectionView = Int(scrollView.contentOffset.x)
                } else{
                    scrollView.contentOffset.x = CGFloat(lastContentOffsetCollectionView)
                }
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        
        if scrollView == colWorkingCondition {
            
            for cell in colWorkingCondition.visibleCells  as [UICollectionViewCell]    {
                
                let indexPath = colWorkingCondition.indexPath(for: cell as UICollectionViewCell)
                
                visibleCell = (indexPath?.row)!
                print("scroll ended\(visibleCell)")
                
                if visibleCell == 7 {
                    lblTitle.text = "Resources"
                }else if visibleCell < 7 {
                    lblTitle.text = "Working Conditions"
                }else{
                    lblTitle.text = "Living Conditions"
                }
            }
        }
    }
    
    
    //MARK:- Questions Array
    
    func createQuestionsArray(){
        
        var dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("The principal/director/chair is always fair when dealing with members of staff.", forKey: "Question1")
        dictQuestion.setValue("Other senior managers are always fair when dealing with members of staff.", forKey: "Question2")
        dictQuestion.setValue("", forKey: "SelectedQue1")
        dictQuestion.setValue("", forKey: "SelectedQue2")
        arrQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("My line manager is always fair when dealing with members of staff.", forKey: "Question1")
        dictQuestion.setValue("My opinion is valued by senior management.", forKey: "Question2")
        dictQuestion.setValue("", forKey: "SelectedQue1")
        dictQuestion.setValue("", forKey: "SelectedQue2")
        arrQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("My school/college/university is a friendly, happy and supportive place to work.", forKey: "Question1")
        dictQuestion.setValue("I would encourage others to work at my school/college/university.", forKey: "Question2")
        dictQuestion.setValue("", forKey: "SelectedQue1")
        dictQuestion.setValue("", forKey: "SelectedQue2")
        arrQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("The physical condition of the school/college/university is excellent.", forKey: "Question1")
        dictQuestion.setValue("The teaching resources and budget for department are generous.", forKey: "Question2")
        dictQuestion.setValue("", forKey: "SelectedQue1")
        dictQuestion.setValue("", forKey: "SelectedQue2")
        arrQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Academic standards are high.", forKey: "Question1")
        dictQuestion.setValue("Administration staff are always friendly and helpful.", forKey: "Question2")
        dictQuestion.setValue("", forKey: "SelectedQue1")
        dictQuestion.setValue("", forKey: "SelectedQue2")
        arrQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("The school/college/university always honors their contracts.", forKey: "Question1")
        dictQuestion.setValue("Teacher evaluations are professional and transparent.", forKey: "Question2")
        dictQuestion.setValue("", forKey: "SelectedQue1")
        dictQuestion.setValue("", forKey: "SelectedQue2")
        arrQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Relevant PD opportunities are readily available.", forKey: "Question1")
        dictQuestion.setValue("My PD budget allows me to travel internationally for my PD sessions.", forKey: "Question2")
        dictQuestion.setValue("", forKey: "SelectedQue1")
        dictQuestion.setValue("", forKey: "SelectedQue2")
        arrQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        arrQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("I have access to all the creature comforts I am used to.", forKey: "Question1")
        dictQuestion.setValue("Shopping and leisure activities are of a high standard.", forKey: "Question2")
        dictQuestion.setValue("", forKey: "SelectedQue1")
        dictQuestion.setValue("", forKey: "SelectedQue2")
        arrQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("There are a wide variety of weekend activities to keep me busy.", forKey: "Question1")
        dictQuestion.setValue("There are a wide range of activities to keep the children entertained.", forKey: "Question2")
        dictQuestion.setValue("", forKey: "SelectedQue1")
        dictQuestion.setValue("", forKey: "SelectedQue2")
        arrQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Health care facilities are excellent.", forKey: "Question1")
        dictQuestion.setValue("I always feel safe living here.", forKey: "Question2")
        dictQuestion.setValue("", forKey: "SelectedQue1")
        dictQuestion.setValue("", forKey: "SelectedQue2")
        arrQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("The locals are friendly and helpful.", forKey: "Question1")
        dictQuestion.setValue("I enjoy living here.", forKey: "Question2")
        dictQuestion.setValue("", forKey: "SelectedQue1")
        dictQuestion.setValue("", forKey: "SelectedQue2")
        arrQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("It is easy to save money living here.", forKey: "Question1")
        dictQuestion.setValue("", forKey: "Question2")
        dictQuestion.setValue("", forKey: "SelectedQue1")
        dictQuestion.setValue("", forKey: "SelectedQue2")
        arrQuestion.add(dictQuestion)
    }
    
    func createTableQuestionArray(){
        
        var dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Football field", forKey: "Question")
        arrTableQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Athletics track", forKey: "Question")
        arrTableQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Swimming pool", forKey: "Question")
        arrTableQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Gymnasium", forKey: "Question")
        arrTableQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Sports hall", forKey: "Question")
        arrTableQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Library", forKey: "Question")
        arrTableQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Auditorium", forKey: "Question")
        arrTableQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Computer labs", forKey: "Question")
        arrTableQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("School bus service", forKey: "Question")
        arrTableQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Theatre", forKey: "Question")
        arrTableQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Duke of Edinburgh Award", forKey: "Question")
        arrTableQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("International Award", forKey: "Question")
        arrTableQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("CCA / ECA program", forKey: "Question")
        arrTableQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("High speed WiFi", forKey: "Question")
        arrTableQuestion.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Multi vendor canteen", forKey: "Question")
        arrTableQuestion.add(dictQuestion)
        
        if dictSelectedSchool.value(forKey: "resources") != nil {
            for i in 0 ..< (dictSelectedSchool.value(forKey: "resources") as! NSArray).count {
                
                if ((dictSelectedSchool.value(forKey: "resources") as! NSArray).object(at: i) as! NSDictionary).value(forKey: "id") as! String != "" {
                    
                    let index = (arrTableQuestion.value(forKey: "Question") as AnyObject).index(of: ((dictSelectedSchool.value(forKey: "resources") as! NSArray).object(at: i) as! NSDictionary).value(forKey: "name") as! String)
                    
                    if index != NSNotFound {
                        
                        (arrTableQuestion.object(at: index) as! NSMutableDictionary).setValue("true", forKey: "isSelected")
                    }
                }
            }
        }
    }
}

class WorkingConditionsCollectionCell: UICollectionViewCell {
    
    @IBOutlet var viewQuestion1: UIView!
    @IBOutlet var viewQuestion2: UIView!
    
    @IBOutlet var imgQuestion1: UIImageView!
    @IBOutlet var imgQuestion2: UIImageView!
    
    @IBOutlet var lblQuestion1: UILabel!
    @IBOutlet var lblQuestion2: UILabel!
    
    @IBOutlet var btnAgreeQue1: UIButton!
    @IBOutlet var btnTendsToAgreeQue1: UIButton!
    @IBOutlet var btnTendsToDisagreeQue1: UIButton!
    @IBOutlet var btnDisagreeQue1: UIButton!
    
    @IBOutlet var btnAgreeQue2: UIButton!
    @IBOutlet var btnTendsToAgreeQue2: UIButton!
    @IBOutlet var btnTendsToDisagreeQue2: UIButton!
    @IBOutlet var btnDisagreeQue2: UIButton!
    
    @IBOutlet var btnSubmit: UIButton!
}

class Question8CollectionCell: UICollectionViewCell {
    
    @IBOutlet var tblQuestion8: UITableView!
    
    @IBOutlet var imgQuestionBg: UIImageView!
    @IBOutlet var imgArrow: UIImageView!
}

class Question8TableCell: UITableViewCell{
    @IBOutlet var lblQue: UILabel!
    @IBOutlet var btnQue: UIButton!
}
