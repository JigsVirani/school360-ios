//
//  RegisterVC.swift
//  Schools360
//
//  Created by Sandeep Gangajaliya on 14/04/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController, UITextFieldDelegate {

    //MARK:- Outlet Declaration
    @IBOutlet var btnJoinCommunity: UIButton!
    
    @IBOutlet var txtEmail: JVFloatLabeledTextField!
    @IBOutlet var txtPassword: JVFloatLabeledTextField!
    @IBOutlet var txtConfirmPassword: JVFloatLabeledTextField!
    
    @IBOutlet var imgEmailLine: UIImageView!
    @IBOutlet var imgPassword: UIImageView!
    @IBOutlet var imgConfirmPassword: UIImageView!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        ProjectUtility.setCommonButton(button: btnJoinCommunity)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK:- Webservice Call
    
    func registerJson()
    {
        ProjectUtility.loadingShow()
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        let dict = ["email_address":txtEmail.text!, "password":txtPassword.text!, "user_type":"2", "country_id":"0"]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/login/register"
        print("\(strUrl) --- \(dict)")
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            ProjectUtility.loadingHide()
            
            print(response?.webserviceResponse)
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        self.performSegue(withIdentifier: "thankYou", sender: nil)
                        
                        Constants.appDelegate.dictLoginUserDetail = (dictData.value(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        
                        if Constant.appDelegate.dictLoginUserDetail.value(forKey: "user_id") != nil {
                            Constant.appDelegate.dictLoginUserDetail.setValue("\(Constant.appDelegate.dictLoginUserDetail.value(forKey: "user_id") as! NSInteger)", forKey: "user_id")
                        }else{
                            Constant.appDelegate.dictLoginUserDetail.setValue("0", forKey: "user_id")
                        }
                        
                        Constant.appDelegate.dictLoginUserDetail.setValue(self.txtPassword.text, forKey: "password")
                        
                        let archivedUser = NSKeyedArchiver.archivedData(withRootObject: Constants.appDelegate.dictLoginUserDetail)
                        UserDefaults.standard.setValue(archivedUser, forKey: "UserDetail")
                        UserDefaults.standard.synchronize()
                        
                        return
                    }
                    ProjectUtility.displayTost(erroemessage: dictData.value(forKey: "message") as! String)
                    return
                }
            }
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
            
        }) { (error) -> Void in
            
            print(error!)
            ProjectUtility.loadingHide()
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    // MARK:- Button TouchUp
    @IBAction func btnBackAction (_ sender: UIButton){
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnJoinCommunity (_ sender: UIButton){
        
//        self.performSegue(withIdentifier: "thankYou", sender: nil)
        
        self.view.endEditing(true)
        
        if txtEmail.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please enter email address")
        }else if txtPassword.text == ""{
            ProjectUtility.displayTost(erroemessage: "Please enter password")
        }else if (txtPassword.text?.characters.count)! < 6{
            ProjectUtility.displayTost(erroemessage: "The password must be at least 6 characters")
        }else if txtPassword.text != txtConfirmPassword.text{
            ProjectUtility.displayTost(erroemessage: "Your password is not matching with confirm password")
        }else{
            self.registerJson()
            
            var arrEmail = NSMutableArray()
            let dict = NSMutableDictionary()
            dict.setObject(txtEmail.text!, forKey: "Email" as NSCopying)
            dict.setObject(txtPassword.text!, forKey: "Password" as NSCopying)
            if UserDefaults.standard.value(forKey: "EmailList") != nil {
                
                let unarchivedObject = UserDefaults.standard.object(forKey: "EmailList") as? NSData
                arrEmail = NSKeyedUnarchiver.unarchiveObject(with: unarchivedObject! as Data) as! NSMutableArray
            }
            arrEmail.add(dict)
            let archivedUser = NSKeyedArchiver.archivedData(withRootObject: arrEmail)
            
            UserDefaults.standard.set(archivedUser, forKey: "EmailList")
            UserDefaults.standard.synchronize()
        }
    }
    
    @IBAction func btnShowPasswordAction (_ sender: UIButton){
        
        if sender.tag == 1 {
            
            if sender.isSelected == true {
                
                txtPassword.isSecureTextEntry = true
                sender.isSelected = false
            }else{
                txtPassword.isSecureTextEntry = false
                sender.isSelected = true
            }
        }else {
            if sender.isSelected == true {
                
                txtConfirmPassword.isSecureTextEntry = true
                sender.isSelected = false
            }else{
                txtConfirmPassword.isSecureTextEntry = false
                sender.isSelected = true
            }
        }
    }
    
    // MARK:- TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtEmail {
            self.changeTextFiledLineColor(textField, imgEmailLine)
        }else if textField == txtPassword {
            self.changeTextFiledLineColor(textField, imgPassword)
        }else if textField == txtConfirmPassword {
            self.changeTextFiledLineColor(textField, imgConfirmPassword)
        }
        
    }
    
    @IBAction func textFieldEditingChanged(_ textField: UITextField) {
        
        if textField == txtEmail {
            self.changeTextFiledLineColor(textField, imgEmailLine)
        }else if textField == txtPassword {
            self.changeTextFiledLineColor(textField, imgPassword)
        }else if textField == txtConfirmPassword {
            self.changeTextFiledLineColor(textField, imgConfirmPassword)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        imgEmailLine.backgroundColor = UIColor.lightGray
        imgPassword.backgroundColor = UIColor.lightGray
        imgConfirmPassword.backgroundColor = UIColor.lightGray
    }
    
    func changeTextFiledLineColor(_ textField: UITextField, _ imageView: UIImageView){
        
        if textField.text == "" {
            imageView.backgroundColor = UIColor.lightGray
        }else{
            imageView.backgroundColor = UIColor.init(red: 99.0 / 255.0, green: 160.0 / 255.0, blue: 248.0 / 255.0, alpha: 1.0)
        }
    }
    
    // MARK:- Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let vc: ThankYouVC = segue.destination as! ThankYouVC
        vc.strEmail = txtEmail.text!
    }
}
