//
//  MyPackageVC.swift
//  Schools360
//
//  Created by Sandeep Gangajaliya on 17/04/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit

class MyPackageVC: UIViewController, UIScrollViewDelegate {

    //MARK:- Outlet Declartion
    @IBOutlet var scrlViewMyPackage: UIScrollView!
    fileprivate (set) var scrollBar : SwiftyVerticalScrollBar!
    @IBOutlet var btnContinue: UIButton!
    
    @IBOutlet var txtLowestMothlyTeachingSalary: UITextField!
    @IBOutlet var txtHighestMothlyTeachingSalary: UITextField!
    @IBOutlet var txtResponsibiltyPoint: UITextField!
    @IBOutlet var txtHousingAllowance: UITextField!
    @IBOutlet var txtSchoolFees: UITextField!
    @IBOutlet var txtHealthInsurance: UITextField!
    @IBOutlet var txtFlightsAllowance: UITextField!
    @IBOutlet var txtShippingAllowance: UITextField!
    @IBOutlet var txtGratuity: UITextField!
    @IBOutlet var txtAnnualLeave: UITextField!
    
    @IBOutlet var btnLowestMothlyTeachingSalary: UIButton!
    @IBOutlet var btnHighestMothlyTeachingSalary: UIButton!
    @IBOutlet var btnHousingAllowance: UIButton!
    @IBOutlet var btnSchoolFees: UIButton!
    @IBOutlet var btnHealthInsurance: UIButton!
    @IBOutlet var btnFlightsAllowance: UIButton!
    @IBOutlet var btnShippingAllowance: UIButton!
    @IBOutlet var btnGratuity: UIButton!
    @IBOutlet var btnAnnualLeave: UIButton!
    
    @IBOutlet var imgResponsibiltyPoint: UIImageView!
    @IBOutlet var imgDownArrowGIF: UIImageView!
    
    //MARK: Variable Declartion
    var dictDetail = NSMutableDictionary()
    var dictSelectedSchool = NSMutableDictionary()
    
    var arrLowestMonthlySalary = NSMutableArray()
    var arrHeighestMonthlySalary = NSMutableArray()
    
//    var arrLowestMonthlySalary: NSArray = ["1 - 250","251 - 500","501 - 750","751 - 1000","1001 - 1250","1251 - 1500","1501 - 1750","1751 - 2000","2001 - 2250","2251 - 2500","2501 - 2750","2751 - 3000","3001 - 3350","3251 - 3500","3501 - 3750","3751 - 4000","4001 - 4350","4251 - 4500","4501 - 4750","4751 - 5000","> 5000"]
//    var arrHeighestMonthlySalary: NSArray = ["> 5000","5001 - 5350","5251 - 5500","5501 - 5750","5751 - 6000","6001 - 6350","6251 - 6500","6751 - 7000","6751 - 7000","> 7000"]
    var arrHousingAllowance: NSArray = ["No housing allowance provided","Allowance covers some of the rent","Allowance covers most of the rent","Allowance is 100 %","Provided by the school / college etc."]
    var arrSchoolFees: NSArray = ["No school fees provided","Provided for 1 child","Provided for 2 children","Provided for 3 children","Provided for more than 3 children","Other"]
    var arrHealthInsurance: NSArray = ["Not provided","Provided by school / college etc."]
    var arrFlightAllowance: NSArray = ["No flight allowance","End of contract - me only","Annually - me only","End of contract + dependants","Annual flights + dependants"]
    var arrShippingAllowance: NSArray = ["No shipping allowance","US$1 - 500","US$501 - 1000","US$1000 - 1500","More than US$ 1500"]
    var arrGratuity: NSArray = ["Other","No gratuity","401 K contribution","Superannuation contribution","1 Months salary for every year"]
    var arrAnnualLeave: NSArray = ["Less than 3 weeks","3 weeks","4 weeks","5 weeks","6 weeeks","7 weeks","8 weeks","9 weeks","10 weeks","11 weeks","12 weeks","13 weeks","14 weeks","15 weeks","16 weeks","more than 16 weeks"]
    
    //MARK:DropDowns
    let lowestMothlySalaryDropDown = DropDown()
    let highestMothlySalaryDropDown = DropDown()
    let housingAllowanceDropDown = DropDown()
    let schoolFeesDropDown = DropDown()
    let healthInsuranceDropDown = DropDown()
    let flightsAllowanceDropDown = DropDown()
    let shippingAllowanceDropDown = DropDown()
    let gratuityDropDown = DropDown()
    let annualLeaveDropDown = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.lowestMothlySalaryDropDown,
            self.highestMothlySalaryDropDown,
            self.housingAllowanceDropDown,
            self.schoolFeesDropDown,
            self.healthInsuranceDropDown,
            self.flightsAllowanceDropDown,
            self.shippingAllowanceDropDown,
            self.gratuityDropDown,
            self.annualLeaveDropDown
        ]
    }()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialization()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.scrollBar.frame = CGRect(x: Constant.ScreenSize.SCREEN_WIDTH-20, y: 75, width: 20, height: Constant.ScreenSize.SCREEN_HEIGHT - 153)
    }
    
    // MARK:- Initialization
    
    func initialization(){
        
        self.scrollBar = SwiftyVerticalScrollBar(frame: CGRect.zero, targetScrollView: self.scrlViewMyPackage)
        self.view.addSubview(self.scrollBar!)
        
        ProjectUtility.setCommonButton(button: btnContinue)
        
        setupDropDowns()
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        
        DropDown.setupDefaultAppearance()
        
        imgDownArrowGIF.image = UIImage.gifImageWithName("down_arrow")
        
        if dictSelectedSchool.value(forKey: "school_name") != nil {
            
            txtResponsibiltyPoint.text = self.dictSelectedSchool.value(forKey: "responsiblity_point") as? String
            
            for i in 0 ..< self.arrHousingAllowance.count{
                
                if i+1 == Int(self.dictSelectedSchool.value(forKey: "housing_allowance") as! String){
                    
                    self.txtHousingAllowance.text = self.arrHousingAllowance.object(at: i) as? String
                    self.txtHousingAllowance.tag = i + 1
                }
            }
            
            for i in 0 ..< self.arrSchoolFees.count{
                
                if i+1 == Int(self.dictSelectedSchool.value(forKey: "school_fees") as! String){
                    
                    self.txtSchoolFees.text = self.arrSchoolFees.object(at: i) as? String
                    self.txtSchoolFees.tag = i + 1
                }
            }
            
            for i in 0 ..< self.arrHealthInsurance.count{
                
                if i+1 == Int(self.dictSelectedSchool.value(forKey: "health_insurance") as! String){
                    
                    self.txtHealthInsurance.text = self.arrHealthInsurance.object(at: i) as? String
                    self.txtHealthInsurance.tag = i + 1
                }
            }
            
            for i in 0 ..< self.arrFlightAllowance.count{
                
                if i+1 == Int(self.dictSelectedSchool.value(forKey: "flight_allowance") as! String){
                    
                    self.txtFlightsAllowance.text = self.arrFlightAllowance.object(at: i) as? String
                    self.txtFlightsAllowance.tag = i + 1
                }
            }
            
            for i in 0 ..< self.arrShippingAllowance.count{
                
                if i+1 == Int(self.dictSelectedSchool.value(forKey: "shipping_allowance") as! String){
                    
                    self.txtShippingAllowance.text = self.arrShippingAllowance.object(at: i) as? String
                    self.txtShippingAllowance.tag = i + 1
                }
            }
            
            for i in 0 ..< self.arrGratuity.count{
                
                if i+1 == Int(self.dictSelectedSchool.value(forKey: "gratuity") as! String){
                    
                    self.txtGratuity.text = self.arrGratuity.object(at: i) as? String
                    self.txtGratuity.tag = i + 1
                }
            }
            
            for i in 0 ..< self.arrAnnualLeave.count{
                
                if i+1 == Int(self.dictSelectedSchool.value(forKey: "annual_leave") as! String){
                    
                    self.txtAnnualLeave.text = self.arrAnnualLeave.object(at: i) as? String
                    self.txtAnnualLeave.tag = i + 1
                }
            }
        }
        
        self.getSalaryInfoJson()
    }
    
    //MARK:- Webservice Call
    func getSalaryInfoJson() {
        
        ProjectUtility.loadingShow()
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        let dict = ["country_id":dictDetail.value(forKey: "country_id")!]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/home/salaryinfo"
        print("\(strUrl) --- \(dict)")
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            ProjectUtility.loadingHide()
            
            print(response?.webserviceResponse)
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        for i in 0 ..< (dictData.value(forKey: "data") as! NSArray).count {
                            
                            if i < 21 {
                                self.arrLowestMonthlySalary.add(((dictData.value(forKey: "data") as! NSArray).object(at: i) as! NSDictionary).value(forKey: "range") as! String)
                            }else{
                                self.arrHeighestMonthlySalary.add(((dictData.value(forKey: "data") as! NSArray).object(at: i) as! NSDictionary).value(forKey: "range") as! String)
                            }
                        }
                        
                        if self.arrLowestMonthlySalary.count != 0 {
                            let arrTemp = (self.arrLowestMonthlySalary.object(at: 0) as! String).components(separatedBy: " ") as NSArray
                            
                            self.txtResponsibiltyPoint.attributedPlaceholder = NSAttributedString(string: "Responsibility point (\(arrTemp.object(at: arrTemp.count-1) as! String))", attributes: [NSForegroundColorAttributeName: UIColor.lightGray])
                        }
                        
                        self.setupLowestMothlySalaryDropDown()
                        self.setupHighestMothlySalaryDropDown()
                    }
                }
            }
            
        }) { (error) -> Void in
            
            print(error)
            ProjectUtility.loadingHide()
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    // MARK:- Button TouchUp
    
    @IBAction func btnBackAction (_ sender: UIButton){
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLowestMothlyTeachingSalaryAction (_ sender: UIButton){
        self.view.endEditing(true)
        lowestMothlySalaryDropDown.show()
    }
    @IBAction func btnHighestMothlyTeachingSalaryAction (_ sender: UIButton){
        self.view.endEditing(true)
        highestMothlySalaryDropDown.show()
    }
    @IBAction func btnHousingAllowanceAction (_ sender: UIButton){
        self.view.endEditing(true)
        housingAllowanceDropDown.show()
    }
    @IBAction func btnSchoolFeesAction (_ sender: UIButton){
        self.view.endEditing(true)
        schoolFeesDropDown.show()
    }
    @IBAction func btnHealthInsuranceAction (_ sender: UIButton){
        self.view.endEditing(true)
        healthInsuranceDropDown.show()
    }
    @IBAction func btnFlightsAllowanceAction (_ sender: UIButton){
        self.view.endEditing(true)
        flightsAllowanceDropDown.show()
    }
    @IBAction func btnShippingAllowanceAction (_ sender: UIButton){
        self.view.endEditing(true)
        shippingAllowanceDropDown.show()
    }
    @IBAction func btnGratuityAction (_ sender: UIButton){
        self.view.endEditing(true)
        gratuityDropDown.show()
    }
    @IBAction func btnAnnualLeaveAction (_ sender: UIButton){
        self.view.endEditing(true)
        annualLeaveDropDown.show()
    }
    
    @IBAction func btnContinueToWorkingConditionAction (_ sender: UIButton){
        self.view.endEditing(true)
        if txtLowestMothlyTeachingSalary.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please select lowest monthly teaching salary")
        }else if txtHighestMothlyTeachingSalary.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please select highest monthly teaching salary")
        }else if txtResponsibiltyPoint.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please enter responsibility point")
        }else if txtHousingAllowance.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please select housing allowance")
        }else if txtSchoolFees.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please select school fees")
        }else if txtHealthInsurance.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please select health insurance")
        }else if txtFlightsAllowance.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please select flight allowance")
        }else if txtShippingAllowance.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please select shipping allowance")
        }else if txtGratuity.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please select gratuity")
        }else if txtAnnualLeave.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please select annual leave")
        }else{
            self.performSegue(withIdentifier: "workingCondition", sender: nil)
        }
    }
    
    // MARK:- SetUp DropDown
    func setupDropDowns() {
        setupHousingAllowanceDropDown()
        setupSchoolFeesDropDown()
        setupHealthInsuranceDropDown()
        setupFlightsAllowanceDropDown()
        setupshippingAllowanceDropDown()
        setupGratuityDropDown()
        setupannualLeaveDropDown()
    }
    
    func setupLowestMothlySalaryDropDown() {
        lowestMothlySalaryDropDown.anchorView = btnLowestMothlyTeachingSalary
        lowestMothlySalaryDropDown.bottomOffset = CGPoint(x: 0, y: 40)
        
        lowestMothlySalaryDropDown.dataSource = (arrLowestMonthlySalary as NSArray) as! [String]
        
        lowestMothlySalaryDropDown.selectionAction = { [unowned self] (index, item) in
            self.txtLowestMothlyTeachingSalary.text = item
            self.txtLowestMothlyTeachingSalary.tag = index + 1
        }
    }
    func setupHighestMothlySalaryDropDown() {
        highestMothlySalaryDropDown.anchorView = btnHighestMothlyTeachingSalary
        highestMothlySalaryDropDown.bottomOffset = CGPoint(x: 0, y: 40)
        
        highestMothlySalaryDropDown.dataSource = (arrHeighestMonthlySalary as NSArray) as! [String]
        
        highestMothlySalaryDropDown.selectionAction = { [unowned self] (index, item) in
            self.txtHighestMothlyTeachingSalary.text = item
            self.txtHighestMothlyTeachingSalary.tag = index + 21
        }
    }
    func setupHousingAllowanceDropDown() {
        housingAllowanceDropDown.anchorView = btnHousingAllowance
        housingAllowanceDropDown.bottomOffset = CGPoint(x: 0, y: 40)
        
        housingAllowanceDropDown.dataSource = arrHousingAllowance as! [String]
        
        housingAllowanceDropDown.selectionAction = { [unowned self] (index, item) in
            self.txtHousingAllowance.text = item
            self.txtHousingAllowance.tag = index + 1
        }
    }
    func setupSchoolFeesDropDown() {
        schoolFeesDropDown.anchorView = btnSchoolFees
        schoolFeesDropDown.bottomOffset = CGPoint(x: 0, y: 40)
        
        schoolFeesDropDown.dataSource = arrSchoolFees as! [String]
        
        schoolFeesDropDown.selectionAction = { [unowned self] (index, item) in
            self.txtSchoolFees.text = item
            self.txtSchoolFees.tag = index + 1
        }
    }
    func setupHealthInsuranceDropDown() {
        healthInsuranceDropDown.anchorView = btnHealthInsurance
        healthInsuranceDropDown.bottomOffset = CGPoint(x: 0, y: 40)
        
        healthInsuranceDropDown.dataSource = arrHealthInsurance as! [String]
        
        healthInsuranceDropDown.selectionAction = { [unowned self] (index, item) in
            self.txtHealthInsurance.text = item
            self.txtHealthInsurance.tag = index + 1
        }
    }
    func setupFlightsAllowanceDropDown() {
        flightsAllowanceDropDown.anchorView = btnFlightsAllowance
        flightsAllowanceDropDown.bottomOffset = CGPoint(x: 0, y: 40)
        
        flightsAllowanceDropDown.dataSource = arrFlightAllowance as! [String]
        
        flightsAllowanceDropDown.selectionAction = { [unowned self] (index, item) in
            self.txtFlightsAllowance.text = item
            self.txtFlightsAllowance.tag = index + 1
        }
    }
    func setupshippingAllowanceDropDown() {
        shippingAllowanceDropDown.anchorView = btnShippingAllowance
        shippingAllowanceDropDown.bottomOffset = CGPoint(x: 0, y: 40)
        
        shippingAllowanceDropDown.dataSource = arrShippingAllowance as! [String]
        
        shippingAllowanceDropDown.selectionAction = { [unowned self] (index, item) in
            self.txtShippingAllowance.text = item
            self.txtShippingAllowance.tag = index + 1
        }
    }
    func setupGratuityDropDown() {
        gratuityDropDown.anchorView = btnGratuity
        gratuityDropDown.bottomOffset = CGPoint(x: 0, y: 40)
        
        gratuityDropDown.dataSource = arrGratuity as! [String]
        
        gratuityDropDown.selectionAction = { [unowned self] (index, item) in
            self.txtGratuity.text = item
            self.txtGratuity.tag = index + 1
        }
    }
    func setupannualLeaveDropDown() {
        annualLeaveDropDown.anchorView = btnAnnualLeave
        annualLeaveDropDown.bottomOffset = CGPoint(x: 0, y: 40)
        
        annualLeaveDropDown.dataSource = arrAnnualLeave as! [String]
        
        annualLeaveDropDown.selectionAction = { [unowned self] (index, item) in
            self.txtAnnualLeave.text = item
            self.txtAnnualLeave.tag = index + 1
        }
    }
    
    // MARK:- TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtResponsibiltyPoint {
            self.changeTextFiledLineColor(textField, imgResponsibiltyPoint)
        }
    }
    
    @IBAction func textFieldEditingChanged(_ textField: UITextField) {
        
        if textField == txtResponsibiltyPoint {
            self.changeTextFiledLineColor(textField, imgResponsibiltyPoint)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        imgResponsibiltyPoint.backgroundColor = UIColor.lightGray
    }
    
    func changeTextFiledLineColor(_ textField: UITextField, _ imageView: UIImageView){
        
        if textField.text == "" {
            imageView.backgroundColor = UIColor.lightGray
        }else{
            imageView.backgroundColor = UIColor.init(red: 99.0 / 255.0, green: 160.0 / 255.0, blue: 248.0 / 255.0, alpha: 1.0)
        }
    }
    
    // MARK:- Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        dictDetail.setValue(txtLowestMothlyTeachingSalary.tag, forKey: "salary_low")
        dictDetail.setValue(txtHighestMothlyTeachingSalary.tag, forKey: "salary_high")
        dictDetail.setValue(txtResponsibiltyPoint.text, forKey: "responsiblity_point")
        dictDetail.setValue(txtHousingAllowance.tag, forKey: "housing_allowance_id")
        dictDetail.setValue(txtSchoolFees.tag, forKey: "school_fees")
        dictDetail.setValue(txtHealthInsurance.tag, forKey: "health_ins_id")
        dictDetail.setValue(txtFlightsAllowance.tag, forKey: "flight_allowance")
        dictDetail.setValue(txtShippingAllowance.tag, forKey: "shipping_allowance_id")
        dictDetail.setValue(txtGratuity.tag, forKey: "gratuity_id")
        dictDetail.setValue(txtAnnualLeave.tag, forKey: "annual_leave_id")
        
        let vc: WorkingConditionsVC = segue.destination as! WorkingConditionsVC
        vc.dictDetail = dictDetail
        vc.dictSelectedSchool = dictSelectedSchool
    }
    
    // MARK:- Scroll VOew Delegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        
        let scrollViewHeight = scrollView.frame.size.height;
        let scrollContentSizeHeight = scrollView.contentSize.height;
        let scrollOffset = scrollView.contentOffset.y;
        
        if (scrollOffset + scrollViewHeight == scrollContentSizeHeight){
            imgDownArrowGIF.isHidden = true
        }else{
            imgDownArrowGIF.isHidden = false
        }
    }
}
