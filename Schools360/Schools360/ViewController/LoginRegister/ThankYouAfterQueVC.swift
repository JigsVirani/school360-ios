//
//  ThankYouAfterQueVC.swift
//  Schools360
//
//  Created by Sandeep Gangajaliya on 25/04/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit
import Social

class ThankYouAfterQueVC: UIViewController {

    //MARK:- Outlet Declaration
    @IBOutlet var btnInviteColleagues: UIButton!
    @IBOutlet var btnSearchSchools: UIButton!
    
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ProjectUtility.setCommonButton(button: btnInviteColleagues)
        ProjectUtility.setCommonButton(button: btnSearchSchools)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    // MARK:- Button TouchUp
    
    @IBAction func btnBackAction (_ sender: UIButton){
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnInviteFrndsAction (_ sender: UIButton){
        let strAppstoreURL = "Try this amazing app for teachers.\n" + "It knows where the best schools are https://itunes.apple.com/in/app/facebook/id284882215?mt=8"
        
        let textToShare:Array = [strAppstoreURL] as [Any]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
}
