//
//  LoginVC.swift
//  Schools360
//
//  Created by Sandeep Gangajaliya on 25/04/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    //MARK:- Outlet Declaration
    @IBOutlet var btnLogin: UIButton!
    
    @IBOutlet var txtEmail: SearchTextField!
    @IBOutlet var txtPassword: JVFloatLabeledTextField!
    
    @IBOutlet var imgEmailLine: UIImageView!
    @IBOutlet var imgPasswordLine: UIImageView!
    
    //MARK: Other Variable
    var arrEmail = NSMutableArray()
    
    //MARK:TypeAndSearch
    var loginEmailResults = [SearchTextFieldItem]()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ProjectUtility.setCommonButton(button: btnLogin)
        
        if UserDefaults.standard.value(forKey: "EmailList") != nil {
            
            let unarchivedObject = UserDefaults.standard.object(forKey: "EmailList") as? NSData
            arrEmail = NSKeyedUnarchiver.unarchiveObject(with: (unarchivedObject as Data?)!) as! NSMutableArray
            
            for i in 0 ..< arrEmail.count {
                self.loginEmailResults.append(SearchTextFieldItem(title: (arrEmail.object(at: i) as! NSMutableDictionary).value(forKey: "Email") as! String, subtitle: (arrEmail.object(at: i) as! NSMutableDictionary).value(forKey: "Password") as? String, image: nil, id: ""))
            }
        }
        
        self.configureEmailSearchTextField()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
    }
    
    // MARK:- Webservice Call
    
    func loginJson()
    {        
        ProjectUtility.loadingShow()
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        let dict = ["email_address":txtEmail.text!, "password":txtPassword.text!]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/login/loginuser"
        print("\(strUrl) --- \(dict)")
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            ProjectUtility.loadingHide()
            
            print(response?.webserviceResponse)
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    ProjectUtility.displayTost(erroemessage: dictData.value(forKey: "message") as! String)
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        Constants.appDelegate.dictLoginUserDetail = (dictData.value(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        Constant.appDelegate.dictLoginUserDetail.setValue(self.txtPassword.text, forKey: "password")
                        
                        let archivedUser = NSKeyedArchiver.archivedData(withRootObject: Constants.appDelegate.dictLoginUserDetail)
                        UserDefaults.standard.setValue(archivedUser, forKey: "UserDetail")
                        UserDefaults.standard.synchronize()
        
//                        if Constants.appDelegate.dictLoginUserDetail.value(forKey: "is_contributed") as! String == "0"{
//                            
//                            self.performSegue(withIdentifier: "contribute", sender: nil)
//                        }else{
                            self.performSegue(withIdentifier: "advanceSearchSegue", sender: nil)
//                        }
                    }
                    
                    return
                }
            }
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
            
        }) { (error) -> Void in
            
            print(error!)
            ProjectUtility.loadingHide()
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    // MARK:- Button TouchUp
    @IBAction func btnBackAction (_ sender: UIButton){
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnShowPasswordAction (_ sender: UIButton){
        if sender.isSelected == true {
            
            txtPassword.isSecureTextEntry = true
            sender.isSelected = false
        }else{
            txtPassword.isSecureTextEntry = false
            sender.isSelected = true
        }
    }
    
    @IBAction func btnLoginAction (_ sender: UIButton){
        if txtEmail.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please enter email address")
        }else if txtPassword.text == ""{
            ProjectUtility.displayTost(erroemessage: "Please enter password")
        }else{
            self.loginJson()
            
            var arrEmail = NSMutableArray()
            
            let dict = NSMutableDictionary()
            dict.setObject(txtEmail.text!, forKey: "Email" as NSCopying)
            dict.setObject(txtPassword.text!, forKey: "Password" as NSCopying)
            
            if UserDefaults.standard.value(forKey: "EmailList") != nil {
                
                let unarchivedObject = UserDefaults.standard.object(forKey: "EmailList") as? NSData
                arrEmail = NSKeyedUnarchiver.unarchiveObject(with: unarchivedObject! as Data) as! NSMutableArray
                
                var isExist = false
                
                for i in 0 ..< arrEmail.count {
                    
                    if (arrEmail.object(at: i) as! NSDictionary).value(forKey: "Email") as! String == txtEmail.text! {
                        
                        isExist = true
                        arrEmail.removeObject(at: i)
                        arrEmail.add(dict)
                        break
                    }
                }
                
                if isExist == false {
                    arrEmail.add(dict)
                }
            }else{
                arrEmail.add(dict)
            }
            let archivedUser = NSKeyedArchiver.archivedData(withRootObject: arrEmail)
            
            UserDefaults.standard.set(archivedUser, forKey: "EmailList")
            UserDefaults.standard.synchronize()
            
            self.loginEmailResults = [SearchTextFieldItem]()
            
            for i in 0 ..< arrEmail.count {
                self.loginEmailResults.append(SearchTextFieldItem(title: (arrEmail.object(at: i) as! NSMutableDictionary).value(forKey: "Email") as! String, subtitle: "", image: nil, id: ""))
            }
        }
    }

    // MARK:- TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtEmail {
            self.changeTextFiledLineColor(textField, imgEmailLine)
        }else if textField == txtPassword {
            self.changeTextFiledLineColor(textField, imgPasswordLine)
        }
    }
    
    @IBAction func textFieldEditingChanged(_ textField: UITextField) {
        
        if textField == txtEmail {
            self.changeTextFiledLineColor(textField, imgEmailLine)
        }else if textField == txtPassword {
            self.changeTextFiledLineColor(textField, imgPasswordLine)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        imgEmailLine.backgroundColor = UIColor.lightGray
        imgPasswordLine.backgroundColor = UIColor.lightGray
            
        if Constants.appDelegate.dictLoginUserDetail.value(forKey: "email_address") == nil && txtEmail.text != "" && txtPassword.text != "" {
            self.btnLoginAction(btnLogin)
        }
    }
    
    func changeTextFiledLineColor(_ textField: UITextField, _ imageView: UIImageView){
        
        if textField.text == "" {
            imageView.backgroundColor = UIColor.lightGray
        }else{
            imageView.backgroundColor = UIColor.init(red: 99.0 / 255.0, green: 160.0 / 255.0, blue: 248.0 / 255.0, alpha: 1.0)
        }
    }
    
    //MARK:- Set Suggetion Box
    fileprivate func configureEmailSearchTextField() {
        
        txtEmail.maxNumberOfResults = 100
        txtEmail.maxResultsListHeight = 200
        txtEmail.theme.font = UIFont.init(name: "Avenir Next", size: 12)!
        
        txtEmail.itemSelectionHandler = { item, itemPosition in
            
            self.view.endEditing(true)
            self.txtEmail.text = item.title
            self.txtPassword.text = item.subtitle
        }
        
        txtEmail.userStoppedTypingHandler = {
            if self.loginEmailResults.count != 0 {
                if let criteria = self.txtEmail.text {
                    if criteria.characters.count > 0 {
                        
                        self.txtEmail.showLoadingIndicator()
                        
                        self.txtEmail.filterItems(self.loginEmailResults)
                        self.txtEmail.stopLoadingIndicator()
                    }
                }
            }
        }
    }
}
