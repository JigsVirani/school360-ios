//
//  ThankYouVC.swift
//  Schools360
//
//  Created by Sandeep Gangajaliya on 15/04/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit

class ThankYouVC: UIViewController {

    //MARK:- Outlet Declartion
    @IBOutlet var tblDescription: UITableView!
    
    @IBOutlet var btnContribute: UIButton!
    
    fileprivate (set) var scrollBar : SwiftyVerticalScrollBar!
    
    //MARK:Other Variable
    var strEmail = ""
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblDescription.rowHeight = UITableViewAutomaticDimension
        tblDescription.estimatedRowHeight = 328.0
        
        ProjectUtility.setCommonButton(button: btnContribute)
        
        self.scrollBar = SwiftyVerticalScrollBar(frame: CGRect.zero, targetScrollView: self.tblDescription)
        self.view.addSubview(self.scrollBar!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.scrollBar.frame = CGRect(x: Constant.ScreenSize.SCREEN_WIDTH-20, y: 75, width: 20, height: Constant.ScreenSize.SCREEN_HEIGHT - 153)
    }
    
    //MARK:- Tableview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        
        let cell:ThankYouTableCell = tableView.dequeueReusableCell(withIdentifier: "ThankYouTableCell", for: indexPath as IndexPath) as! ThankYouTableCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell.imgBorder.layer.cornerRadius = 5.0
        cell.imgBorder.clipsToBounds = true
        
        cell.imgBorder.layer.borderWidth = 1.0
        cell.imgBorder.layer.borderColor = UIColor.init(red: 86.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        
        cell.lblEmail.text = strEmail
        
        return cell;
    }
}

class ThankYouTableCell: UITableViewCell {
    
    @IBOutlet var imgBorder: UIImageView!
    @IBOutlet var lblEmail: UILabel!
}
