//
//  ForgotPasswordVC.swift
//  EdYouRate
//
//  Created by Sandeep on 07/09/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    //MARK:- Outlet Declaration
    @IBOutlet var btnReset: UIButton!
    @IBOutlet var txtEmail: JVFloatLabeledTextField!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        ProjectUtility.setCommonButton(button: btnReset)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK:- Webservice Call
    func forgotPasswordJson() {
        
        ProjectUtility.loadingShow()
        
        let dict = ["email_address": txtEmail.text!] as NSDictionary
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/login/forgotpassword"
        print("\(strUrl) --- \(dict)")
        
        let manager = AFHTTPSessionManager()
        manager.requestSerializer.timeoutInterval = 600
        
        manager.post(strUrl, parameters: dict, progress: { (progress) in
            print("InProgress")
        }, success: { (task, responseObject) in
            
            ProjectUtility.loadingHide()
            print(responseObject!)
            
            if responseObject != nil, let dictResponse = responseObject as? NSDictionary {
                
//                if dictResponse.value(forKey: "success") != nil && dictResponse.value(forKey: "success") as! Bool == true{
                
                    self.navigationController?.popViewController(animated: true)
//                }
                
                ProjectUtility.displayTost(erroemessage: dictResponse.value(forKey: "message") as! String)
                return
            }
            
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
            
        }) { (task, error) in
            print(error)
            ProjectUtility.loadingHide()
            
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    // MARK:- Button TouchUp
    @IBAction func btnBackAction (_ sender: UIButton){
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnResetAction (_ sender: UIButton){
        
        self.view.endEditing(true)
        if txtEmail.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please enter email address")
        }else{
            self.forgotPasswordJson()
        }
    }
}
