//
//  MyProfileVC.swift
//  Schools360
//
//  Created by Sandeep Gangajaliya on 27/07/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit

class MyProfileVC: UIViewController, UIActionSheetDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    //MARK:- Outlet Declaration
    @IBOutlet var imgProfilePic: UIImageView!
    
    @IBOutlet var txtFirstName: SearchTextField!
    @IBOutlet var txtLastName: SearchTextField!
    @IBOutlet var txtEmail: SearchTextField!
    
    @IBOutlet var imgFirstName: UIImageView!
    @IBOutlet var imgLastName: UIImageView!
    
    @IBOutlet var btnUpdateProfile: UIButton!
    @IBOutlet var btnChangePassword: UIButton!
    
    var dictProfileDetail = NSMutableDictionary()
    var isEditedImage = false
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialization()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Initialization
    func initialization() {
        btnUpdateProfile.layer.cornerRadius = 20
        btnChangePassword.layer.cornerRadius = 20
        
        imgProfilePic.layer.cornerRadius = 61
        
        self.getUserProfileJson()
    }
    
    // MARK:- Webservice Call
    func getUserProfileJson() {
        ProjectUtility.loadingShow()
        
        let dict = ["user_id": Constant.appDelegate.dictLoginUserDetail.value(forKey: "user_id") as! String] as NSDictionary
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/profile/userprofile"
        print("\(strUrl) --- \(dict)")
        
        let manager = AFHTTPSessionManager()
        manager.requestSerializer.timeoutInterval = 600
        
        manager.post(strUrl, parameters: dict, progress: { (progress) in
            print("InProgress")
        }, success: { (task, responseObject) in
            
            ProjectUtility.loadingHide()
            print(responseObject!)
            
            if responseObject != nil, let dictResponse = responseObject as? NSDictionary {
                
                if dictResponse.value(forKey: "success") != nil && dictResponse.value(forKey: "success") as! Bool == true{
                    
                    if (dictResponse.value(forKey: "data") as! NSDictionary).value(forKey: "contribution") != nil {
                        
                        self.dictProfileDetail = (dictResponse.value(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        
                        self.txtFirstName.text = self.dictProfileDetail.value(forKey: "first_name") as? String
                        self.txtLastName.text = self.dictProfileDetail.value(forKey: "last_name") as? String
                        self.txtEmail.text = self.dictProfileDetail.value(forKey: "email_address") as? String
                        
                        self.imgProfilePic.sd_setImage(with: NSURL(string: self.dictProfileDetail.value(forKey: "profile_pic") as! String) as URL!)
                        
                        return
                    }
                }
                
                ProjectUtility.displayTost(erroemessage: dictResponse.value(forKey: "message") as! String)
                return
            }
            
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
            
        }) { (task, error) in
            print(error)
            ProjectUtility.loadingHide()
            
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    func updateUserProfileJson() {
        
        ProjectUtility.loadingShow()
        
        let dict = ["user_id": Constant.appDelegate.dictLoginUserDetail.value(forKey: "user_id") as! String,"first_name": txtFirstName.text!, "last_name": txtLastName.text!] as NSDictionary
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/profile/updatepersonalinfo"
        print("\(strUrl) --- \(dict)")
        
        let manager = AFHTTPSessionManager()
        manager.requestSerializer.timeoutInterval = 600
        
        manager.post(strUrl, parameters: dict, constructingBodyWith: { (formData) in
            
            if self.isEditedImage == true {
                var imgVenue: UIImage = UIImage()
                imgVenue = ObjectiveCMethods.scaleAndRotateImage(self.imgProfilePic.image)
                let data = UIImagePNGRepresentation(imgVenue)!
                
                formData.appendPart(withFileData: data, name: "profile_pic", fileName: ObjectiveCMethods.findUniqueSavePathImage(), mimeType: "image/jpeg")
            }
            
        }, progress: { (uploadProgress) in
            
            print("InProgress")
        }, success: { (task, responseObject) in
            
            ProjectUtility.loadingHide()
            print(responseObject!)
            
            if responseObject != nil, let dictResponse = responseObject as? NSDictionary {
                
                if dictResponse.value(forKey: "success") != nil && dictResponse.value(forKey: "success") as! Bool == true{
                    
                    for controller: Any in (self.navigationController?.viewControllers)! {
                        if (controller is DashboardVC) {
                            self.navigationController?.popToViewController(controller as! AdvanceSearchVC, animated: false)
                            return
                        }
                    }
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = (storyboard.instantiateViewController(withIdentifier: "AdvanceSearchVC") as! AdvanceSearchVC)
                    SlideNavigationController.sharedInstance().pushViewController(controller, animated: false)
                    
                    if self.isEditedImage == true {
                        Constant.appDelegate.dictLoginUserDetail.setValue((dictResponse.value(forKey: "data") as! NSDictionary).value(forKey: "profile_pic") as! String, forKey: "profile_pic")
                    }
                    Constant.appDelegate.dictLoginUserDetail.setValue(self.txtFirstName.text, forKey: "first_name")
                    Constant.appDelegate.dictLoginUserDetail.setValue(self.txtLastName.text, forKey: "last_name")
                    
                    let archivedUser = NSKeyedArchiver.archivedData(withRootObject: Constants.appDelegate.dictLoginUserDetail)
                    UserDefaults.standard.setValue(archivedUser, forKey: "UserDetail")
                    UserDefaults.standard.synchronize()
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadUserData"), object: nil, userInfo: nil)
                    
                    return
                }
            }
            
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
            
        }) { (task, error) in
            print(error)
            ProjectUtility.loadingHide()
            
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    // MARK:- Button TouchUp
    @IBAction func btnMenuAction (_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnProfilePicAction (_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        alert.addAction(UIAlertAction.init(title: "Take Photo", style: UIAlertActionStyle.default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imag = UIImagePickerController()
                imag.delegate = self
                imag.sourceType = .camera
                self.present(imag, animated: true, completion: nil)
            }
        })
        alert.addAction(UIAlertAction.init(title: "Choose Photo", style: UIAlertActionStyle.default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary){
                let imag = UIImagePickerController()
                imag.delegate = self
                imag.sourceType = .photoLibrary
                self.present(imag, animated: true, completion: nil)
            }
        })
        alert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel) { (action) in
        })
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnUpdateProfileAction (_ sender: UIButton) {
        self.updateUserProfileJson()
    }
    
    //MARK: ImagePicker Delegate
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        isEditedImage = true
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imgProfilePic.image = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    // MARK:- TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtFirstName {
            self.changeTextFiledLineColor(textField, imgFirstName)
        }else if textField == txtLastName {
            self.changeTextFiledLineColor(textField, imgLastName)
        }
    }
    
    @IBAction func textFieldEditingChanged(_ textField: UITextField) {
        
        if textField == txtFirstName {
            self.changeTextFiledLineColor(textField, imgFirstName)
        }else if textField == txtLastName {
            self.changeTextFiledLineColor(textField, imgLastName)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        imgFirstName.backgroundColor = UIColor.lightGray
        imgLastName.backgroundColor = UIColor.lightGray
    }
    
    func changeTextFiledLineColor(_ textField: UITextField, _ imageView: UIImageView){
        
        if textField.text == "" {
            imageView.backgroundColor = UIColor.lightGray
        }else{
            imageView.backgroundColor = UIColor.init(red: 99.0 / 255.0, green: 160.0 / 255.0, blue: 248.0 / 255.0, alpha: 1.0)
        }
    }
}
