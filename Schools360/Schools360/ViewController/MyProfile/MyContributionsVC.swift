//
//  MyContributionsVC.swift
//  EdYouRate
//
//  Created by Sandeep on 18/09/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit

class MyContributionsVC: UIViewController {
    
    @IBOutlet var tblMyContribution: UITableView!
    
    var arrContributions = NSMutableArray()
    
    fileprivate (set) var scrollBar : SwiftyVerticalScrollBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblMyContribution.rowHeight = UITableViewAutomaticDimension
        tblMyContribution.estimatedRowHeight = 110
        
        self.getUserProfileJson()
        
        self.scrollBar = SwiftyVerticalScrollBar(frame: CGRect.zero, targetScrollView: self.tblMyContribution)
        self.view.addSubview(self.scrollBar!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.scrollBar.frame = CGRect(x: Constant.ScreenSize.SCREEN_WIDTH-20, y: 75, width: 20, height: Constant.ScreenSize.SCREEN_HEIGHT - 75)
    }
    
    // MARK:- Webservice Call
    func getUserProfileJson() {
        
        ProjectUtility.loadingShow()
        
        let dict = ["user_id": Constant.appDelegate.dictLoginUserDetail.value(forKey: "user_id") as! String] as NSDictionary
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/profile/userprofile"
        print("\(strUrl) --- \(dict)")
        
        let manager = AFHTTPSessionManager()
        manager.requestSerializer.timeoutInterval = 600
        
        manager.post(strUrl, parameters: dict, progress: { (progress) in
            print("InProgress")
        }, success: { (task, responseObject) in
            
            ProjectUtility.loadingHide()
            print(responseObject!)
            
            if responseObject != nil, let dictResponse = responseObject as? NSDictionary {
                
                if dictResponse.value(forKey: "success") != nil && dictResponse.value(forKey: "success") as! Bool == true{
                    
                    if (dictResponse.value(forKey: "data") as! NSDictionary).value(forKey: "contribution") != nil {
                        self.arrContributions = ((dictResponse.value(forKey: "data") as! NSDictionary).value(forKey: "contribution") as! NSArray).mutableCopy() as! NSMutableArray
                        self.tblMyContribution.reloadData()
                        
                        return
                    }
                }
                
                ProjectUtility.displayTost(erroemessage: dictResponse.value(forKey: "message") as! String)
                return
            }
            
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
            
        }) { (task, error) in
            print(error)
            ProjectUtility.loadingHide()
            
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    // MARK:- Button TouchUp
    @IBAction func btnMenuAction (_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrContributions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        
        let cell:MyContributionTableCell = tableView.dequeueReusableCell(withIdentifier: "MyContributionTableCell", for: indexPath as IndexPath) as! MyContributionTableCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell.lblQuestion.text = (arrContributions.object(at: indexPath.row) as! NSDictionary).value(forKey: "question") as? String
        cell.lblAnswer.text = (arrContributions.object(at: indexPath.row) as! NSDictionary).value(forKey: "status") as? String
        
        cell.imgBG.layer.shadowRadius = 3
        cell.imgBG.layer.shadowOffset = CGSize.init(width: 0, height: 3)
        cell.imgBG.layer.shadowColor = UIColor.init(red: 39.0/255.0, green: 46.0/255.0, blue: 54.0/255.0, alpha: 1.0).cgColor
        cell.imgBG.layer.shadowOpacity = 0.75
        
        return cell
    }

}

class MyContributionTableCell: UITableViewCell {
    
    @IBOutlet var lblQuestion: UILabel!
    @IBOutlet var lblAnswer: UILabel!
    
    @IBOutlet var imgBG: UIImageView!
}
