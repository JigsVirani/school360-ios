
//
//  ChangePasswordVC.swift
//  Schools360
//
//  Created by Sandeep Gangajaliya on 01/08/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {
    
    //MARK:- Outlet Declaration
    @IBOutlet var btnChangePassword: UIButton!
    
    @IBOutlet var txtCurrentPassword: JVFloatLabeledTextField!
    @IBOutlet var txtNewPassword: JVFloatLabeledTextField!
    @IBOutlet var txtConfirmPassword: JVFloatLabeledTextField!
    
    @IBOutlet var imgCurrentPassword: UIImageView!
    @IBOutlet var imgNewPassword: UIImageView!
    @IBOutlet var imgConfirmPassword: UIImageView!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialization()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Initialization
    func initialization() {
        btnChangePassword.layer.cornerRadius = 20
    }
    
    // MARK:- Webservice Call
    func updatePasswordJson() {
        ProjectUtility.loadingShow()
        
        let dict = ["user_id": Constant.appDelegate.dictLoginUserDetail.value(forKey: "user_id") as! String, "current_password": txtCurrentPassword.text!, "password": txtNewPassword.text!] as NSDictionary
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/login/updatepassword"
        print("\(strUrl) --- \(dict)")
        
        let manager = AFHTTPSessionManager()
        manager.requestSerializer.timeoutInterval = 600
        
        manager.post(strUrl, parameters: dict, progress: { (progress) in
            print("InProgress")
        }, success: { (task, responseObject) in
            
            ProjectUtility.loadingHide()
            print(responseObject!)
            
            if responseObject != nil, let dictResponse = responseObject as? NSDictionary {
                
                if dictResponse.value(forKey: "success") != nil && dictResponse.value(forKey: "success") as! Bool == true{
                    
                    Constant.appDelegate.dictLoginUserDetail.setValue(self.txtNewPassword.text, forKey: "password")
                    
                    for controller: Any in (self.navigationController?.viewControllers)! {
                        if (controller is DashboardVC) {
                            self.navigationController?.popToViewController(controller as! AdvanceSearchVC, animated: false)
                            return
                        }
                    }
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = (storyboard.instantiateViewController(withIdentifier: "AdvanceSearchVC") as! AdvanceSearchVC)
                    SlideNavigationController.sharedInstance().pushViewController(controller, animated: false)
                }
                
                ProjectUtility.displayTost(erroemessage: dictResponse.value(forKey: "message") as! String)
                return
            }
            
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
            
        }) { (task, error) in
            print(error)
            ProjectUtility.loadingHide()
            
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    // MARK:- Button TouchUp
    @IBAction func btnBackAction (_ sender: UIButton){
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnChangePasswordAction (_ sender: UIButton){
        self.view.endEditing(true)
        if txtCurrentPassword.text != Constant.appDelegate.dictLoginUserDetail.value(forKey: "password") as? String {
            ProjectUtility.displayTost(erroemessage: "Current password is not correct")
        
        }else if txtNewPassword.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please enter new password")
        }else if txtNewPassword.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please enter new password")
        }else if (txtNewPassword.text?.characters.count)! < 6{
            ProjectUtility.displayTost(erroemessage: "The password must be at least 6 characters")
        }else if txtNewPassword.text != txtConfirmPassword.text{
            ProjectUtility.displayTost(erroemessage: "Your password is not matching with confirm password")
        }else{
            self.updatePasswordJson()
        }
    }
    
    // MARK:- TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtCurrentPassword {
            self.changeTextFiledLineColor(textField, imgCurrentPassword)
        }else if textField == txtNewPassword {
            self.changeTextFiledLineColor(textField, imgNewPassword)
        }else if textField == txtConfirmPassword {
            self.changeTextFiledLineColor(textField, imgConfirmPassword)
        }
    }
    
    @IBAction func textFieldEditingChanged(_ textField: UITextField) {
        
        if textField == txtCurrentPassword {
            self.changeTextFiledLineColor(textField, imgCurrentPassword)
        }else if textField == txtNewPassword {
            self.changeTextFiledLineColor(textField, imgNewPassword)
        }else if textField == txtConfirmPassword {
            self.changeTextFiledLineColor(textField, imgConfirmPassword)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        imgCurrentPassword.backgroundColor = UIColor.lightGray
        imgNewPassword.backgroundColor = UIColor.lightGray
        imgConfirmPassword.backgroundColor = UIColor.lightGray
    }
    
    func changeTextFiledLineColor(_ textField: UITextField, _ imageView: UIImageView){
        
        if textField.text == "" {
            imageView.backgroundColor = UIColor.lightGray
        }else{
            imageView.backgroundColor = UIColor.init(red: 99.0 / 255.0, green: 160.0 / 255.0, blue: 248.0 / 255.0, alpha: 1.0)
        }
    }
}
