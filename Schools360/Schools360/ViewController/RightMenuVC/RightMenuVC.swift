//
//  LeftMenuVC.swift
//  yump
//
//  Created by Fusion on 16/07/16.
//  Copyright © 2016 Fusion. All rights reserved.
//

import UIKit
import Social

class RightMenuVC: UIViewController
{
    //MARK:- Otlet Declaration
    @IBOutlet var tblMenuList: UITableView!
    @IBOutlet var constViewMainLeading: NSLayoutConstraint!
    
    //MARK: Variable Declaration
    var cellIdentifier = NSString()
    var arrMenuTitle : NSMutableArray = ["", "My Profile", "My Contribution", "Logout"]

    //MARK:- View Life Cycle
    
    override func viewDidLoad(){
        super.viewDidLoad()
                
        NotificationCenter.default.addObserver(self, selector: #selector(RightMenuVC.reloadUserData), name: NSNotification.Name(rawValue: "reloadUserData"), object: nil)
        
        tblMenuList.register(UINib.init(nibName: "RightMenuCell", bundle: nil), forCellReuseIdentifier: "RightMenuCell")
        tblMenuList.register(UINib.init(nibName: "MenuProfileCell", bundle: nil), forCellReuseIdentifier: "MenuProfileCell")
        
        constViewMainLeading.constant = UIScreen.main.bounds.size.width * 0.3867
        
        self.reloadUserData()
    }
    
    func reloadUserData(){
        
        tblMenuList.reloadData()
        
        constViewMainLeading.constant = UIScreen.main.bounds.size.width * 0.3867
    }

    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews(){
    }
    
    //MARK:- Tableview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return arrMenuTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        
        if indexPath.row == 0 {
            let cell:MenuProfileCell = tableView.dequeueReusableCell(withIdentifier: "MenuProfileCell", for: indexPath as IndexPath) as! MenuProfileCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            cell.imgProfilePic.layer.cornerRadius = 61
            
            if Constant.appDelegate.dictLoginUserDetail.value(forKey: "profile_pic") != nil {
                cell.imgProfilePic.sd_setImage(with: NSURL(string: Constant.appDelegate.dictLoginUserDetail.value(forKey: "profile_pic") as! String) as URL!)
            }
            
            cell.lblId.text = "# \(Constants.appDelegate.dictLoginUserDetail.value(forKey: "user_id")!)"
            
            return cell;
        }else{
            
            let cell:RightMenuCell = tableView.dequeueReusableCell(withIdentifier: "RightMenuCell", for: indexPath as IndexPath) as! RightMenuCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            cell.imgBG.image = UIImage.init(named: "")
            cell.lblTitle.isHidden = false
            
            cell.lblTitle.text = arrMenuTitle.object(at: indexPath.row) as? String
            
            return cell;
        }
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        
        let parentViewController = SlideNavigationController.sharedInstance().viewControllers.last!
        let className = NSStringFromClass(parentViewController.classForCoder)
      
        if arrMenuTitle.object(at: indexPath.row) as! String == "My Profile" {
            if className == "Schools360.MyProfileVC"{
                SlideNavigationController.sharedInstance().closeMenu(completion: nil)
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = (storyboard.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC)
                SlideNavigationController.sharedInstance().pushViewController(controller, animated: false)
            }
        }else if arrMenuTitle.object(at: indexPath.row) as! String == "My Contribution" {
            if className == "Schools360.MyContributionsVC"{
                SlideNavigationController.sharedInstance().closeMenu(completion: nil)
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = (storyboard.instantiateViewController(withIdentifier: "MyContributionsVC") as! MyContributionsVC)
                SlideNavigationController.sharedInstance().pushViewController(controller, animated: false)
            }
        }else if arrMenuTitle.object(at: indexPath.row) as! String == "Logout" {
            SlideNavigationController.sharedInstance().popToRootViewController(animated: true)

            UserDefaults.standard.removeObject(forKey: "UserDetail")
            UserDefaults.standard.synchronize()

        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat{
        
        if indexPath.row == 0 {
            return 166
        }else{
            return 60
        }
    }
}
