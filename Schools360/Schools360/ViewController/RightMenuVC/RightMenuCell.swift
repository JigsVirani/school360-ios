//
//  RightMenuCell.swift
//  PTO
//
//  Created by fiplmac1 on 23/08/16.
//  Copyright © 2016 fusion. All rights reserved.
//

import UIKit

class RightMenuCell: UITableViewCell
{
    @IBOutlet var imgBG: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    
    override func awakeFromNib()
    {
        imgBG.layer.shadowRadius = 6
        imgBG.layer.shadowOffset = CGSize.init(width: 0, height: 8)
        imgBG.layer.shadowColor = UIColor.init(red: 39.0/255.0, green: 46.0/255.0, blue: 54.0/255.0, alpha: 1.0).cgColor
        imgBG.layer.shadowOpacity = 0.75
        
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
}
