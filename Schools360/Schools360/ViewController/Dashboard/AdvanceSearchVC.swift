//
//  AdvanceSearchVC.swift
//  Schools360
//
//  Created by Sandeep Gangajaliya on 26/04/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit

class AdvanceSearchVC: UIViewController {

    //MARK:- Outlet Declaration
    
    @IBOutlet var btnSelectCurriculum: UIButton!
    @IBOutlet var btnSelectResources: UIButton!
    @IBOutlet var btnSearch: UIButton!
    
    @IBOutlet var txtCountry: SearchTextField!
    @IBOutlet var txtCurriculum: UITextField!
    @IBOutlet var txtResources: UITextField!
    
    @IBOutlet var imgCountryLine: UIImageView!
    
    @IBOutlet var tblAdvanceSearch: UITableView!
    
    @IBOutlet var btnSearchFooter:UIButton!
    @IBOutlet var lblSearch:UILabel!
    
    // MARK: Resources Popup
    @IBOutlet var viewResourcesPopup: UIView!
    @IBOutlet var viewResourcesPopupIn: UIView!
    @IBOutlet var tblResources: UITableView!
    
    //MARK: Other Variables
    var arrOptions = NSMutableArray()
    var arrResources = NSMutableArray()
    var arrAdvanceSearchResult = NSMutableArray()
    var arrSelectedOptions = NSMutableArray()
    var dictSelectedOptionsKey = NSMutableDictionary()
    var arrSelectedResources = NSMutableArray()
    var arrSelectedQuestions = NSMutableArray()
    
    fileprivate (set) var scrollBar : SwiftyVerticalScrollBar!
    
    var intSelectedCount = 0
    var intPackageSelectionInLine = 0
    var intQuestionSelectionInLine = 0
    
    //MARK: DropDowns
    let curriculumDropDown = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.curriculumDropDown
        ]
    }()
    
    //MARK:TypeAndSearch
    var countryResults = [SearchTextFieldItem]()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialization()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = false
        
        if UIApplication.shared.statusBarOrientation != UIInterfaceOrientation.portrait {
            let value = UIInterfaceOrientation.portrait.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
        }
        
        self.scrollBar = SwiftyVerticalScrollBar(frame: CGRect.zero, targetScrollView: self.tblAdvanceSearch)
        self.view.addSubview(self.scrollBar!)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadUserData"), object: nil, userInfo: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.scrollBar.frame = CGRect(x: Constant.ScreenSize.SCREEN_WIDTH-20, y: 67, width: 20, height: Constant.ScreenSize.SCREEN_HEIGHT - 127)
    }
    
    // MARK:- Initialization
    
    func initialization(){
        
        ProjectUtility.setCommonButton(button: btnSearch)
        
        setupDropDowns()
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        
        DropDown.setupDefaultAppearance()
        
        self.configureCountrySearchTextField()
        
        viewResourcesPopup.frame = CGRect(x: 0, y: 0, width: Constants.ScreenSize.SCREEN_WIDTH, height: Constants.ScreenSize.SCREEN_HEIGHT)
        self.view.addSubview(viewResourcesPopup)
        viewResourcesPopup.isHidden = true
        
        viewResourcesPopupIn.layer.cornerRadius = 5.0
        
        self.createOptionArray()
        self.createResourcesArray()
        
        if UserDefaults.standard.value(forKey: "AdvanceSearchAlertPopup") == nil {
            let alert = UIAlertController(title: "", message: "EdYouRate allows you to select up to 10 variables in the database at a time.\n\nYou can filter the schools you choose by country, curriculum and/or resources.\n\nThe default is all schools.\n\nWithin the results you can sort each of the 10 attributes you have chosen from high to low to help you better understand how schools compare.\n\nYou can also click on the school's name in the results, and be taken to the complete results for that school.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            alert.addAction(UIAlertAction.init(title: "Don't show again", style: UIAlertActionStyle.default, handler: { (action) in
                    let archivedUser = NSKeyedArchiver.archivedData(withRootObject: "Yes")
                    UserDefaults.standard.setValue(archivedUser, forKey: "AdvanceSearchAlertPopup")
                    UserDefaults.standard.synchronize()
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- Webservice Call
    func advanceSearchJson()
    {
        arrSelectedOptions = NSMutableArray()
        dictSelectedOptionsKey = NSMutableDictionary()
        arrSelectedResources = NSMutableArray()
        arrSelectedQuestions = NSMutableArray()
        
        arrSelectedOptions.add("Country Name")
        dictSelectedOptionsKey.setValue("country_name", forKey: "Country Name")
        
        let dict = NSMutableDictionary()
        
        if txtCountry.accessibilityIdentifier != nil {
            dict.setValue(txtCountry.accessibilityIdentifier!, forKey:"country_id")
        }
        
        dictSelectedOptionsKey.setValue("school_name", forKey: "School Name")
        
        if txtCurriculum.text != "" && txtCurriculum.text != "All Curriculum Or Select One" {
            dict.setValue(txtCurriculum.tag, forKey:"tech_id")
            
            arrSelectedOptions.add("Curriculum taught")
            dictSelectedOptionsKey.setValue("tech_id", forKey: "Curriculum taught")
        }else if txtCurriculum.text == "All Curriculum Or Select One"{
            dict.setValue("", forKey:"tech_id")
        }
        
        var strResources = ""
        for i in 0 ..< arrResources.count {
            
            if (arrResources.object(at: i) as! NSMutableDictionary).value(forKey: "isSelected") != nil && (arrResources.object(at: i) as! NSMutableDictionary).value(forKey: "isSelected") as! String == "true" {
                
                if strResources != "" {
                    strResources = strResources + ","
                }
                strResources = strResources + "\(i+1)"
                
                arrSelectedOptions.add((arrResources.object(at: i) as! NSMutableDictionary).value(forKey: "title") as! String)
                dictSelectedOptionsKey.setValue((arrResources.object(at: i) as! NSMutableDictionary).value(forKey: "title") as! String, forKey: (arrResources.object(at: i) as! NSMutableDictionary).value(forKey: "title") as! String)
                
                arrSelectedResources.add((arrResources.object(at: i) as! NSMutableDictionary).value(forKey: "title") as! String)
            }
        }
        
        if strResources != "" {
            dict.setValue(strResources, forKey:"resources_id")
        }
        
        var strQuestionId = ""
        var strIsSorting = ""
        for i in 0 ..< arrOptions.count {
            
            let dictOptionDetail = arrOptions.object(at: i) as! NSMutableDictionary
            
            if dictOptionDetail.value(forKey: "isSelected") != nil && dictOptionDetail.value(forKey: "isSelected") as! String == "true" {
                
                if i < 13 && dict.value(forKey: "sorted_value") == nil {
                    
                    dict.setValue(dictOptionDetail.value(forKey: "title") as! String, forKey:"sorted_value")
                }
                if i < 13 {
                        
                    for j in 0 ..< 13 {
                        if dictOptionDetail.value(forKey: "selectionNumber") as! NSInteger != 0 && dictOptionDetail.value(forKey: "selectionNumber") as! NSInteger == j {
                        
                            if strIsSorting != "" {
                                strIsSorting = strIsSorting + ","
                            }
                            
                            strIsSorting = strIsSorting + (dictOptionDetail.value(forKey: "key") as!  String)
                            
                            var strKeyValue = dictOptionDetail.value(forKey: "key") as!  String
                            
                            if strKeyValue == "responsibility_point" {
                                strKeyValue = "responsiblity_point"
                            }
                            
                            if strKeyValue == "shippinng_allowance_id" {
                                strKeyValue = "shipping_allowance"
                            }
                            
                            if strKeyValue == "housing_allowance_id" {
                                strKeyValue = "housing_allowance"
                            }
                            
                            if strKeyValue == "gratuity_id" {
                                strKeyValue = "gratuity"
                            }
                            
                            if strKeyValue == "health_insurance_id" {
                                strKeyValue = "health_insurance"
                            }
                            
                            if strKeyValue == "leave_id" {
                                strKeyValue = "annual_leave"
                            }
                            
                            dictSelectedOptionsKey.setValue(strKeyValue, forKey: dictOptionDetail.value(forKey: "title") as! String)
                            
                            arrSelectedOptions.add(dictOptionDetail.value(forKey: "title") as! String)
                            
                            break
                        }
                    }
                }else{
                    arrSelectedQuestions.add(dictOptionDetail.value(forKey: "title") as! String)
                }
            }
        }
        
        for j in 1 ..< 24 {
            
            for i in 13 ..< arrOptions.count {
                
                let dictOptionDetail = arrOptions.object(at: i) as! NSMutableDictionary
            
                if dictOptionDetail.value(forKey: "selectionNumber") as! NSInteger != 0 && dictOptionDetail.value(forKey: "selectionNumber") as! NSInteger == j {
                    
                    if strQuestionId != "" {
                        
                        strQuestionId = strQuestionId + ","
                    }
                    strQuestionId = strQuestionId + "\(i-13+1)"
                    
                    dictSelectedOptionsKey.setValue("\(i-13+1)", forKey: dictOptionDetail.value(forKey: "title") as! String)
                    
                    arrSelectedOptions.add(dictOptionDetail.value(forKey: "title") as! String)
                    break
                }
            }
        }
        
        if strIsSorting != "" {
            dict.setValue(strIsSorting, forKey:"ios_sorting")
        }
        
        if strQuestionId != "" {
            dict.setValue(strQuestionId, forKey:"question_id")
        }
        
        ProjectUtility.loadingShow()
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/home/advancesearch"
        print("\(strUrl) --- \(dict)")
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            ProjectUtility.loadingHide()
            
            print(response?.webserviceResponse)
            
            if response?.webserviceResponse != nil{

                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        self.arrAdvanceSearchResult = (dictData.value(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                        
                        if self.txtCurriculum.text != "" && self.txtCurriculum.text != "All Curriculum Or Select One" {
                            for i in 0 ..< self.arrAdvanceSearchResult.count{
                                
                                let dict = (self.arrAdvanceSearchResult.object(at: i) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                                
                                dict.setValue(self.txtCurriculum.text, forKey: "tech_id")
                                
                                self.arrAdvanceSearchResult.replaceObject(at:i, with: dict)
                            }
                        }
                        
                        self.performSegue(withIdentifier: "advanceSearchResult", sender: nil)
                        return
                    }
                    
                    ProjectUtility.displayTost(erroemessage: "No result found as per your requirement.")
                    return
                    
                }
                ProjectUtility.displayTost(erroemessage: "We are having some problem")
            }
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
            
        }) { (error) -> Void in
            
            print(error)
            ProjectUtility.loadingHide()
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    fileprivate func searchCountryJson(_ criteria: String, callback: @escaping ((_ results: [SearchTextFieldItem]) -> Void)) {
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/home/countrylist"
        print("\(strUrl)")
        
        webserviceCall.get(NSURL(string: strUrl) as URL!, parameters: nil as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            print(response?.webserviceResponse)
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        for result in dictData.value(forKey: "data") as! NSArray {
                            self.countryResults.append(SearchTextFieldItem(title: (result as! NSDictionary).value(forKey: "name") as! String, subtitle: "", image: nil, id: (result as! NSDictionary).value(forKey: "id") as? String))
                        }
                        
                        DispatchQueue.main.async {
                            callback(self.countryResults)
                        }
                    }else {
                        DispatchQueue.main.async {
                            callback([])
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        callback([])
                    }
                }
            }else {
                DispatchQueue.main.async {
                    callback([])
                }
            }
            
        }) { (error) -> Void in
            
            print(error!)
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    // MARK:- Button TouchUp
    
    @IBAction func btnBackAction (_ sender: UIButton){
        
        SlideNavigationController.sharedInstance().open(MenuLeft, withCompletion: nil)
    }
    
    @IBAction func btnResetAction (_ sender: UIButton){
        
        txtCountry.text = ""
        txtCountry.accessibilityIdentifier = ""
        txtCurriculum.text = ""
        txtResources.text = ""
        
        for i in 0 ..< arrOptions.count {
            
            if (arrOptions.object(at: i) as! NSMutableDictionary).value(forKey: "title") as! String != "" {
                (arrOptions.object(at: i) as! NSMutableDictionary).setValue("false", forKey: "isSelected")
            }
        }
        intSelectedCount = 0
        intPackageSelectionInLine = 0
        intQuestionSelectionInLine = 0
        
        for i in 0 ..< arrResources.count {
            
            (arrResources.object(at: i) as! NSMutableDictionary).setValue("false", forKey: "isSelected")
        }
        
        tblResources.reloadData()
        tblAdvanceSearch.reloadData()
    }
    
    @IBAction func btnSelectCurriculumAction (_ sender: UIButton){
        curriculumDropDown.show()
    }
    
    @IBAction func btnSelectResourcesAction (_ sender: UIButton){
        self.viewResourcesPopup.isHidden = false
        ProjectUtility.animatePopupView(viewPopup: self.viewResourcesPopup)
    }
    
    @IBAction func btnSearchAction (_ sender: UIButton){
        self.advanceSearchJson()
    }
    
    @IBAction func btnFooterActionAction (_ sender: UIButton) {
        
        if sender.tag != 1 {
            ProjectUtility.footerTabActions(intTag: sender.tag, viewController: self, navigationController: self.navigationController!)
        }
    }
    
    // MARK: Resources Popup
    @IBAction func btnDoneAction (_ sender: UIButton){
        self.viewResourcesPopup.isHidden = true
        
        var strResources = ""
        for i in 0 ..< arrResources.count {
            
            if (arrResources.object(at: i) as! NSMutableDictionary).value(forKey: "isSelected") != nil && (arrResources.object(at: i) as! NSMutableDictionary).value(forKey: "isSelected") as! String == "true" {
                
                if strResources != "" {
                    strResources = strResources + ", "
                }
                
                strResources = strResources + ((arrResources.object(at: i) as! NSMutableDictionary).value(forKey: "title") as! String)
            }
        }
        
        txtResources.text = strResources
    }
    
    //MARK:- Tableview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if tableView == tblAdvanceSearch {
            return arrOptions.count
        }else{
            return arrResources.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        
        if tableView == tblAdvanceSearch {
            if (arrOptions.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "title") as! String != "" {
                
                let cell:AdvanceSearchTableCell = tableView.dequeueReusableCell(withIdentifier: "AdvanceSearchTableCell", for: indexPath as IndexPath) as! AdvanceSearchTableCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                cell.lblTitle.text = (arrOptions.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "title") as? String
                
                if (arrOptions.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "isSelected") != nil && (arrOptions.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "isSelected") as! String == "true" {
                    
                    cell.imgCheckBox.image = UIImage(named:"checked_radio")
                }else{
                    cell.imgCheckBox.image = UIImage(named:"unchecked_radio")
                }
                
                cell.imgBG.layer.shadowRadius = 3
                cell.imgBG.layer.shadowOffset = CGSize.init(width: 0, height: 3)
                cell.imgBG.layer.shadowColor = UIColor.init(red: 39.0/255.0, green: 46.0/255.0, blue: 54.0/255.0, alpha: 1.0).cgColor
                cell.imgBG.layer.shadowOpacity = 0.75
                
                return cell;
                
            }else{
                
                let cell:AdvanceSearchLineTableCell = tableView.dequeueReusableCell(withIdentifier: "AdvanceSearchLineTableCell", for: indexPath as IndexPath) as! AdvanceSearchLineTableCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                return cell
            }
        }else{
            let cell:AdvanceSearchTableCell = tableView.dequeueReusableCell(withIdentifier: "AdvanceSearchTableCell", for: indexPath as IndexPath) as! AdvanceSearchTableCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            cell.lblTitle.text = (arrResources.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "title") as? String
            
            if (arrResources.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "isSelected") != nil && (arrResources.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "isSelected") as! String == "true" {
                
                cell.imgCheckBox.image = UIImage(named:"checked_radio")
            }else{
                cell.imgCheckBox.image = UIImage(named:"unchecked_radio")
            }
            
            cell.imgBG.layer.shadowRadius = 3
            cell.imgBG.layer.shadowOffset = CGSize.init(width: 0, height: 3)
            cell.imgBG.layer.shadowColor = UIColor.init(red: 39.0/255.0, green: 46.0/255.0, blue: 54.0/255.0, alpha: 1.0).cgColor
            cell.imgBG.layer.shadowOpacity = 0.75
            
            return cell;
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        if tableView == tblAdvanceSearch {
            if (arrOptions.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "title") as! String != "" {
                
                if (arrOptions.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "isSelected") != nil && (arrOptions.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "isSelected") as! String == "true"{
                    
                    (arrOptions.object(at: indexPath.row) as! NSMutableDictionary).setValue("false", forKey: "isSelected")
                    
                    intSelectedCount = intSelectedCount - 1
                    
                    if indexPath.row < 12 {
                        intPackageSelectionInLine = intPackageSelectionInLine - 1
                    }else{
                        intQuestionSelectionInLine = intQuestionSelectionInLine - 1
                    }
                    
                    let intQuestionSelectionNumber = (arrOptions.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "selectionNumber") as! NSInteger
                    
                    (arrOptions.object(at: indexPath.row) as! NSMutableDictionary).setValue(0, forKey: "selectionNumber")
                    
                    for i in 0 ..< arrOptions.count {
                        
                        if i != 0 {
                            if intQuestionSelectionNumber < (arrOptions.object(at: i) as! NSMutableDictionary).value(forKey: "selectionNumber") as! NSInteger {
                                
                                var intTemp = (arrOptions.object(at: i) as! NSMutableDictionary).value(forKey: "selectionNumber") as! NSInteger
                                
                                intTemp = intTemp - 1
                                
                                (arrOptions.object(at: i) as! NSMutableDictionary).setValue(intTemp, forKey: "selectionNumber")
                            }
                        }
                    }
                }else{
                    
                    if intSelectedCount == 10 {
                        ProjectUtility.displayTost(erroemessage: "You have selected the maximum limit.")
                        return
                    }
                    
                    (arrOptions.object(at: indexPath.row) as! NSMutableDictionary).setValue("true", forKey: "isSelected")
                    
                    intSelectedCount = intSelectedCount + 1
                    
                    if indexPath.row <= 12 {
                        intPackageSelectionInLine = intPackageSelectionInLine + 1
                        (arrOptions.object(at: indexPath.row) as! NSMutableDictionary).setValue(intPackageSelectionInLine, forKey: "selectionNumber")
                    }else{
                        intQuestionSelectionInLine = intQuestionSelectionInLine + 1
                        (arrOptions.object(at: indexPath.row) as! NSMutableDictionary).setValue(intQuestionSelectionInLine, forKey: "selectionNumber")
                    }
                }
                tblAdvanceSearch.reloadData()
            }
        }else{
            if (arrResources.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "isSelected") != nil && (arrResources.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "isSelected") as! String == "true"{
                
                (arrResources.object(at: indexPath.row) as! NSMutableDictionary).setValue("false", forKey: "isSelected")
            }else{
                (arrResources.object(at: indexPath.row) as! NSMutableDictionary).setValue("true", forKey: "isSelected")
            }
            tblResources.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAtIndexPath indexPath: IndexPath){
            //            CellAnimator.animateCell(cell: cell, withTransform: CellAnimator.TransformWave, andDuration: 1)
        
        if tableView == tblAdvanceSearch {
            let cellContentView = cell.contentView
            let rotationAngleDegrees: CGFloat = -30
            let rotationAngleRadians: CGFloat = rotationAngleDegrees * (CGFloat(M_PI) / 180)
            let offsetPositioning = CGPoint(x: 500, y: -20.0)
            var transform = CATransform3DIdentity
            transform = CATransform3DRotate(transform, rotationAngleRadians, -50.0, 0.0, 1.0)
            transform = CATransform3DTranslate(transform, offsetPositioning.x, offsetPositioning.y, -50.0)
            cellContentView.layer.transform = transform
            cellContentView.layer.opacity = 0.8
            UIView.animate(withDuration: 0.9, delay: 0.1, usingSpringWithDamping: 0.85, initialSpringVelocity: 0.8, options: [], animations: {() -> Void in
                cellContentView.layer.transform = CATransform3DIdentity
                cellContentView.layer.opacity = 1
                }, completion: {(finished: Bool) -> Void in
            })
        }
    }
    
    // MARK:- TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtCountry {
            self.changeTextFiledLineColor(textField, imgCountryLine)
        }
    }
    
    @IBAction func textFieldEditingChanged(_ textField: UITextField) {
        
        if textField == txtCountry {
            self.changeTextFiledLineColor(textField, imgCountryLine)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        imgCountryLine.backgroundColor = UIColor.lightGray
    }
    
    func changeTextFiledLineColor(_ textField: UITextField, _ imageView: UIImageView){
        
        if textField.text == "" {
            imageView.backgroundColor = UIColor.lightGray
        }else{
            imageView.backgroundColor = UIColor.init(red: 99.0 / 255.0, green: 160.0 / 255.0, blue: 248.0 / 255.0, alpha: 1.0)
        }
    }
    
    // MARK:- SetUp Dropdown
    func setupDropDowns() {
        setupCurrculumDropDown()
    }
    
    func setupCurrculumDropDown() {
        curriculumDropDown.anchorView = btnSelectCurriculum
        curriculumDropDown.bottomOffset = CGPoint(x: 0, y: 40)
        
        curriculumDropDown.dataSource = ["All Curriculum Or Select One", "PHP", "MYP", "IBDP", "British", "GCSE / IGCSE", "A-Level", "American", "AP", "Indian", "Canadian", "Australian", "Other"]
        
        curriculumDropDown.selectionAction = { [unowned self] (index, item) in
            self.txtCurriculum.text = item
            self.txtCurriculum.tag = index
        }
    }
    
    //MARK:- Create Option Array
    
    func createOptionArray(){
        
        var dict = NSMutableDictionary()
        dict.setValue("", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("School size", forKey: "title")
        dict.setValue("school_size", forKey: "key")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Salary low", forKey: "title")
        dict.setValue("salary_low", forKey: "key")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Salary high", forKey: "title")
        dict.setValue("salary_high", forKey: "key")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Responsibility point", forKey: "title")
        dict.setValue("responsibility_point", forKey: "key")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Housing allowance", forKey: "title")
        dict.setValue("housing_allowance_id", forKey: "key")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("School fees", forKey: "title")
        dict.setValue("school_fees", forKey: "key")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Health insurance", forKey: "title")
        dict.setValue("health_insurance_id", forKey: "key")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Flight allowance", forKey: "title")
        dict.setValue("flight_allowance", forKey: "key")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Shipping allowance", forKey: "title")
        dict.setValue("shippinng_allowance_id", forKey: "key")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Gratuity", forKey: "title")
        dict.setValue("gratuity_id", forKey: "key")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Leave allowance", forKey: "title")
        dict.setValue("leave_id", forKey: "key")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Principal fair", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Senior management fair", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Line manager fair", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Opinions valued", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Friendly atmosphere", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Will recommend school", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Condition of school", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Teaching resources", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Academic standards", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Friendly admin", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Honours contracts", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Evalutions transparent", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("PD opportunities", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("International PD opportunities", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Creature comfort level", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Shopping standard", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Variety of weekend activities", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Children's activities", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Health care facilities", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Safety level", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Friendly locals", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Living enjoyment", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Easy to save money", forKey: "title")
        dict.setValue(0, forKey: "selectionNumber")
        arrOptions.add(dict)
    }
    
    func createResourcesArray(){
        
        var dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Football field", forKey: "title")
        arrResources.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Athletics track", forKey: "title")
        arrResources.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Swimming pool", forKey: "title")
        arrResources.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Gymnasium", forKey: "title")
        arrResources.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Sports hall", forKey: "title")
        arrResources.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Library", forKey: "title")
        arrResources.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Auditorium", forKey: "title")
        arrResources.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Computer labs", forKey: "title")
        arrResources.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("School bus service", forKey: "title")
        arrResources.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Theatre", forKey: "title")
        arrResources.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Duke of Edinburgh Award", forKey: "title")
        arrResources.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("International Award", forKey: "title")
        arrResources.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("CCA / ECA program", forKey: "title")
        arrResources.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("High speed WiFi", forKey: "title")
        arrResources.add(dictQuestion)
        
        dictQuestion = NSMutableDictionary()
        dictQuestion.setValue("Multi vendor canteen", forKey: "title")
        arrResources.add(dictQuestion)
    }
    
    //MARK:- Set Suggetion Box
    fileprivate func configureCountrySearchTextField() {
        
        txtCountry.maxNumberOfResults = 100
        txtCountry.maxResultsListHeight = 200
        txtCountry.theme.font = UIFont.init(name: "Avenir Next", size: 12)!
        
        txtCountry.itemSelectionHandler = { item, itemPosition in
            
            self.view.endEditing(true)
            self.txtCountry.text = item.title
            self.txtCountry.accessibilityIdentifier = item.id
        }
        
        txtCountry.userStoppedTypingHandler = {
            self.txtCountry.accessibilityIdentifier = ""
            
            if let criteria = self.txtCountry.text {
                if criteria.characters.count > 0 {
                    
                    if self.countryResults.count == 0 {
                        self.txtCountry.showLoadingIndicator()
                        
                        self.searchCountryJson(criteria) { results in
                            
                            self.txtCountry.filterItems(results)
                            self.txtCountry.stopLoadingIndicator()
                        }
                    }else{
                        self.txtCountry.showLoadingIndicator()
                        self.txtCountry.filterItems(self.countryResults)
                        self.txtCountry.stopLoadingIndicator()
                    }
                }
            }
        }
    }
    
    // MARK:- Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let vc: AdvanceSearchResultVC = segue.destination as! AdvanceSearchResultVC
        vc.arrAdvanceSearchResult = arrAdvanceSearchResult
        vc.arrSelectedOptions = arrSelectedOptions
        vc.dictSelectedOptionsKey = dictSelectedOptionsKey
        vc.arrResources = arrSelectedResources
        vc.arrQuestions = arrSelectedQuestions
    }
}

class AdvanceSearchTableCell: UITableViewCell {
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var imgCheckBox: UIImageView!
    @IBOutlet var imgBG: UIImageView!
}

class AdvanceSearchLineTableCell: UITableViewCell {
    @IBOutlet var imgLine: UIImageView!
}

