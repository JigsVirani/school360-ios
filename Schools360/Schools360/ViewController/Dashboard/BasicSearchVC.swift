//
//  BasicSearchVC.swift
//  Schools360
//
//  Created by Sandeep Gangajaliya on 24/05/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit

class BasicSearchVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK:- Outlet Declaration
    
    @IBOutlet var lblSchoolName: UILabel!
    @IBOutlet var lblViews: UILabel!
    
    @IBOutlet var txtCountry: UITextField!
    @IBOutlet var txtState: UITextField!
    @IBOutlet var txtCity: UITextField!
    @IBOutlet var txtCurriculumTaught: UITextField!
    @IBOutlet var txtSchoolSize: UITextField!
    @IBOutlet var txtMonthlySalaryLow: UITextField!
    @IBOutlet var txtMonthlySalaryHigh: UITextField!
    @IBOutlet var txtResponsibilityPoint: UITextField!
    @IBOutlet var txtHousingAllowance: UITextField!
    @IBOutlet var txtSchoolFees: UITextField!
    @IBOutlet var txtHealthInsurance: UITextField!
    @IBOutlet var txtFlightsAllowance: UITextField!
    @IBOutlet var txtShippingAllowance: UITextField!
    @IBOutlet var txtGratuity: UITextField!
    @IBOutlet var txtAnnualLeave: UITextField!
    
    @IBOutlet var tblResources: UITableView!
    @IBOutlet var tblQuestions: UITableView!
    
    @IBOutlet var viewQuestionHeader: UIView!
    
    @IBOutlet var constTableResourcesHeight: NSLayoutConstraint!
    @IBOutlet var constViewTableHeaderHeight: NSLayoutConstraint!
    
    fileprivate (set) var scrollBar : SwiftyVerticalScrollBar!
    
    //MARK: Variable Declaration
    var dictBasicSearchResult = NSMutableDictionary()
    var arrResources = NSMutableArray()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialization()
        
        tblQuestions.rowHeight = UITableViewAutomaticDimension
        tblQuestions.estimatedRowHeight = 219.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if UIApplication.shared.statusBarOrientation != UIInterfaceOrientation.portrait {
            let value = UIInterfaceOrientation.portrait.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.scrollBar.frame = CGRect(x: Constant.ScreenSize.SCREEN_WIDTH-20, y: 75, width: 20, height: Constant.ScreenSize.SCREEN_HEIGHT - 136)
    }
    
    // MARK:- Initialization
    
    func initialization(){
        
        self.scrollBar = SwiftyVerticalScrollBar(frame: CGRect.zero, targetScrollView: self.tblQuestions)
        self.view.addSubview(self.scrollBar!)
        
        lblSchoolName.text = (dictBasicSearchResult.value(forKey: "school_data") as! NSDictionary).value(forKey: "school_name") as? String
        
        lblViews.text = "Reviews : " + "\((dictBasicSearchResult.value(forKey: "school_data") as! NSDictionary).value(forKey: "views") as! String)"
        
        txtCountry.text = (dictBasicSearchResult.value(forKey: "school_data") as! NSDictionary).value(forKey: "country_name") as? String
        txtState.text = (dictBasicSearchResult.value(forKey: "school_data") as! NSDictionary).value(forKey: "state_name") as? String
        txtCity.text = (dictBasicSearchResult.value(forKey: "school_data") as! NSDictionary).value(forKey: "city_name") as? String
        txtCurriculumTaught.text = (dictBasicSearchResult.value(forKey: "school_data") as! NSDictionary).value(forKey: "tech_name") as? String
        txtSchoolSize.text = (dictBasicSearchResult.value(forKey: "school_data") as! NSDictionary).value(forKey: "school_size") as? String
        txtMonthlySalaryLow.text = (dictBasicSearchResult.value(forKey: "school_data") as! NSDictionary).value(forKey: "salary_low") as? String
        txtMonthlySalaryHigh.text = (dictBasicSearchResult.value(forKey: "school_data") as! NSDictionary).value(forKey: "salary_high") as? String
        txtResponsibilityPoint.text = (dictBasicSearchResult.value(forKey: "school_data") as! NSDictionary).value(forKey: "responsiblity_point") as? String
        txtHousingAllowance.text = (dictBasicSearchResult.value(forKey: "school_data") as! NSDictionary).value(forKey: "housing_allowance") as? String
        txtSchoolFees.text = (dictBasicSearchResult.value(forKey: "school_data") as! NSDictionary).value(forKey: "school_fees") as? String
        txtHealthInsurance.text = (dictBasicSearchResult.value(forKey: "school_data") as! NSDictionary).value(forKey: "health_insurance") as? String
        txtFlightsAllowance.text = (dictBasicSearchResult.value(forKey: "school_data") as! NSDictionary).value(forKey: "flight_allowance") as? String
        txtShippingAllowance.text = (dictBasicSearchResult.value(forKey: "school_data") as! NSDictionary).value(forKey: "shipping_allowance") as? String
        txtGratuity.text = (dictBasicSearchResult.value(forKey: "school_data") as! NSDictionary).value(forKey: "gratuity") as? String
        txtAnnualLeave.text = (dictBasicSearchResult.value(forKey: "school_data") as! NSDictionary).value(forKey: "annual_leave") as? String
        
        self.createResourcesArray()
    }
    
    // MARK:- Button TouchUp
    
    @IBAction func btnBackAction (_ sender: UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Cell Events
    @IBAction func btnInfoAction (_ sender: UIButton){
        let alert = UIAlertController(title: "", message: "The score for each question is based on the score users gave during the registration process.\n\nThe score ranges from 1 - 4. A score of 1 is assigned to “DISAGREE” and a score of 4 is assigned to \"AGREE\".\n\nIf two or more teachers have rated a school, then the average of their responses for each questions is given.\n\nThe ranking system is also based on the score for each question. For example, if there are two schools in a city, then the school with the higher average for that question will rank 1/2. The same is the case for the regional, country and world rank.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Tableview Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if tableView == tblResources {
            return arrResources.count
        }else if tableView == tblQuestions {
            return (dictBasicSearchResult.value(forKey: "school_question") as! NSArray).count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if tableView == tblResources {
            
            let cell:AdvanceSearchTableCell = tableView.dequeueReusableCell(withIdentifier: "AdvanceSearchTableCell", for: indexPath as IndexPath) as! AdvanceSearchTableCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            cell.lblTitle.text = (arrResources.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "title") as? String
            
//            let arrResoucesId = dictBasicSearchResult.value(forKey: "resorces_data") as! NSArray
//            
//            for i in 0 ..< arrResoucesId.count {
//                
//                if indexPath.row + 1 == NSInteger(arrResoucesId.object(at: i) as! String) {
                    cell.imgCheckBox.image = UIImage(named:"checked_radio")
//                    break
//                }else{
//                    cell.imgCheckBox.image = UIImage(named:"unchecked_radio")
//                }
//            }
            cell.imgBG.layer.shadowRadius = 3
            cell.imgBG.layer.shadowOffset = CGSize.init(width: 0, height: 3)
            cell.imgBG.layer.shadowColor = UIColor.init(red: 39.0/255.0, green: 46.0/255.0, blue: 54.0/255.0, alpha: 1.0).cgColor
            cell.imgBG.layer.shadowOpacity = 0.75
            
            return cell
        }else if tableView == tblQuestions{
            
            let cell:BasicSearchQuestionsTableCell = tableView.dequeueReusableCell(withIdentifier: "BasicSearchQuestionsTableCell", for: indexPath as IndexPath) as! BasicSearchQuestionsTableCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            let dictTemp = (dictBasicSearchResult.value(forKey: "school_question") as! NSArray).object(at: indexPath.row) as! NSDictionary
            
            cell.lblQuestion.text = dictTemp.value(forKey: "question") as? String
            
            cell.lblScore.text = "\(dictTemp.value(forKey: "score") as! String)/\(dictTemp.value(forKey: "score_outof") as! NSInteger)"
            
            cell.lblCityRank.text = "\(dictTemp.value(forKey: "score_of_city") as! NSInteger)/\(dictTemp.value(forKey: "score_outof_city") as! NSInteger)"
            
            cell.lblRegionalRank.text = "\(dictTemp.value(forKey: "score_of_regional") as! NSInteger)/\(dictTemp.value(forKey: "score_outof_regional") as! NSInteger)"
            
            cell.lblCountryRank.text = "\(dictTemp.value(forKey: "score_of_country") as! NSInteger)/\(dictTemp.value(forKey: "score_outof_country") as! NSInteger)"
            
            cell.lblWorldRank.text = "\(dictTemp.value(forKey: "score_of_world") as! NSInteger)/\(dictTemp.value(forKey: "score_outof_world") as! NSInteger)"
            
            cell.btnInfo.addTarget(self, action: #selector(btnInfoAction(_:)), for: UIControlEvents.touchUpInside)
            
            cell.imgQuestionBG.layer.shadowRadius = 6
            cell.imgQuestionBG.layer.shadowOffset = CGSize.init(width: 0, height: 8)
            cell.imgQuestionBG.layer.shadowColor = UIColor.init(red: 39.0/255.0, green: 46.0/255.0, blue: 54.0/255.0, alpha: 1.0).cgColor
            cell.imgQuestionBG.layer.shadowOpacity = 0.75
            
            return cell
        }
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath as IndexPath)
        
        return cell
    }
    
    //MARK:- Create Option Array
    
    func createResourcesArray(){
        
        let arrTemp = NSMutableArray()
        
        var dict = NSMutableDictionary()
        dict.setValue("Football field", forKey: "title")
        arrTemp.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Athletics track", forKey: "title")
        arrTemp.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Swimming pool", forKey: "title")
        arrTemp.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Gymnasium", forKey: "title")
        arrTemp.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Sports hall", forKey: "title")
        arrTemp.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Library", forKey: "title")
        arrTemp.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Auditorium", forKey: "title")
        arrTemp.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Computer labs", forKey: "title")
        arrTemp.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("School bus service", forKey: "title")
        arrTemp.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Theatre", forKey: "title")
        arrTemp.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Duke of Edinburgh Award", forKey: "title")
        arrTemp.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("International Award", forKey: "title")
        arrTemp.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("CCA / ECA program", forKey: "title")
        arrTemp.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("High speed wifi", forKey: "title")
        arrTemp.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Multi vendor canteen", forKey: "title")
        arrTemp.add(dict)
        
        let arrResourcesId = dictBasicSearchResult.value(forKey: "resorces_data") as! NSArray
        for i in 0 ..< arrResourcesId.count {
            
            for j in 0 ..< arrTemp.count {
                if j + 1 == NSInteger(arrResourcesId.object(at: i) as! String) {
                    
                    if (arrResources.value(forKey: "title") as AnyObject).index(of: (arrTemp.object(at: j) as! NSMutableDictionary).value(forKey: "title") as AnyObject) == NSNotFound{
                        
                        arrResources.add(arrTemp.object(at: j))
                        break
                    }
                }
            }
        }
        
        constTableResourcesHeight.constant = CGFloat(arrResources.count * 44)
        viewQuestionHeader.frame = CGRect.init(x: 0, y: 0, width: Int(Constant.ScreenSize.SCREEN_WIDTH), height: 891 + (arrResources.count * 44))
    }
}

class BasicSearchTableCell: UITableViewCell {
    
    @IBOutlet var txtCountry: UITextField!
    @IBOutlet var txtState: UITextField!
    @IBOutlet var txtCity: UITextField!
    @IBOutlet var txtCurriculumTaught: UITextField!
    @IBOutlet var txtSchoolSize: UITextField!
    @IBOutlet var txtMonthlySalaryLow: UITextField!
    @IBOutlet var txtMonthlySalaryHigh: UITextField!
    @IBOutlet var txtResponsibilityPoint: UITextField!
    @IBOutlet var txtHousingAllowance: UITextField!
    @IBOutlet var txtSchoolFees: UITextField!
    @IBOutlet var txtHealthInsurance: UITextField!
    @IBOutlet var txtFlightsAllowance: UITextField!
    @IBOutlet var txtShippingAllowance: UITextField!
    @IBOutlet var txtGratuity: UITextField!
    @IBOutlet var txtAnnualLeave: UITextField!
    
    @IBOutlet var tblResources: UITableView!
    @IBOutlet var tblQuestions: UITableView!
    
    @IBOutlet var constTableQuestionsHeight: NSLayoutConstraint!
}

class BasicSearchQuestionsTableCell: UITableViewCell {
    @IBOutlet var imgQuestionBG: UIImageView!
    
    @IBOutlet var lblQuestion: UILabel!
    @IBOutlet var lblScore: UILabel!
    @IBOutlet var lblCityRank: UILabel!
    @IBOutlet var lblRegionalRank: UILabel!
    @IBOutlet var lblCountryRank: UILabel!
    @IBOutlet var lblWorldRank: UILabel!
    
    @IBOutlet var btnInfo: UIButton!
}
