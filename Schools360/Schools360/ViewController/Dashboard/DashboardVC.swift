//
//  DashboardVC.swift
//  Schools360
//
//  Created by Sandeep Gangajaliya on 26/04/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit

class DashboardVC: UIViewController {

    //MARK:- Outlet Declaration
    @IBOutlet var txtSchoolName: SearchTextField!
    @IBOutlet var imgSearchSchoolLine: UIImageView!
    
    @IBOutlet var btnSearch: UIButton!
    @IBOutlet var btnAdvanceSearch: UIButton!
    @IBOutlet var btnForum: UIButton!
    @IBOutlet var btnMyProfile: UIButton!
    
    //MARK: Variable Declaration
    var dictBasicSearchResult = NSMutableDictionary()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ProjectUtility.setCommonButton(button: btnSearch)
        ProjectUtility.setCommonButton(button: btnAdvanceSearch)
        ProjectUtility.setCommonButton(button: btnForum)
        ProjectUtility.setCommonButton(button: btnMyProfile)
        
        self.configureSchoolSearchTextField()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
    }
    
    // MARK:- Webservice Call
    
    func basicSearchJson() {
        ProjectUtility.loadingShow()
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        let dict = ["school_name":txtSchoolName.accessibilityIdentifier!]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/home/basicsearch"
        print("\(strUrl) --- \(dict)")
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            ProjectUtility.loadingHide()
            
            print(response?.webserviceResponse)
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                     
                        self.dictBasicSearchResult = (dictData.value(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        
                        self.performSegue(withIdentifier: "basicSearch", sender: nil)
                        
                        return
                    }
                    ProjectUtility.displayTost(erroemessage: dictData.value(forKey: "message") as! String)
                    return
                }
            }
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
            
        }) { (error) -> Void in
            
            print(error)
            ProjectUtility.loadingHide()
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    fileprivate func searchSchoolNameJson(_ criteria: String, callback: @escaping ((_ results: [SearchTextFieldItem]) -> Void)) {
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        let dict = ["terms":txtSchoolName.text!]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/home/searchsuggestion"
        print("\(strUrl) --- \(dict)")
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            print(response?.webserviceResponse)
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        var results = [SearchTextFieldItem]()
                        
                        for result in dictData.value(forKey: "data") as! NSArray {
                            results.append(SearchTextFieldItem(title: (result as! NSDictionary).value(forKey: "school_name") as! String, subtitle: "", image: nil, id: (result as! NSDictionary).value(forKey: "id") as? String))
                        }
                        
                        DispatchQueue.main.async {
                            callback(results)
                        }
                    }else {
                        DispatchQueue.main.async {
                            callback([])
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        callback([])
                    }
                }
            }else {
                DispatchQueue.main.async {
                    callback([])
                }
            }
            
        }) { (error) -> Void in
            
            print(error)
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    // MARK:- Button TouchUp
    
    @IBAction func btnMenuAction (_ sender: UIButton) {
        
        SlideNavigationController.sharedInstance().open(MenuLeft, withCompletion: nil)
    }
    
    @IBAction func btnSearchAction (_ sender: UIButton) {
        
        if txtSchoolName.text == "" {
            
            ProjectUtility.displayTost(erroemessage: "Please enter shcool name")
        }else if txtSchoolName.accessibilityIdentifier! == ""{
            
            ProjectUtility.displayTost(erroemessage: "Please selecte school from drop down box")
        }
        else{
            self.basicSearchJson()
        }
    }
    
    // MARK:- TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtSchoolName {
            self.changeTextFiledLineColor(textField, imgSearchSchoolLine)
        }
    }
    
    @IBAction func textFieldEditingChanged(_ textField: UITextField) {
        
        if textField == txtSchoolName {
            self.changeTextFiledLineColor(textField, imgSearchSchoolLine)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        imgSearchSchoolLine.backgroundColor = UIColor.lightGray
    }
    
    func changeTextFiledLineColor(_ textField: UITextField, _ imageView: UIImageView){
        
        if textField.text == "" {
            imageView.backgroundColor = UIColor.lightGray
        }else{
            imageView.backgroundColor = UIColor.init(red: 99.0 / 255.0, green: 160.0 / 255.0, blue: 248.0 / 255.0, alpha: 1.0)
        }
    }
    
    //MARK:- Set Suggetion Box
    fileprivate func configureSchoolSearchTextField() {
        
        txtSchoolName.maxNumberOfResults = 100
        txtSchoolName.maxResultsListHeight = 200
        txtSchoolName.theme.font = UIFont.init(name: "Avenir Next", size: 12)!
        
        txtSchoolName.itemSelectionHandler = { item, itemPosition in
            
            self.view.endEditing(true)
            self.txtSchoolName.text = item.title
            self.txtSchoolName.accessibilityIdentifier = item.id
        }
        
        txtSchoolName.userStoppedTypingHandler = {
            self.txtSchoolName.accessibilityIdentifier = ""
            if let criteria = self.txtSchoolName.text {
                if criteria.characters.count > 1 {
                    
                    self.txtSchoolName.showLoadingIndicator()
                    
                    self.searchSchoolNameJson(criteria) { results in
                        
                        self.txtSchoolName.filterItems(results)
                        self.txtSchoolName.stopLoadingIndicator()
                    }
                }
            }
        }
    }
    
    // MARK:- Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "basicSearch" {
            
            let vc: BasicSearchVC = segue.destination as! BasicSearchVC
            vc.dictBasicSearchResult = dictBasicSearchResult
        }
    }
}
