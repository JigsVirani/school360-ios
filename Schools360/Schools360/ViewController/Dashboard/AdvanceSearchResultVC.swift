//
//  AdvanceSearchResultVC.swift
//  Schools360
//
//  Created by Sandeep Gangajaliya on 24/05/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit

class AdvanceSearchResultVC: UIViewController, KEZCollectionViewDelegateTableLayout, SortingPopupDelegate {

    //MARK:- Outlet Declaration
    
    var dataSource = KEZTableLabelDataSource()
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var viewGoThrought: UIView!
    
    @IBOutlet var constBottomToLeftArrowLeading: NSLayoutConstraint!
    
    fileprivate (set) var scrollBar : SwiftyVerticalScrollBar!
    
    //MARK: School Name Delegate
    var loginController = LoginVC()
    
    var useCornerDecoration = Bool()
    var useRowHeaders = Bool()
    var useColumnHeaders = Bool()
    
    //MARK: Other Variables
    var arrAdvanceSearchResult = NSMutableArray()
    var arrSortedAdvanceSearchResult = NSMutableArray()
    var arrSelectedOptions = NSMutableArray()
    var arrResources = NSMutableArray()
    var arrQuestions = NSMutableArray()
    
    var arrSchoolSize: NSArray = ["Less than 500 students", "500 - 900 students","1000 - 1500 students","More than 1500"]
    var arrSchoolFees: NSArray = ["No school fees provided","Provided for 1 child","Provided for 2 children","Provided for 3 children","Provided for more than 3 children","Other"]
    var arrShippingAllowance: NSArray = ["No shipping allowance","US$1 - 500","US$501 - 1000","US$1000 - 1500","More than US$ 1500"]
    var arrAnnualLeave: NSArray = ["Less than 3 weeks","3 weeks","4 weeks","5 weeks","6 weeeks","7 weeks","8 weeks","9 weeks","10 weeks","11 weeks","12 weeks","13 weeks","14 weeks","15 weeks","16 weeks","more than 16 weeks"]
    
    var dictSelectedOptionsKey = NSMutableDictionary()
    var dictBasicSearchResult = NSMutableDictionary()
    
    var heighestCellWidth = 0.0
    
    //MARK: DropDowns
    let openDropDown = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.openDropDown
        ]
    }()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(arrSelectedOptions)
        print(dictSelectedOptionsKey)
        
        arrSortedAdvanceSearchResult = arrAdvanceSearchResult.mutableCopy() as! NSMutableArray
        
        self.initialization()
        
        NotificationCenter.default.addObserver(self, selector: #selector(opneSchoolNameDropDown(notification:)), name: NSNotification.Name(rawValue: "opneSchoolNameDropDown"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let value = UIInterfaceOrientation.landscapeRight.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.scrollBar.frame = CGRect(x: Constant.ScreenSize.SCREEN_HEIGHT-20, y: 75, width: 20, height: Constant.ScreenSize.SCREEN_WIDTH - 84)
    }
    
    func canRotate() -> Void {}
    
    // MARK:- Initialization
    func initialization(){
        
        self.collectionView.register(KEZLabeledHeaderView.self, forSupplementaryViewOfKind: KEZCollectionViewTableLayoutSupplementaryViewRowHeader, withReuseIdentifier: "LabelHeader")
        self.collectionView.register(KEZLabeledHeaderView.self, forSupplementaryViewOfKind: KEZCollectionViewTableLayoutSupplementaryViewColumnHeader, withReuseIdentifier: "LabelHeader")
        self.collectionView.collectionViewLayout.register(KEZCornerCell.self, forDecorationViewOfKind: KEZCollectionViewTableLayoutDecorationViewCornerCell)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tap(_:)))
        viewGoThrought.addGestureRecognizer(tapGesture)
        
        let arrSchoolName = NSMutableArray()
        
        for i in 0 ..< arrSortedAdvanceSearchResult.count {
            arrSchoolName.add((arrSortedAdvanceSearchResult.object(at: i) as! NSDictionary).value(forKey: "school_name") as! String)
        }
        
        dataSource = KEZTableLabelDataSource.sampleDataSource(withRows: arrSchoolName, columns: arrSelectedOptions, rowHeaders: true, columnHeaders: true, data:arrSortedAdvanceSearchResult, dict: dictSelectedOptionsKey)
        collectionView.dataSource = dataSource
        
        dataSource.sortingPopupDelegate = self
        
        dataSource.arrResources = arrResources
        dataSource.arrQuestions = arrQuestions
        
        self.scrollBar = SwiftyVerticalScrollBar(frame: CGRect.zero, targetScrollView: self.collectionView)
        self.view.addSubview(self.scrollBar!)
        
        self.collectionView.reloadData()

        if UserDefaults.standard.value(forKey: "AdvanceSearchGoThrough") == nil {
            let archivedUser = NSKeyedArchiver.archivedData(withRootObject: "Yes")
            UserDefaults.standard.setValue(archivedUser, forKey: "AdvanceSearchGoThrough")
            UserDefaults.standard.synchronize()
        }else{
            viewGoThrought.isHidden = true
        }
    }
    
    // MARK:- Webservice Call
    
    func getSchoolIdJson(strSchoolName: String){
        ProjectUtility.loadingShow()
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        let dict = ["terms":strSchoolName]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/home/searchsuggestion"
        print("\(strUrl) --- \(dict)")
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            ProjectUtility.loadingHide()
            
            print(response?.webserviceResponse!)
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        self.basicSearchJson(strSchoolId: ((dictData.value(forKey: "data") as! NSArray).object(at: 0) as! NSDictionary).value(forKey: "id") as! String)
                        
                        return
                    }
                    if dictData.value(forKey: "message") != nil{
                        ProjectUtility.displayTost(erroemessage: dictData.value(forKey: "message") as! String)
                        return
                    }
                }
            }
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
            
        }) { (error) -> Void in
            
            print(error!)
            ProjectUtility.loadingHide()
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    func basicSearchJson(strSchoolId: String){
        ProjectUtility.loadingShow()
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        let dict = ["school_name":strSchoolId]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/home/basicsearch"
        print("\(strUrl) --- \(dict)")
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            ProjectUtility.loadingHide()
            
            print(response?.webserviceResponse!)
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        self.dictBasicSearchResult = (dictData.value(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        
                        self.performSegue(withIdentifier: "basicSearch", sender: nil)
                        
                        return
                    }
                    if dictData.value(forKey: "message") != nil{
                        ProjectUtility.displayTost(erroemessage: dictData.value(forKey: "message") as! String)
                        return
                    }
                }
            }
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
            
        }) { (error) -> Void in
            
            print(error!)
            ProjectUtility.loadingHide()
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    // MARK:- Button TouchUp
    @IBAction func btnBackAction (_ sender: UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Geture
    func tap(_ sender: UITapGestureRecognizer) {
        viewGoThrought.isHidden = true
    }
    
    //MARK:- CollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, layout: KEZCollectionViewTableLayout, sizeForCellAt indexPath: IndexPath) -> CGSize {
        let label: String = dataSource.label(at: indexPath)
        let attributes: NSDictionary = [NSFontAttributeName: UIFont.systemFont(ofSize: CGFloat(16.0))]//[NSFontAttributeName: UIFont.init(name: "Avenir Next Regular", size: 13.0)]
        let size: CGSize = label.size(attributes: attributes as? [String : Any])
        let adjustedSize = CGSize(width: CGFloat(ceil(size.width)+14), height: CGFloat(30.0))//ceil(size.height)))
        return adjustedSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout: KEZCollectionViewTableLayout, supplementaryViewOfKind kind: String, sizeAt indexPath: IndexPath) -> CGSize {
        if (kind == KEZCollectionViewTableLayoutSupplementaryViewRowHeader) {
            let label = self.dataSource.rowHeaderTitle(at: indexPath as IndexPath!)
//            print(label!)
            let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)]
            let size = label?.size(attributes: attributes)
            let adjustedSize = CGSize.init(width: (size?.width)! + 14, height: 30.0)//(size?.height)!)
            
            if CGFloat(heighestCellWidth) < adjustedSize.width {
                
                heighestCellWidth = Double(adjustedSize.width)
                constBottomToLeftArrowLeading.constant = CGFloat(heighestCellWidth/2)
            }
            
            return adjustedSize
        }
        else if (kind == KEZCollectionViewTableLayoutSupplementaryViewColumnHeader) {
            let label = self.dataSource.columnHeaderTitle(at: indexPath as IndexPath!)
            let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)]
            let size = label?.size(attributes: attributes)
            let adjustedSize = CGSize.init(width: (size?.width)! + 14, height: 30.0)//(size?.height)!)
            return adjustedSize
        }
        return CGSize.zero
    }
    
    // MARK:- SetUp Dropdown
    func showDropDown(btnDropDown: UIButton) {
        openDropDown.anchorView = btnDropDown
        openDropDown.bottomOffset = CGPoint(x: 0, y: 40)
        
        openDropDown.dataSource = ["Ascending", "Descending", "None"]
        
        openDropDown.selectionAction = { [unowned self] (index, item) in
            
            self.arrSortedAdvanceSearchResult = NSMutableArray()
            
            let arrTemp = self.arrAdvanceSearchResult.mutableCopy() as! NSMutableArray
            
            print(self.dictSelectedOptionsKey.value(forKey: btnDropDown.accessibilityIdentifier!) as! String)
            
            if btnDropDown.accessibilityIdentifier! == "School size" {
                if index == 0 {
                    
                    for i in 0 ..< self.arrSchoolSize.count {
                        let predicate = NSPredicate(format: "school_size == %@", self.arrSchoolSize.object(at: i) as! String)
                        self.arrSortedAdvanceSearchResult.addObjects(from: ((arrTemp as NSArray).filtered(using: predicate) as! NSMutableArray) as! [Any])
                    }
                }else if index == 1{
                    
                    for i in stride(from: self.arrSchoolSize.count, to: 0, by: -1) {
                        let predicate = NSPredicate(format: "school_size == %@", self.arrSchoolSize.object(at: i-1) as! String)
                        self.arrSortedAdvanceSearchResult.addObjects(from: ((arrTemp as NSArray).filtered(using: predicate) as! NSMutableArray) as! [Any])
                    }
                }
            }else if btnDropDown.accessibilityIdentifier! == "School fees"{
                if index == 0 {
                    
                    for i in 0 ..< self.arrSchoolFees.count {
                        let predicate = NSPredicate(format: "school_fees == %@", self.arrSchoolFees.object(at: i) as! String)
                        self.arrSortedAdvanceSearchResult.addObjects(from: ((arrTemp as NSArray).filtered(using: predicate) as! NSMutableArray) as! [Any])
                    }
                }else if index == 1{
                    
                    for i in stride(from: self.arrSchoolFees.count, to:  0, by: -1) {
                        let predicate = NSPredicate(format: "school_fees == %@", self.arrSchoolFees.object(at: i-1) as! String)
                        self.arrSortedAdvanceSearchResult.addObjects(from: ((arrTemp as NSArray).filtered(using: predicate) as! NSMutableArray) as! [Any])
                    }
                }
            }else if btnDropDown.accessibilityIdentifier! == "Shipping allowance"{
                if index == 0 {
                    
                    for i in 0 ..< self.arrShippingAllowance.count {
                        let predicate = NSPredicate(format: "shipping_allowance == %@", self.arrShippingAllowance.object(at: i) as! String)
                        self.arrSortedAdvanceSearchResult.addObjects(from: ((arrTemp as NSArray).filtered(using: predicate) as! NSMutableArray) as! [Any])
                    }
                }else if index == 1{
                    
                    for i in stride(from: self.arrShippingAllowance.count, to:  0, by: -1) {
                        let predicate = NSPredicate(format: "shipping_allowance == %@", self.arrShippingAllowance.object(at: i-1) as! String)
                        self.arrSortedAdvanceSearchResult.addObjects(from: ((arrTemp as NSArray).filtered(using: predicate) as! NSMutableArray) as! [Any])
                    }
                }
            }else if btnDropDown.accessibilityIdentifier! == "Leave allowance"{
                if index == 0 {
                    
                    for i in 0 ..< self.arrAnnualLeave.count {
                        let predicate = NSPredicate(format: "annual_leave == %@", self.arrAnnualLeave.object(at: i) as! String)
                        self.arrSortedAdvanceSearchResult.addObjects(from: ((arrTemp as NSArray).filtered(using: predicate) as! NSMutableArray) as! [Any])
                    }
                }else if index == 1{
                    
                    for i in stride(from: self.arrAnnualLeave.count, to:  0, by: -1) {
                        let predicate = NSPredicate(format: "annual_leave == %@", self.arrAnnualLeave.object(at: i-1) as! String)
                        self.arrSortedAdvanceSearchResult.addObjects(from: ((arrTemp as NSArray).filtered(using: predicate) as! NSMutableArray) as! [Any])
                    }
                }
            }else if btnDropDown.accessibilityIdentifier! == "Salary low" || btnDropDown.accessibilityIdentifier! == "Salary high" || btnDropDown.accessibilityIdentifier! == "Responsibility point"{
                
                if index == 0 {
                    let sortDescriptor = NSSortDescriptor(key: "\(self.dictSelectedOptionsKey.value(forKey: btnDropDown.accessibilityIdentifier!) as! String).floatValue", ascending: true)
                    self.arrSortedAdvanceSearchResult = (arrTemp as NSArray).sortedArray(using: [sortDescriptor]) as! NSMutableArray
                }else if index == 1{
                    let sortDescriptor = NSSortDescriptor(key: "\(self.dictSelectedOptionsKey.value(forKey: btnDropDown.accessibilityIdentifier!) as! String).floatValue", ascending: false)
                    self.arrSortedAdvanceSearchResult = (arrTemp as NSArray).sortedArray(using: [sortDescriptor]) as! NSMutableArray
                }
                
            }else if index == 0 {
                
                let brandDescriptor = NSSortDescriptor(key: self.dictSelectedOptionsKey.value(forKey: btnDropDown.accessibilityIdentifier!) as? String, ascending: true)
                let sortDescriptors = [brandDescriptor]
                self.arrSortedAdvanceSearchResult = ((arrTemp as NSArray).sortedArray(using: sortDescriptors) as NSArray).mutableCopy() as! NSMutableArray
            }else if index == 1 {
                
                let brandDescriptor = NSSortDescriptor(key: self.dictSelectedOptionsKey.value(forKey: btnDropDown.accessibilityIdentifier!) as? String, ascending: false)
                let sortDescriptors = [brandDescriptor]
                self.arrSortedAdvanceSearchResult = ((arrTemp as NSArray).sortedArray(using: sortDescriptors) as NSArray).mutableCopy() as! NSMutableArray
            }
            
            if index == 2 {
                self.arrSortedAdvanceSearchResult = self.arrAdvanceSearchResult.mutableCopy() as! NSMutableArray
            }
            
            self.initialization()
        }
    }
    
    //MARK:- Custom delegate
    func strSchoolName(_ strSchoolTitle: String!) {
        self.getSchoolIdJson(strSchoolName: strSchoolTitle)
    }
    
    func opneDropDown(_ btnDropDown: UIButton!) {
        self.showDropDown(btnDropDown: btnDropDown)
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        DropDown.setupDefaultAppearance()
        openDropDown.show()
    }
    
    func opneSchoolNameDropDown(notification: Notification) {
        
        print(notification)
        let btn = (notification.object as! NSMutableDictionary).value(forKey: "Button") as? UIButton
        
        self.showDropDown(btnDropDown: btn!)
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        DropDown.setupDefaultAppearance()
        openDropDown.show()
    }
    
    // MARK:- Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "basicSearch" {
            
            let vc: BasicSearchVC = segue.destination as! BasicSearchVC
            vc.dictBasicSearchResult = dictBasicSearchResult
        }
    }
}
