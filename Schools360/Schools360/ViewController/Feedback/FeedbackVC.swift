//
//  FeedbackVC.swift
//  EdYouRate
//
//  Created by Sandeep on 07/09/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit

class FeedbackVC: UIViewController {

    //MARK:- Outlet Declaration
    @IBOutlet var imgMostLike: UIImageView!
    @IBOutlet var imgImprove: UIImageView!
    @IBOutlet var txtViewMostLike: UITextView!
    @IBOutlet var txtViewImprove: UITextView!
    
    @IBOutlet var btnSubmit: UIButton!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgMostLike.layer.cornerRadius = 8
        imgMostLike.layer.borderColor = UIColor.darkGray.cgColor
        imgMostLike.layer.borderWidth = 1
        
        imgImprove.layer.cornerRadius = 8
        imgImprove.layer.borderColor = UIColor.darkGray.cgColor
        imgImprove.layer.borderWidth = 1
        
        ProjectUtility.setCommonButton(button: btnSubmit)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK:- Webservice Call
    func sendFeedbackJson() {
        
        ProjectUtility.loadingShow()
        
        let dict = ["email_address": Constant.appDelegate.dictLoginUserDetail.value(forKey: "email_address") as! String, "feedback": "", "like": txtViewMostLike.text, "suggestion": txtViewImprove.text] as NSDictionary
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/home/sendfeedback"
        print("\(strUrl) --- \(dict)")
        
        let manager = AFHTTPSessionManager()
        manager.requestSerializer.timeoutInterval = 600
        
        manager.post(strUrl, parameters: dict, progress: { (progress) in
            print("InProgress")
        }, success: { (task, responseObject) in
            
            ProjectUtility.loadingHide()
            print(responseObject!)
            
            if responseObject != nil, let dictResponse = responseObject as? NSDictionary {
                
                if dictResponse.value(forKey: "success") != nil && dictResponse.value(forKey: "success") as! Bool == true{
                    
                    self.txtViewMostLike.text = ""
                    self.txtViewImprove.text = ""
                    
                    for controller: Any in (self.navigationController?.viewControllers)! {
                        if (controller is DashboardVC) {
                            self.navigationController?.popToViewController(controller as! AdvanceSearchVC, animated: false)
                            return
                        }
                    }
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = (storyboard.instantiateViewController(withIdentifier: "AdvanceSearchVC") as! AdvanceSearchVC)
                    SlideNavigationController.sharedInstance().pushViewController(controller, animated: false)
                }
                
                ProjectUtility.displayTost(erroemessage: dictResponse.value(forKey: "message") as! String)
                return
            }
            
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
            
        }) { (task, error) in
            print(error)
            ProjectUtility.loadingHide()
            
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    // MARK:- Button TouchUp
    @IBAction func btnMenuAction (_ sender: UIButton) {
        SlideNavigationController.sharedInstance().open(MenuLeft, withCompletion: nil)
    }
    
    @IBAction func btnSubmitAction (_ sender: UIButton) {
        self.view.endEditing(true)
        if txtViewMostLike.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please enter what you like")
        }else if txtViewImprove.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please enter what you like to improve")
        }else{
            self.sendFeedbackJson()
        }
    }
    
    @IBAction func btnFooterActionAction (_ sender: UIButton) {
        
        if sender.tag != 4 {
            ProjectUtility.footerTabActions(intTag: sender.tag, viewController: self, navigationController: self.navigationController!)
        }
    }
}
