//
//  ForumVC.swift
//  Schools360
//
//  Created by Sandeep Gangajaliya on 17/07/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class ForumVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK:- Outlet Declaration
    @IBOutlet var imgSearchText: UIImageView!
    @IBOutlet var txtSearch: UITextField!
    
    @IBOutlet var viewShare: UIView!
    
    @IBOutlet var tblForum: UITableView!
    
    //MARK:- Other
    var arrForumList = NSMutableArray()
    var intPageCount = 0
    var isLoading = false
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgSearchText.layer.borderColor = UIColor.init(red: 36.0/255.0, green: 159.0/255.0, blue: 231.0/255.0, alpha: 1.0).cgColor
        imgSearchText.layer.borderWidth = 1.0
        imgSearchText.layer.cornerRadius = 17.5
        
        viewShare.layer.borderColor = UIColor.lightGray.cgColor
        viewShare.layer.borderWidth = 1.0
        viewShare.layer.cornerRadius = 8
        
        tblForum.rowHeight = UITableViewAutomaticDimension
        tblForum.estimatedRowHeight = 378.0
        
        tblForum.delegate = self
        tblForum.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tblForum.reloadData()
        
        if arrForumList.count == 0 {
            intPageCount = 0
            isLoading = true
            self.getForumListJson()
        }
    }
    
    // MARK:- Webservice Call
    func getForumListJson() {
        
        ProjectUtility.loadingShow()
        
        let dict = ["user_id": Constant.appDelegate.dictLoginUserDetail.value(forKey: "user_id") as! String, "index": intPageCount] as NSDictionary
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/forum/forumlist"
        print("\(strUrl) --- \(dict)")
        
        let manager = AFHTTPSessionManager()
        manager.requestSerializer.timeoutInterval = 600
        
        manager.post(strUrl, parameters: dict, progress: { (progress) in
            print("InProgress")
        }, success: { (task, responseObject) in
            
            self.isLoading = false
            
            ProjectUtility.loadingHide()
            print(responseObject!)
            
            if responseObject != nil, let dictResponse = responseObject as? NSDictionary {
                
                if dictResponse.value(forKey: "success") != nil && dictResponse.value(forKey: "success") as! Bool == true{
                    
                    for i in 0 ..< (dictResponse.value(forKey: "data") as! NSArray).count {
                        let dict = ((dictResponse.value(forKey: "data") as! NSArray).object(at: i) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        
                        self.arrForumList.add(dict)
                    }
                    
                    self.tblForum.reloadData()
                    
                    return
                }
                return
            }
            
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
            
        }) { (task, error) in
            print(error)
            ProjectUtility.loadingHide()
            self.isLoading = false
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    func likeDislikePostJson(index: NSInteger) {
        
        var strStatus = "1"
        if (arrForumList.object(at: index) as! NSMutableDictionary).value(forKey: "like_status") as! NSInteger == 1 {
            strStatus = "2"
        }
        
        let dict = ["user_id": Constant.appDelegate.dictLoginUserDetail.value(forKey: "user_id") as! String, "forum_id": (arrForumList.object(at: index) as! NSMutableDictionary).value(forKey: "id") as! String, "status": strStatus] as NSDictionary
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/forum/likepost"
        print("\(strUrl) --- \(dict)")
        
        let manager = AFHTTPSessionManager()
        manager.requestSerializer.timeoutInterval = 600
        
        manager.post(strUrl, parameters: dict, progress: { (progress) in
            print("InProgress")
        }, success: { (task, responseObject) in
            
            print(responseObject!)
            
            if responseObject != nil, let dictResponse = responseObject as? NSDictionary {
                
                if dictResponse.value(forKey: "success") != nil && dictResponse.value(forKey: "success") as! Bool == true{
                    
                    return
                }
                return
            }
            
        }) { (task, error) in
            print(error)
            ProjectUtility.loadingHide()
        }
    }
    
    // MARK:- Button TouchUp
    @IBAction func btnMenuAction (_ sender: UIButton) {
        SlideNavigationController.sharedInstance().open(MenuLeft, withCompletion: nil)
    }
    
    @IBAction func btnFooterActionAction (_ sender: UIButton) {
        
        if sender.tag != 2 {
            ProjectUtility.footerTabActions(intTag: sender.tag, viewController: self, navigationController: self.navigationController!)
        }
    }
    
    // MARK: CellEvents
    @IBAction func btnPlayVideoAction (_ sender: UIButton) {
        
        let videoURL = URL(string: ((arrForumList.object(at: sender.tag) as! NSMutableDictionary).value(forKey: "files") as! NSArray).object(at: 0) as! String)
        let player = AVPlayer.init(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    @IBAction func btnLikePostAction (_ sender: UIButton) {
        self.likeDislikePostJson(index: sender.tag)
        
        var intTotalCount = Int((self.arrForumList.object(at: sender.tag) as! NSMutableDictionary).value(forKey: "total_likes") as! String)
        
        if (self.arrForumList.object(at: sender.tag) as! NSMutableDictionary).value(forKey: "like_status") as! NSInteger == 1 {
            (self.arrForumList.object(at: sender.tag) as! NSMutableDictionary).setValue(0, forKey: "like_status")
            intTotalCount = intTotalCount! - 1
        }else{
            (self.arrForumList.object(at: sender.tag) as! NSMutableDictionary).setValue(1, forKey: "like_status")
            intTotalCount = intTotalCount! + 1
        }
        (self.arrForumList.object(at: sender.tag) as! NSMutableDictionary).setValue("\(intTotalCount!)", forKey: "total_likes")
        
        self.tblForum.reloadRows(at: [NSIndexPath.init(row: sender.tag, section: 0) as IndexPath], with: UITableViewRowAnimation.none)
    }
    
    @IBAction func btnPostCountAction (_ sender: UIButton) {
        self.performSegue(withIdentifier: "postedPhotoVideoSegues", sender: sender.tag)
    }
    
    @IBAction func btnCommentsAction (_ sender: UIButton) {
        self.performSegue(withIdentifier: "postCommentsSegue", sender: sender.tag)
    }
    
    //MARK:- Tableview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return arrForumList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell:ForumTableCell = tableView.dequeueReusableCell(withIdentifier: "ForumTableCell", for: indexPath as IndexPath) as! ForumTableCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let dict = arrForumList.object(at: indexPath.row) as! NSMutableDictionary
        
        cell.imgProfilePic.sd_setImage(with: NSURL(string: dict.value(forKey: "profile_pic") as! String) as URL!)
        cell.imgProfilePic.layer.cornerRadius = 20
        
//        cell.lblName.text = (dict.value(forKey: "first_name") as! String) + " " + (dict.value(forKey: "last_name") as! String)
//        if dict.value(forKey: "first_name") as! String == "" && dict.value(forKey: "last_name") as! String == "" {
            cell.lblName.text = "# \(dict.value(forKey: "user_id")!)"
//        }
        cell.lblDuration.text = ProjectUtility.timeAgoSinceDate(ProjectUtility.dateFromString(strDate: dict.value(forKey: "created_at") as! String, strFormatter: "yyyy-MM-dd HH:mm:ss"), currentDate: Date(), numericDates: true)
        cell.lblDescription.text = dict.value(forKey: "text") as? String
        
        cell.btnPlayVideo.isHidden = true
        cell.viewCountLayer.isHidden = true
        if dict.value(forKey: "files") != nil && (dict.value(forKey: "files") as? NSArray)?.count != 0 {
            
            cell.constPostImagesViewHeight.constant = 210
            
            let arrTemp = ((dict.value(forKey: "files") as! NSArray).object(at: 0) as! String).components(separatedBy: ".") as NSArray
            if arrTemp.count != 0 {
                if arrTemp.object(at: arrTemp.count-1) as! String == "jpg" || arrTemp.object(at: arrTemp.count-1) as! String == "png" {
                    
                    cell.imgPost.sd_setImage(with: NSURL(string: (dict.value(forKey: "files") as! NSArray).object(at: 0) as! String) as URL!)
                }else{
//                    cell.imgPost.image =  ProjectUtility.videoSnapshot(filePathLocal: (dict.value(forKey: "files") as! NSArray).object(at: 0) as! String)
                    cell.btnPlayVideo.isHidden = false
                    cell.btnPlayVideo.tag = indexPath.row
                    cell.btnPlayVideo.addTarget(self, action: #selector(btnPlayVideoAction(_:)), for: UIControlEvents.touchUpInside)
                }
            }
            
            if (dict.value(forKey: "files") as! NSArray).count > 1 {
                cell.viewCountLayer.isHidden = false
                cell.btnPostsCount.setTitle("\((dict.value(forKey: "files") as! NSArray).count - 1)+", for: UIControlState.normal)
                cell.btnPostsCount.tag = indexPath.row
                cell.btnPostsCount.addTarget(self, action: #selector(btnPostCountAction(_:)), for: UIControlEvents.touchUpInside)
            }
            
        }else{
            cell.constPostImagesViewHeight.constant = 0
        }
        
        if Int(dict.value(forKey: "total_likes") as! String)! < 2 {
            cell.lblLikes.text = "\(dict.value(forKey: "total_likes") as! String) Like"
        }else{
            cell.lblLikes.text = "\(dict.value(forKey: "total_likes") as! String) Likes"
        }
        
        if dict.value(forKey: "like_status") as! NSInteger == 0 {
            cell.imgLike.image = UIImage.init(named: "like_icon_gray")
            cell.lblLike.textColor = UIColor.init(red: 85.0/255.0, green: 85.0/255.0, blue: 85.0/255.0, alpha: 1.0)
        }else{
            cell.imgLike.image = UIImage.init(named: "like_icon")
            cell.lblLike.textColor = UIColor.init(red: 36.0/255.0, green: 159.0/255.0, blue: 231.0/255.0, alpha: 1.0)
        }
        
        if Int(dict.value(forKey: "total_comments") as! String)! < 2 {
            cell.lblCommnets.text = "\(dict.value(forKey: "total_comments") as! String) Comment"
        }else{
            cell.lblCommnets.text = "\(dict.value(forKey: "total_comments") as! String) Comments"
        }
        
        cell.btnLikePost.tag = indexPath.row
        cell.btnLikePost.addTarget(self, action: #selector(btnLikePostAction(_:)), for: UIControlEvents
        .touchUpInside)
        
        cell.btnCommnet.tag = indexPath.row
        cell.btnCommnet.addTarget(self, action: #selector(btnCommentsAction(_:)), for: UIControlEvents
            .touchUpInside)
        
        cell.btnCommnets.tag = indexPath.row
        cell.btnCommnets.addTarget(self, action: #selector(btnCommentsAction(_:)), for: UIControlEvents
            .touchUpInside)
        
        cell.imgBG.layer.shadowRadius = 6
        cell.imgBG.layer.shadowOffset = CGSize.init(width: 0, height: 6)
        cell.imgBG.layer.shadowColor = UIColor.init(red: 39.0/255.0, green: 46.0/255.0, blue: 54.0/255.0, alpha: 1.0).cgColor
        cell.imgBG.layer.shadowOpacity = 0.75
        
        return cell
    }
    
    @objc(tableView:willDisplayCell:forRowAtIndexPath:) func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if isLoading == false && indexPath.row == tblForum.numberOfRows(inSection: 0) - 1 {
            intPageCount = intPageCount + 1
            
            isLoading = true
            self.getForumListJson()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    // MARK:- Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "postSelectionSegue" {
            let vc: PostSelectionVC = segue.destination as! PostSelectionVC
            vc.arrForumList = arrForumList
        }else if segue.identifier == "postedPhotoVideoSegues" {
            let vc: PostedPhotoVideoVC = segue.destination as! PostedPhotoVideoVC
            vc.dictPostDetail = arrForumList.object(at: sender as! Int) as! NSMutableDictionary
        }else if segue.identifier == "postCommentsSegue" {
            let vc: PostCommentsVC = segue.destination as! PostCommentsVC
            vc.dictPost = arrForumList.object(at: sender as! Int) as! NSMutableDictionary
        }
    }
}

class ForumTableCell: UITableViewCell {
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblDuration: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var lblLikes: UILabel!
    @IBOutlet var lblLike: UILabel!
    @IBOutlet var lblCommnets: UILabel!
    
    @IBOutlet var btnPlayVideo: UIButton!
    @IBOutlet var btnLikePost: UIButton!
    @IBOutlet var btnPostsCount: UIButton!
    @IBOutlet var btnCommnet: UIButton!
    @IBOutlet var btnCommnets: UIButton!
    
    @IBOutlet var imgBG: UIImageView!
    @IBOutlet var imgProfilePic: UIImageView!
    @IBOutlet var imgPost: UIImageView!
    @IBOutlet var imgLike: UIImageView!
    
    @IBOutlet var viewCountLayer: UIView!
    
    @IBOutlet var constPostImagesViewHeight: NSLayoutConstraint!
}
