//
//  PostSelectionVC.swift
//  Schools360
//
//  Created by Sandeep on 24/08/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit
//import BSImagePicker
import Photos
import TLPhotoPicker
import AVKit

class PostSelectionVC: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate,TLPhotosPickerViewControllerDelegate {

    //MARK:- Outlet Declaration
    
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var imgProfilePic: UIImageView!
    
    @IBOutlet var tblPhotoVideoList: UITableView!
    @IBOutlet var txtViewPost: UITextView!
    
    @IBOutlet var txtSchoolName: SearchTextField!
    @IBOutlet var txtCountry: SearchTextField!
    @IBOutlet var txtState: SearchTextField!
    @IBOutlet var txtCity: SearchTextField!
    
    @IBOutlet var imgSchoolName: UIImageView!
    @IBOutlet var imgCountry: UIImageView!
    @IBOutlet var imgState: UIImageView!
    @IBOutlet var imgCity: UIImageView!
    
    //MARK: Other Varibles
    var arrForumList = NSMutableArray()
    var arrPhotoVideoList = NSMutableArray()
    var selectedAssets = [TLPHAsset]()
    
    //MARK:TypeAndSearch
    var countryResults = [SearchTextFieldItem]()
    var stateResults = [SearchTextFieldItem]()
    var cityResults = [SearchTextFieldItem]()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialization()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
    }
    
    // MARK:- Initialization
    
    func initialization(){
        
        imgProfilePic.layer.cornerRadius = 20
        if Constant.appDelegate.dictLoginUserDetail.value(forKey: "profile_pic") != nil {
            imgProfilePic.sd_setImage(with: NSURL(string: Constant.appDelegate.dictLoginUserDetail.value(forKey: "profile_pic") as! String) as URL!)
        }
        lblUserName.text =  "# \(Constants.appDelegate.dictLoginUserDetail.value(forKey: "user_id")!)"
        
        self.configureSchoolSearchTextField()
    }
    
    // MARK:- Webservice Call
    func makePostJson() {
        
        ProjectUtility.loadingShow()
        
        let dict = NSMutableDictionary()
        dict.setValue("user_id", forKey:Constant.appDelegate.dictLoginUserDetail.value(forKey: "user_id") as! String)
        dict.setValue("text", forKey: txtViewPost.text)
        
        if txtSchoolName.text == "" {
            dict.setValue(txtSchoolName.accessibilityIdentifier, forKey: "school_id")
        }else if txtCountry.text == ""{
            dict.setValue(txtCountry.accessibilityIdentifier, forKey: "country_id")
        }else if txtState.text == ""{
            dict.setValue(txtState.accessibilityIdentifier, forKey: "state_id")
        }else if txtCity.text == ""{
            dict.setValue(txtCity.accessibilityIdentifier, forKey: "city_id")
        }
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/forum/makepost"
        print("\(strUrl) --- \(dict)")
        
        let manager = AFHTTPSessionManager()
        manager.requestSerializer.timeoutInterval = 600
        
        manager.post(strUrl, parameters: dict, constructingBodyWith: { (formData) in
            
            for i in 0 ..< self.arrPhotoVideoList.count {
                
                if (self.arrPhotoVideoList.object(at: i) as! NSMutableDictionary).value(forKey: "Video") != nil {
                    
                    let videoAsset = (self.arrPhotoVideoList.object(at: i) as! NSMutableDictionary).value(forKey: "Video") as? AVURLAsset
                    
                    do {
                        let data = try Data.init(contentsOf: (videoAsset?.url)!)//NSData.init(contentsOf: (videoAsset?.url)!)
                        formData.appendPart(withFileData: data, name: "files[]", fileName: ObjectiveCMethods.findUniqueSavePathVideo(), mimeType: "video/quicktime")
                    }catch {
                        
                    }
                    
                }else{
                    
                    var imgVenue: UIImage = UIImage()
                    imgVenue = ObjectiveCMethods.scaleAndRotateImage((self.arrPhotoVideoList.object(at: i) as! NSMutableDictionary).value(forKey: "Image") as? UIImage)
                    let data = UIImagePNGRepresentation(imgVenue)!
                    
                    formData.appendPart(withFileData: data, name: "files[]", fileName: ObjectiveCMethods.findUniqueSavePathImage(), mimeType: "image/jpeg")
                }
            }
            
        }, progress: { (uploadProgress) in
            
            print("InProgress")
        }, success: { (task, responseObject) in
            
            ProjectUtility.loadingHide()
            print(responseObject!)
            
            if responseObject != nil, let dictResponse = responseObject as? NSDictionary {
                
                if dictResponse.value(forKey: "success") != nil && dictResponse.value(forKey: "success") as! Bool == true{
                    
                    self.arrForumList.insert((dictResponse.value(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary, at: 0)
                    
                    ProjectUtility.displayTost(erroemessage: "Your post has been posted")
                    _ = self.navigationController?.popViewController(animated: true)
                    
                    return
                }
            }
            
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
            
        }) { (task, error) in
            print(error)
            ProjectUtility.loadingHide()
            
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    //MARK:- Webservice Call
    fileprivate func searchSchoolNameJson(_ criteria: String, callback: @escaping ((_ results: [SearchTextFieldItem]) -> Void)) {
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        let dict = ["terms":txtSchoolName.text!]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/home/searchsuggestion"
        print("\(strUrl) --- \(dict)")
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            print(response?.webserviceResponse)
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        var results = [SearchTextFieldItem]()
                        
                        for result in dictData.value(forKey: "data") as! NSArray {
                            results.append(SearchTextFieldItem(title: (result as! NSDictionary).value(forKey: "school_name") as! String, subtitle: "", image: nil, id: (result as! NSDictionary).value(forKey: "id") as? String))
                        }
                        
                        DispatchQueue.main.async {
                            callback(results)
                        }
                    }else {
                        DispatchQueue.main.async {
                            callback([])
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        callback([])
                    }
                }
            }else {
                DispatchQueue.main.async {
                    callback([])
                }
            }
            
        }) { (error) -> Void in
            
            print(error)
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    fileprivate func searchCountryJson(_ criteria: String, callback: @escaping ((_ results: [SearchTextFieldItem]) -> Void)) {
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/home/countrylist"
        print("\(strUrl)")
        
        webserviceCall.get(NSURL(string: strUrl) as URL!, parameters: nil as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            print(response?.webserviceResponse)
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        for result in dictData.value(forKey: "data") as! NSArray {
                            self.countryResults.append(SearchTextFieldItem(title: (result as! NSDictionary).value(forKey: "name") as! String, subtitle: "", image: nil, id: (result as! NSDictionary).value(forKey: "id") as? String))
                        }
                        
                        DispatchQueue.main.async {
                            callback(self.countryResults)
                        }
                    }else {
                        DispatchQueue.main.async {
                            callback([])
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        callback([])
                    }
                }
            }else {
                DispatchQueue.main.async {
                    callback([])
                }
            }
            
        }) { (error) -> Void in
            
            print(error)
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    fileprivate func searchStateJson(_ criteria: String, callback: @escaping ((_ results: [SearchTextFieldItem]) -> Void)) {
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        let dict = ["country_id":txtCountry.accessibilityIdentifier!]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/home/getstate"
        print("\(strUrl) --- \(dict)")
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            print(response?.webserviceResponse)
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        for result in dictData.value(forKey: "data") as! NSArray {
                            self.stateResults.append(SearchTextFieldItem(title: (result as! NSDictionary).value(forKey: "name") as! String, subtitle: "", image: nil, id: (result as! NSDictionary).value(forKey: "id") as? String))
                        }
                        
                        DispatchQueue.main.async {
                            callback(self.stateResults)
                        }
                    }else {
                        DispatchQueue.main.async {
                            callback([])
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        callback([])
                    }
                }
            }else {
                DispatchQueue.main.async {
                    callback([])
                }
            }
            
        }) { (error) -> Void in
            
            print(error)
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    fileprivate func searchCityJson(_ criteria: String, callback: @escaping ((_ results: [SearchTextFieldItem]) -> Void)) {
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        let dict = ["state_id":txtState.accessibilityIdentifier!]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/home/getcity"
        print("\(strUrl) --- \(dict)")
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            print(response?.webserviceResponse)
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        for result in dictData.value(forKey: "data") as! NSArray {
                            self.cityResults.append(SearchTextFieldItem(title: (result as! NSDictionary).value(forKey: "name") as! String, subtitle: "", image: nil, id: (result as! NSDictionary).value(forKey: "id") as? String))
                        }
                        
                        DispatchQueue.main.async {
                            callback(self.stateResults)
                        }
                    }else {
                        DispatchQueue.main.async {
                            callback([])
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        callback([])
                    }
                }
            }else {
                DispatchQueue.main.async {
                    callback([])
                }
            }
            
        }) { (error) -> Void in
            
            print(error)
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    // MARK:- Button TouchUp
    @IBAction func btnBackAction (_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPostAction (_ sender: UIButton) {
        
        self.view.endEditing(true)
        if txtViewPost.text != "" || arrPhotoVideoList.count != 0 {
            self.makePostJson()
        }
    }
    
    @IBAction func btnPhotoVideoAction (_ sender: UIButton) {
        
        let viewController = CustomPhotoPickerViewController()
        viewController.delegate = self
        viewController.didExceedMaximumNumberOfSelection = { [weak self] (picker) in
            ProjectUtility.showAlert(vc: picker, strMessage: "Exceed Maximum Number Of Selection")
        }
        var configure = TLPhotosPickerConfigure()
        configure.numberOfColumn = 3
        configure.maxSelectedAssets = 10
        configure.allowedVideo = false
//        configure.nibSet = (nibName: "CustomCell_Instagram", bundle: Bundle.main)
        viewController.configure = configure
        viewController.selectedAssets = self.selectedAssets
        
        self.present(viewController.wrapNavigationControllerWithoutBar(), animated: true, completion: nil)
    }
    
    //MARK: Cell Events
    @IBAction func btnCrossAction (_ sender: UIButton) {
        
        arrPhotoVideoList.removeObject(at: sender.tag)
        tblPhotoVideoList.reloadData()
    }
    
    @IBAction func btnPlayVideoAction (_ sender: UIButton) {
        
        if let videoAsset = (arrPhotoVideoList.object(at: sender.tag) as! NSMutableDictionary).value(forKey: "Video") as? AVURLAsset {
            DispatchQueue.main.async(execute: {
                
                let player = AVPlayer.init(url: videoAsset.url)
                
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            })
        }
        
    }
    
    //MARK:- Tableview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return arrPhotoVideoList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        
        let cell:PostSelectionTableCell = tableView.dequeueReusableCell(withIdentifier: "PostSelectionTableCell", for: indexPath as IndexPath) as! PostSelectionTableCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        
        cell.imgPhoto.image = (arrPhotoVideoList.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "Image") as? UIImage
        
        if (arrPhotoVideoList.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "Video") != nil {
            
            cell.btnPlayVideo.isHidden = false
            cell.btnPlayVideo.tag = indexPath.row
            cell.btnPlayVideo.addTarget(self, action: #selector(btnPlayVideoAction(_:)), for: UIControlEvents.touchUpInside)
        }else{
            cell.btnPlayVideo.isHidden = true
        }
        
        cell.btnCross.tag = indexPath.row
        cell.btnCross.addTarget(self, action: #selector(btnCrossAction(_:)), for: UIControlEvents.touchUpInside)
        
        cell.btnCross.layer.cornerRadius = 15
        
        return cell
    }
    
    //MARK:- Set Suggetion Box
    fileprivate func configureSchoolSearchTextField() {
        
        txtSchoolName.maxNumberOfResults = 100
        txtSchoolName.maxResultsListHeight = 200
        txtSchoolName.theme.font = UIFont.init(name: "Avenir Next", size: 12)!
        
        txtSchoolName.itemSelectionHandler = { item, itemPosition in
            
            self.view.endEditing(true)
            self.txtSchoolName.text = item.title
            self.txtSchoolName.accessibilityIdentifier = item.id
        }
        
        txtSchoolName.userStoppedTypingHandler = {
            self.txtSchoolName.accessibilityIdentifier = ""
            if let criteria = self.txtSchoolName.text {
                if criteria.characters.count > 1 {
                    
                    self.txtSchoolName.showLoadingIndicator()
                    
                    self.searchSchoolNameJson(criteria) { results in
                        
                        self.txtSchoolName.filterItems(results)
                        self.txtSchoolName.stopLoadingIndicator()
                    }
                }
            }
        }
    }
    
    fileprivate func configureCountrySearchTextField() {
        
        txtCountry.maxNumberOfResults = 100
        txtCountry.maxResultsListHeight = 200
        txtCountry.theme.font = UIFont.init(name: "Avenir Next", size: 12)!
        
        txtCountry.itemSelectionHandler = { item, itemPosition in
            
            self.view.endEditing(true)
            self.txtCountry.text = item.title
            self.txtCountry.accessibilityIdentifier = item.id
            self.stateResults = [SearchTextFieldItem]()
            self.cityResults = [SearchTextFieldItem]()
        }
        
        txtCountry.userStoppedTypingHandler = {
            self.txtCountry.accessibilityIdentifier = ""
            self.txtState.text = ""
            self.txtState.accessibilityIdentifier = ""
            self.txtCity.text = ""
            self.txtCity.accessibilityIdentifier = ""
            
            if let criteria = self.txtCountry.text {
                if criteria.characters.count > 1 {
                    
                    if self.countryResults.count == 0 {
                        self.txtCountry.showLoadingIndicator()
                        
                        self.searchCountryJson(criteria) { results in
                            
                            self.txtCountry.filterItems(results)
                            self.txtCountry.stopLoadingIndicator()
                        }
                    }else{
                        self.txtCountry.showLoadingIndicator()
                        self.txtCountry.filterItems(self.countryResults)
                        self.txtCountry.stopLoadingIndicator()
                    }
                }
            }
        }
    }
    
    fileprivate func configureStateSearchTextField() {
        
        txtState.maxNumberOfResults = 100
        txtState.maxResultsListHeight = 200
        txtState.theme.font = UIFont.init(name: "Avenir Next", size: 12)!
        
        txtState.itemSelectionHandler = { item, itemPosition in
            
            self.view.endEditing(true)
            self.txtState.text = item.title
            self.txtState.accessibilityIdentifier = item.id
            
            self.cityResults = [SearchTextFieldItem]()
        }
        
        txtState.userStoppedTypingHandler = {
            self.txtState.accessibilityIdentifier = ""
            self.txtCity.text = ""
            self.txtCity.accessibilityIdentifier = ""
            if let criteria = self.txtState.text {
                if criteria.characters.count > 1 {
                    
                    if self.stateResults.count == 0 {
                        self.txtState.showLoadingIndicator()
                        
                        self.searchStateJson(criteria) { results in
                            
                            self.txtState.filterItems(results)
                            self.txtState.stopLoadingIndicator()
                        }
                    }else{
                        self.txtState.showLoadingIndicator()
                        self.txtState.filterItems(self.stateResults)
                        self.txtState.stopLoadingIndicator()
                    }
                }
            }
        }
    }
    
    fileprivate func configureCitySearchTextField() {
        
        txtCity.maxNumberOfResults = 100
        txtCity.maxResultsListHeight = 200
        txtCity.theme.font = UIFont.init(name: "Avenir Next", size: 12)!
        
        txtCity.itemSelectionHandler = { item, itemPosition in
            
            self.view.endEditing(true)
            self.txtCity.text = item.title
            self.txtCity.accessibilityIdentifier = item.id
        }
        
        txtCity.userStoppedTypingHandler = {
            self.txtCity.accessibilityIdentifier = ""
            if let criteria = self.txtCity.text {
                if criteria.characters.count > 1 {
                    
                    if self.cityResults.count == 0 {
                        self.txtCity.showLoadingIndicator()
                        
                        self.searchCityJson(criteria) { results in
                            
                            self.txtCity.filterItems(results)
                            self.txtCity.stopLoadingIndicator()
                        }
                    }else{
                        self.txtCity.showLoadingIndicator()
                        self.txtCity.filterItems(self.cityResults)
                        self.txtCity.stopLoadingIndicator()
                    }
                }
            }
        }
    }
    
    // MARK:- TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtSchoolName {
            self.changeTextFiledLineColor(textField, imgSchoolName)
        }else if textField == txtCountry {
            self.changeTextFiledLineColor(textField, imgCountry)
        }else if textField == txtState {
            self.changeTextFiledLineColor(textField, imgState)
        }else if textField == txtCity {
            self.changeTextFiledLineColor(textField, imgCity)
        }
    }
    
    @IBAction func textFieldEditingChanged(_ textField: UITextField) {
        
        if textField == txtSchoolName {
            self.changeTextFiledLineColor(textField, imgSchoolName)
        }else if textField == txtCountry {
            self.changeTextFiledLineColor(textField, imgCountry)
        }else if textField == txtState {
            if txtCountry.text == "" {
                txtState.resignFirstResponder()
                txtState.text = ""
                ProjectUtility.displayTost(erroemessage: "Please enter country name")
                return
            }else if txtCountry.accessibilityIdentifier == ""{
                txtState.resignFirstResponder()
                txtState.text = ""
                ProjectUtility.displayTost(erroemessage: "Please select country from drop down box")
                return
            }
            self.changeTextFiledLineColor(textField, imgState)
        }else if textField == txtCity {
            if txtState.text == "" {
                self.view.endEditing(true)
                txtCity.text = ""
                ProjectUtility.displayTost(erroemessage: "Please enter state name")
                return
            }
            self.changeTextFiledLineColor(textField, imgCity)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        imgSchoolName.backgroundColor = UIColor.lightGray
        imgCountry.backgroundColor = UIColor.lightGray
        imgState.backgroundColor = UIColor.lightGray
        imgCity.backgroundColor = UIColor.lightGray
    }
    
    func changeTextFiledLineColor(_ textField: UITextField, _ imageView: UIImageView){
        
        if textField.text == "" {
            imageView.backgroundColor = UIColor.lightGray
        }else{
            imageView.backgroundColor = UIColor.init(red: 99.0 / 255.0, green: 160.0 / 255.0, blue: 248.0 / 255.0, alpha: 1.0)
        }
    }
    
    //MARK:- Custom Image Picker Delegate
    func dismissPhotoPicker(withTLPHAssets: [TLPHAsset]) {
        // use selected order, fullresolution image
        
        let requestOptions = PHImageRequestOptions()
        requestOptions.resizeMode = PHImageRequestOptionsResizeMode.exact
        requestOptions.deliveryMode = PHImageRequestOptionsDeliveryMode.highQualityFormat
        // this one is key
        requestOptions.isSynchronous = true
        
        arrPhotoVideoList.removeAllObjects()
        
        for asset in withTLPHAssets{
            if (asset.phAsset?.mediaType == PHAssetMediaType.image){

                PHImageManager.default().requestImage(for: asset.phAsset! , targetSize: PHImageManagerMaximumSize, contentMode: PHImageContentMode.default, options: requestOptions, resultHandler: { (pickedImage, info) in
                    
                    let dict = NSMutableDictionary()
                    dict.setValue(pickedImage!, forKey: "Image")
                    
                    self.arrPhotoVideoList.add(dict)
                    
                    self.tblPhotoVideoList.reloadData()
                })
            }else if (asset.phAsset?.mediaType == PHAssetMediaType.video) {
                    
//                PHCachingImageManager().requestAVAsset(forVideo: asset.phAsset!, options: nil, resultHandler: {(videoAssets, audioMix, info) in
//                    
//                    let videoAsset = videoAssets as! AVURLAsset
//                    
//                    PHImageManager.default().requestImage(for: asset.phAsset! , targetSize: PHImageManagerMaximumSize, contentMode: PHImageContentMode.default, options: requestOptions, resultHandler: { (pickedImage, info) in
//                        
//                        let dict = NSMutableDictionary()
//                        dict.setValue(pickedImage!, forKey: "Image")
//                        dict.setValue(videoAsset, forKey: "Video")
//                        
//                        self.arrPhotoVideoList.add(dict)
//                        
//                        self.tblPhotoVideoList.reloadData()
//                    })
//                })
            }
        }
        
        self.selectedAssets = withTLPHAssets
    }
    
    func dismissPhotoPicker(withPHAssets: [PHAsset]) {
        // if you want to used phasset.
    }
    
    func photoPickerDidCancel() {
        // cancel
    }
    
    func dismissComplete() {
        // picker dismiss completion
    }
    
    func didExceedMaximumNumberOfSelection(picker: TLPhotosPickerViewController) {
        ProjectUtility.showAlert(vc: picker, strMessage: "Exceed Maximum Number Of Selection")
    }
}

class PostSelectionTableCell: UITableViewCell {
    
    @IBOutlet var imgPhoto: UIImageView!
    @IBOutlet var btnCross: UIButton!
    
    @IBOutlet var btnPlayVideo: UIButton!
}
