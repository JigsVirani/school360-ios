//
//  PostedPhotoVideoVC.swift
//  Schools360
//
//  Created by Sandeep on 29/08/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class PostedPhotoVideoVC: UIViewController {

    //MARK:- Outlet Declaration
    @IBOutlet var imgUserPic: UIImageView!
    
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblDuration: UILabel!
    @IBOutlet var lblDescription: UILabel!
    
    @IBOutlet var tblPostList: UITableView!
    
    //MARK: Other Varibles
    var dictPostDetail = NSMutableDictionary()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(dictPostDetail)
        
        imgUserPic.sd_setImage(with: NSURL(string: dictPostDetail.value(forKey: "profile_pic") as! String) as URL!)
        imgUserPic.layer.cornerRadius = 20
        
//        lblUserName.text = (dictPostDetail.value(forKey: "first_name") as! String) + " " + (dictPostDetail.value(forKey: "last_name") as! String)
//        if dictPostDetail.value(forKey: "first_name") as! String == "" && dictPostDetail.value(forKey: "last_name") as! String == "" {
            lblUserName.text = "# \(dictPostDetail.value(forKey: "user_id")!)"
//        }
        lblDuration.text = ProjectUtility.timeAgoSinceDate(ProjectUtility.dateFromString(strDate: dictPostDetail.value(forKey: "created_at") as! String, strFormatter: "yyyy-MM-dd HH:mm:ss"),currentDate: Date(), numericDates: true)
        lblDescription.text = dictPostDetail.value(forKey: "text") as? String
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    // MARK:- Button TouchUp
    @IBAction func btnBackAction (_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: CellEvents
    @IBAction func btnPlayVideoAction (_ sender: UIButton) {
        
        let videoURL = URL(string: (dictPostDetail.value(forKey: "files") as! NSArray).object(at: sender.tag) as! String)
        let player = AVPlayer.init(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    //MARK:- Tableview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return (dictPostDetail.value(forKey: "files") as! NSArray).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        
        let cell:PostSelectionTableCell = tableView.dequeueReusableCell(withIdentifier: "PostSelectionTableCell", for: indexPath as IndexPath) as! PostSelectionTableCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell.btnPlayVideo.isHidden = true
        
        let arrTemp = ((dictPostDetail.value(forKey: "files") as! NSArray).object(at: indexPath.row) as! String).components(separatedBy: ".") as NSArray
        if arrTemp.count != 0 {
            if arrTemp.object(at: arrTemp.count-1) as! String == "jpg" || arrTemp.object(at: arrTemp.count-1) as! String == "png" {
                
                cell.imgPhoto.sd_setImage(with: NSURL(string: (dictPostDetail.value(forKey: "files") as! NSArray).object(at: 0) as! String) as URL!)
            }else{
                cell.imgPhoto.image =  ProjectUtility.videoSnapshot(filePathLocal: (dictPostDetail.value(forKey: "files") as! NSArray).object(at: 0) as! String)
                cell.btnPlayVideo.isHidden = false
                cell.btnPlayVideo.tag = indexPath.row
                cell.btnPlayVideo.addTarget(self, action: #selector(btnPlayVideoAction(_:)), for: UIControlEvents.touchUpInside)
            }
        }
        
        return cell
    }
}
