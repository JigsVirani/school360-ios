//
//  PostCommentsVC.swift
//  Schools360
//
//  Created by Sandeep on 29/08/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit

class PostCommentsVC: UIViewController {

    //MARK:- Outlet Declaration
    @IBOutlet var imgUserPic: UIImageView!
    
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblDuration: UILabel!
    
    @IBOutlet var txtViewComment: UITextView!
    
    @IBOutlet var tblCommentList: UITableView!
    
    //MARK: Other Varibles
    var dictPost = NSMutableDictionary()
    var arrCommentsList = NSMutableArray()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(dictPost)
        
//        imgUserPic.sd_setImage(with: NSURL(string: dictPost.value(forKey: "profile_pic") as! String) as URL!)
//        imgUserPic.layer.cornerRadius = 20
//        
//        lblUserName.text = (dictPost.value(forKey: "first_name") as! String) + " " + (dictPost.value(forKey: "last_name") as! String)
//        if dictPost.value(forKey: "first_name") as! String == "" && dictPost.value(forKey: "last_name") as! String == "" {
//            lblUserName.text = "No name"
//        }
//        lblDuration.text = ProjectUtility.timeAgoSinceDate(ProjectUtility.dateFromString(strDate: dictPost.value(forKey: "created_at") as! String, strFormatter: "yyyy-MM-dd HH:mm:ss"),currentDate: Date(), numericDates: true)
        
        self.getDetailPostJson()
        
        tblCommentList.rowHeight = UITableViewAutomaticDimension
        tblCommentList.estimatedRowHeight = 100.0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK:- Webservice Call
    func getDetailPostJson() {
        
        ProjectUtility.loadingShow()
        
        let dict = ["user_id": Constant.appDelegate.dictLoginUserDetail.value(forKey: "user_id") as! String, "forum_id": dictPost.value(forKey: "id") as! String] as NSDictionary
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/forum/detailpost"
        print("\(strUrl) --- \(dict)")
        
        let manager = AFHTTPSessionManager()
        manager.requestSerializer.timeoutInterval = 600
        
        manager.post(strUrl, parameters: dict, progress: { (progress) in
            print("InProgress")
        }, success: { (task, responseObject) in
            
            ProjectUtility.loadingHide()
            print(responseObject!)
            
            if responseObject != nil, let dictResponse = responseObject as? NSDictionary {
                
                if dictResponse.value(forKey: "success") != nil && dictResponse.value(forKey: "success") as! Bool == true{
                    
                    self.arrCommentsList = ((dictResponse.value(forKey: "data") as! NSDictionary).value(forKey: "comments") as! NSArray).mutableCopy() as! NSMutableArray
                    self.tblCommentList.reloadData()
                    return
                }
                
                ProjectUtility.displayTost(erroemessage: dictResponse.value(forKey: "message") as! String)
                return
            }
            
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
            
        }) { (task, error) in
            print(error)
            ProjectUtility.loadingHide()
            
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    //MAKE COMMENT
    func makeCommentJson() {
        
        ProjectUtility.loadingShow()
        
        let dict = ["user_id": Constant.appDelegate.dictLoginUserDetail.value(forKey: "user_id") as! String, "forum_id": dictPost.value(forKey: "id") as! String, "text": txtViewComment.text] as NSDictionary
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/forum/commentpost"
        print("\(strUrl) --- \(dict)")
        
        let manager = AFHTTPSessionManager()
        manager.requestSerializer.timeoutInterval = 600
        
        manager.post(strUrl, parameters: dict, progress: { (progress) in
            print("InProgress")
        }, success: { (task, responseObject) in
            
            ProjectUtility.loadingHide()
            print(responseObject!)
            
            if responseObject != nil, let dictResponse = responseObject as? NSDictionary {
                
                if dictResponse.value(forKey: "success") != nil && dictResponse.value(forKey: "success") as! Bool == true{
                    
                    let dict = NSMutableDictionary()
                    dict.setValue(ProjectUtility.stringFromDate(date: Date(), strFormatter: "yyyy-MM-dd HH:mm:ss"), forKey: "created_at")
                    dict.setValue("", forKey: "name")
                    dict.setValue(Constant.appDelegate.dictLoginUserDetail.value(forKey: "user_id"), forKey: "user_id")
                    dict.setValue(Constant.appDelegate.dictLoginUserDetail.value(forKey: "profile_pic") as! String, forKey: "profile_pic")
                    dict.setValue(self.txtViewComment.text, forKey: "text")
                    
                    self.arrCommentsList.insert(dict, at: 0)
                    self.tblCommentList.reloadData()
                    
                    self.dictPost.setValue(self.arrCommentsList, forKey: "comments")
                    
                    var intCommentCount = Int(self.dictPost.value(forKey: "total_comments") as! String)
                    intCommentCount = intCommentCount! + 1
                    self.dictPost.setValue("\(intCommentCount!)", forKey: "total_comments")
                    
                    self.txtViewComment.text = ""
                    
                    return
                }
                
                ProjectUtility.displayTost(erroemessage: dictResponse.value(forKey: "message") as! String)
                return
            }
            
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
            
        }) { (task, error) in
            print(error)
            ProjectUtility.loadingHide()
            
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    // MARK:- Button TouchUp
    @IBAction func btnBackAction (_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPostAction (_ sender: UIButton) {
        
        self.view.endEditing(true)
        if txtViewComment.text != "" {
            self.makeCommentJson()
        }
    }
    
    //MARK:- Tableview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.arrCommentsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        
        let cell:CommentsTableCell = tableView.dequeueReusableCell(withIdentifier: "CommentsTableCell", for: indexPath as IndexPath) as! CommentsTableCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let dict = arrCommentsList.object(at: indexPath.row) as! NSDictionary
        
        if dict.value(forKey: "user_id") != nil {
            cell.lblName.text = "# \(dict.value(forKey: "user_id")!)"
        }else{
            
            cell.lblName.text = dict.value(forKey: "name") as? String
            if dict.value(forKey: "name") as! String == "" || dict.value(forKey: "name") as! String == " " {
                cell.lblName.text = "No name"
            }
        }
        
        cell.lblDescription.text = dict.value(forKey: "text") as? String
        cell.lblDuration.text = ProjectUtility.timeAgoSinceDate(ProjectUtility.dateFromString(strDate: dict.value(forKey: "created_at") as! String, strFormatter: "yyyy-MM-dd HH:mm:ss"),currentDate: Date(), numericDates: true)
        
        cell.imgProfilePic.sd_setImage(with: NSURL(string: dict.value(forKey: "profile_pic") as! String) as URL!)
        cell.imgProfilePic.layer.cornerRadius = 20
        
        cell.imgBG.layer.shadowRadius = 4
        cell.imgBG.layer.shadowOffset = CGSize.init(width: 0, height: 8)
        cell.imgBG.layer.shadowColor = UIColor.init(red: 39.0/255.0, green: 46.0/255.0, blue: 54.0/255.0, alpha: 1.0).cgColor
        cell.imgBG.layer.shadowOpacity = 0.75
        
        return cell
    }
}

class CommentsTableCell: UITableViewCell {
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var lblDuration: UILabel!
    
    @IBOutlet var imgProfilePic: UIImageView!
    @IBOutlet var imgBG: UIImageView!
}
