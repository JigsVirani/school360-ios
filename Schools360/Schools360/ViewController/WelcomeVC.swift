//
//  WelcomeVC.swift
//  Schools360
//
//  Created by Sandeep Gangajaliya on 13/04/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit

class WelcomeVC: UIViewController {

    //MARK:- Outlet Declaration
//    @IBOutlet var imgDescriptionBG: UIImageView!
    
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnRegister: UIButton!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ProjectUtility.setCommonButton(button: btnLogin)
        ProjectUtility.setCommonButton(button: btnRegister)
        
        if Constants.appDelegate.dictLoginUserDetail.value(forKey: "email_address") != nil {
            self.performSegue(withIdentifier: "advanceSearchSegue", sender: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK:- Button TouchUp
    
    @IBAction func btnRegisterAction (_ sender: UIButton){
        
    }
}
